//
//  AppDelegate.swift
//  Family Pic Will
//
//  Created by mac on 4/23/21.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import UserNotifications
import SwiftyJSON
import Alamofire
@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    var user: User?
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        //Messaging.messaging().delegate = self
        Messaging.messaging().token { token, error in
          if let error = error {
            print("Error fetching FCM registration token: \(error)")
          } else if let token = token {
            print("FCM registration token: \(token)")
//            self.fcmRegTokenMessage.text  = "Remote FCM registration token: \(token)"
            UserDefaults.standard.set(token, forKey: "fcmtoken")
            let test = UserDefaults.standard.string(forKey: "fcmtoken")!
            print("FCM registration token2: \(test)")
              if UserDefaults.standard.string(forKey: "token") != nil {
                  self.send_token()
              }
            
          }
        }
     
//        FirebaseApp.configure()
        
        UIFont.familyNames.forEach({name in
            for font_name in UIFont.fontNames(forFamilyName: name){
                print("\n\(font_name)")
            }
        })
        IQKeyboardManager.shared.enable = true
      IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    
        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(String(describing: fcmToken))")
        UserDefaults.standard.set(fcmToken, forKey: "fcmtoken")
      let dataDict:[String: String] = ["token": fcmToken ?? ""]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
    //messaging
    //func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        //print("Firebase registration token: \(String(describing: fcmToken))")

      //let dataDict:[String: String] = ["token": fcmToken ?? ""]
      //NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
      // TODO: If necessary send token to application server.
      // Note: This callback is fired at each app startup and whenever a new token is generated.
        //}
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      Messaging.messaging().apnsToken = deviceToken
        
  
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X.2hhx", $1)})
       
        
        UserDefaults.standard.set(deviceTokenString, forKey: "token")
         
         // Print it to console
         print("APNs device token: \(deviceTokenString)")
         
         // Persist it in your backend in case it's new
         
         let tokenParts = deviceToken.map { data -> String in
             print("Moeezzzz")
             return String(format: "%02.2hhx", data)
         }
         let tokenk = tokenParts.joined()
         
         print("deviceToken: \(tokenk)")
    }
    

      // Receive displayed notifications for iOS 10 devices.
      func userNotificationCenter(_ center: UNUserNotificationCenter,
                                  willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo

        // With swizzling disabled you must let Messaging know about the message, for Analytics
         Messaging.messaging().appDidReceiveMessage(userInfo)

        // ...

        // Print full message.
        print(userInfo)

        // Change this to your preferred presentation option
        completionHandler([[.banner,.list, .sound]])
      }

      func userNotificationCenter(_ center: UNUserNotificationCenter,
                                  didReceive response: UNNotificationResponse,
                                  withCompletionHandler completionHandler: @escaping () -> Void) {
    
        let userInfo = response.notification.request.content.userInfo
        user = (userInfo["User"] as! User)
        //coordinateToSomeVC(id: "user!.id", fcode: "user!.familyCode")
        
        WishAlertBtn(UserId: user!.id, familyCode: user!.familyCode)
            
            completionHandler()
      }
    func WishAlertBtn(UserId: String, familyCode: String) {
        
        let wishAlert = UIAlertController(title: "Send Wishes", message: nil, preferredStyle: .alert)
        wishAlert.addTextField { (textField : UITextField!) -> Void in
              textField.placeholder = "Send Wishes..."
            }
        wishAlert.addAction(UIAlertAction(title: "Wish", style: UIAlertAction.Style.default) { (UIAlertAction) in
            let firstTextField = wishAlert.textFields![0] as UITextField
            self.sendHandler(Wish: firstTextField.text ?? "Happy Birthday...", id: UserId, fcode: familyCode)

        })
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel, handler: nil) /*self.cancelBtn)*/

        wishAlert.addAction(cancelAction)
        self.window?.rootViewController?.present(wishAlert, animated: true, completion: nil)
        //self.present(wishAlert, animated: true)
        
       
        
    }
    
    func sendHandler(Wish: String, id: String, fcode: String){
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let wish = BirthdayWishApi.Request(text: Wish, familyCode: fcode, userid: id)
                let requestService = SCService<BirthdayWishApi.Request,BirthdayWishApi.Response>()
                requestService.request("http://3.143.193.45:1337/api/birthday", method: .post, parameters: wish, encoder: JSONParameterEncoder(), header: header){
                    (result) in

                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == false{
                      //  self.showPromptAlert("Sorry!", message: response.message);
                    }
                       // self.Relation.text = (self.wishText?.text)!

                        //self.performSegue(withIdentifier: "MainNav", sender: self)
                    case .failure(let error):
                        debugPrint(error)

                        print(error)

                    }
                }
    }
    func send_token() {
        
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
                print(header)
        let fcmtoken = UserDefaults.standard.string(forKey: "fcmtoken")!
        print("asdfgh\(fcmtoken)")
        let parameter = LoginPushApi.Request(token: "\(fcmtoken)")
        let requestService = SCService<LoginPushApi.Request,LoginPushApi.Response>()
                requestService.request("http://3.143.193.45:1337/api/fcmToken", method: .post, parameters: parameter, encoder: JSONParameterEncoder(), header: header){
                    (result) in
                    
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == false {
                            //self.showPromptAlert("Sorry!", message: response.message);
                            self.get_Newtoken()
                        }
                        break
                    case .failure(let error):
                    
                        print("hhhhhhh\(error.responseCode)")
                        print("Got failure: \(error)")
                        self.get_Newtoken()
//                        if error.responseCode == 401{
//                            self.get_Newtoken()
//                            }
//                        break
                        debugPrint(error)
                        print(error)
                       }}
}
    func get_Newtoken() {
        
       // let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
         //       print(header)
        let email = UserDefaults.standard.string(forKey: "mail")!
        print("asdfgh\(email)")
        let parameter = UpdateToken.Request(email: UserDefaults.standard.string(forKey: "mail") ?? "")
        let requestService = SCService<UpdateToken.Request,UpdateToken.Response>()
                requestService.request("http://3.143.193.45:1337/api/getnewtoken", method: .post, parameters: parameter, encoder: JSONParameterEncoder()){
                    (result) in
        
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == true {
                            UserDefaults.standard.set(response.data.token, forKey: "token")
                            //self.showPromptAlert("Sorry!", message: response.message);
                        }
                    case .failure(let error):
                        debugPrint(error)
                        print(error)
                       }}
}
    
   func coordinateToSomeVC(id: String, fcode: String)
    {
        

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let yourVC = storyboard.instantiateViewController(withIdentifier: "BirthdayStoryboardID") as? BirthDayViewController
    self.window?.rootViewController?.present(yourVC!, animated: true, completion:nil)

        // you can assign your vc directly or push it in navigation stack as follows:
      
    }
    //BirthDayViewController
    //BirthdayStoryboardID
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      // Messaging.messaging().appDidReceiveMessage(userInfo)

      // Print message ID.
      if let messageID = userInfo["gcmMessageIDKey"] {
        print("Message ID: \(messageID)")
      }

      // Print full message.
      print(userInfo)

      completionHandler(UIBackgroundFetchResult.newData)
    }
    
  


    }



