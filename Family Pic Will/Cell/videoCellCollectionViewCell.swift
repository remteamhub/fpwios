//
//  videoCellCollectionViewCell.swift
//  Family Pic Will
//
//  Created by Apple on 24/05/2021.
//

import UIKit
import AVKit
import AVFoundation

class videoCellCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var videoThumb: UIImageView!
    
//    @IBAction func VidBtn(_ sender: Any) {
//    }
 
    func vidInIt(videoUrl : String) {
            let url = URL(string: videoUrl)
        self.getThumbnailImageFromVideoUrl(url: url!){ (thumbnailImage) in
            self.videoThumb.image = thumbnailImage
        }
        
    }
    
    //MARK -thumbnail
    func checkImage(imageStr: String){
        if imageStr == ""{
            videoThumb.isHidden = true
        }else{
            videoThumb.isHidden = false
            videoThumb.image = UIImage(named: imageStr)
            
        }
    }


    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbNailImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }

    
}
