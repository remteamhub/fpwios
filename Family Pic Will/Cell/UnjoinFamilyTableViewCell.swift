//
//  UnjoinFamilyTableViewCell.swift
//  Family Pic Will
//
//  Created by Apple on 09/05/2021.
//

import UIKit

class UnjoinFamilyTableViewCell: UITableViewCell {
    @IBOutlet weak var familyName: UILabel!
    
    @IBOutlet weak var familyImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configuredData(){
        familyName.font = UIFont(name: "Open Sans Regular", size: 20)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
