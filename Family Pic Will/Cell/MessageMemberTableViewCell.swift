//
//  MessageMemberTableViewCell.swift
//  Family Pic Will
//
//  Created by mac on 8/20/21.
//

import UIKit
import DropDown


class MessageMemberTableViewCell: UITableViewCell, UINavigationControllerDelegate {
    

    
    @IBOutlet weak var dropdownView: UIView!
    @IBOutlet weak var dropdown: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var memberImage: UIImageView!
    @IBOutlet weak var memberView: UIView!
    
    var row:Int?
    let dropDown = DropDown()
    let Values = ["Add/Remove"]
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        dropDown.anchorView = dropdownView
        dropDown.dataSource = Values
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.direction = .bottom
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
        
//            func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//                let passing = segue.destination as! NewGroupViewController

               // passing = sender as! String

            
          
        }
    }
        
        @IBAction func Groupdropdown(_ sender: Any) {
            dropDown.show()
        }
        

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
            
    }

}
