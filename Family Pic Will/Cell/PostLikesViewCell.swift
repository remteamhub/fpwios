//
//  PostLikesViewCell.swift
//  familywithwall
//
//  Created by Apple on 29/04/2021.
//

import UIKit

class PostLikesViewCell: UITableViewCell {
    
    
    @IBOutlet weak var UserImage: UIImageView!
    
    
    @IBOutlet weak var UserName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        UserImage.layer.cornerRadius = UserImage.frame.size.width / 2
        UserImage.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
