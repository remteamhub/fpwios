//
//  FamilyTableViewCell.swift
//  FPW
//
//  Created by Apple on 03/05/2021.
//

import UIKit

class FamilyTableViewCell: UITableViewCell {

    @IBOutlet weak var familyImageView: UIImageView!
    @IBOutlet weak var familyLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configurData(){
        familyLabel.font = UIFont(name: "Open Sans Regular", size: 20)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
