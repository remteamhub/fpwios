//
//  DeceasedTableViewCell.swift
//  Family Pic Will
//
//  Created by Apple on 19/05/2021.
//

import UIKit

class DeceasedTableViewCell: UITableViewCell {

    @IBOutlet weak var deceasedName: UILabel!
    @IBOutlet weak var deceasedImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
