//
//  MemberProfilePostTableViewCell.swift
//  Family Pic Will
//
//  Created by Apple on 27/05/2021.
//

import UIKit
import SwiftyJSON
import Alamofire
import AVKit
import AVFoundation
protocol PofilePostsDataForCcell : AnyObject{
  
    func seguetolikers(possttId:String)
    func seguetocomments(possttId:String)
    func playVideo(videoUrl: String , caption : String)
    func imageviewer(imageStr: String, caption : String)
}
class MemberProfilePostTableViewCell: UITableViewCell {
    var postid: String = ""
    var caption : String = ""
    var delegate : PofilePostsDataForCcell?
    var imageStrr : String = ""
    var typee: String = ""
    var islikedd: Int = 0
    var SECOND_MILLIS: Int = 1000
    var MINUTE_MILLIS: Int = 60
    var HOUR_MILLIS: Int = 60
    var DAY_MILLIS: Int = 24
    var post: Posts!
    var user: User?
    @IBOutlet weak var likebtnoutlet: UIButton!
    @IBOutlet weak var UserImage: UIImageView!
    @IBOutlet weak var CommentTextField: UITextField!
    @IBOutlet weak var VideoPlayerOutlet: UIButton!
 
    @IBOutlet weak var UserImage_Comment: UIImageView!
    @IBOutlet weak var postDescriptionLabel: UILabel!
    @IBOutlet weak var UserName: UILabel!
    
    
    @IBOutlet weak var PostTime: UILabel!
    
    @IBOutlet weak var PostLikes: UILabel!
    @IBOutlet weak var NumberOfComments_Post: UILabel!
    @IBOutlet weak var PostImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.MINUTE_MILLIS = self.MINUTE_MILLIS * self.SECOND_MILLIS
        self.HOUR_MILLIS = self.HOUR_MILLIS * self.MINUTE_MILLIS
        self.DAY_MILLIS = self.DAY_MILLIS * self.HOUR_MILLIS
        UserImage.layer.cornerRadius = UserImage.frame.size.width / 2
        UserImage.clipsToBounds = true
        UserImage_Comment.layer.cornerRadius = UserImage_Comment.frame.size.width / 2
        UserImage_Comment.clipsToBounds = true
        
//        self.UserImage_Comment.sd_setImage(with: URL(string: self.user!.profilepic ), placeholderImage: UIImage(named: "user2.png"))
        let tap = UITapGestureRecognizer(target: self, action: #selector((tapFunctionOnComment)))
        NumberOfComments_Post.addGestureRecognizer(tap)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector((tapFunctionOnLikes)))
        PostLikes.addGestureRecognizer(tap2)
        let imagetap = UITapGestureRecognizer(target: self, action: #selector((callimageviewer)))
        PostImage.isUserInteractionEnabled = true
        PostImage.addGestureRecognizer(imagetap)
    }
    
    @IBAction func commentBtn(_ sender: Any) {
        print("tap working")
    let title:String = postid
    delegate?.seguetocomments(possttId: title)
    }
    @objc func tapFunctionOnComment(sender:UITapGestureRecognizer) {
            print("tap working")
        let title:String = postid
        delegate?.seguetocomments(possttId: title)
        
        }
    @objc func tapFunctionOnLikes(sender:UITapGestureRecognizer) {
        let title:String = postid
       
        delegate?.seguetolikers(possttId: title)
       
        }
    @objc func callimageviewer(sender:UITapGestureRecognizer){
        print("123")
        delegate?.imageviewer(imageStr: imageStrr, caption: caption)
    }
    func checkImage(imageStr: String){
        self.imageStrr = imageStr
        if imageStr == ""{
            PostImage.isHidden = true
        }else{
            PostImage.isHidden = false
            let url = URL(string: imageStr)
            let data = try? Data(contentsOf: url!)

            if let imageData = data {
                PostImage.image = UIImage(data: imageData)
                VideoPlayerOutlet.isHidden = true
            }
            
        }
    }
    func checkVideo(imageStr: String, thuumb: String){
        self.imageStrr = imageStr
        if imageStr == ""{
            PostImage.isHidden = true
        }else{
            PostImage.isHidden = false
            VideoPlayerOutlet.isHidden = false
            
            let url = URL(string: thuumb)
            let data = try? Data(contentsOf: url!)

            if let imageData = data {
                PostImage.image = UIImage(data: imageData)
            }
      //      vidInIt(videoUrl : imageStr)

            
        }
    }
//    func vidInIt(videoUrl : String) {
//          let url = URL(string: videoUrl)
//        self.getThumbnailImageFromVideoUrl(url: url!){ (thumbnailImage) in
//          self.PostImage.image = thumbnailImage
//        }
//      }
//    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
//      DispatchQueue.global().async { //1
//        let asset = AVAsset(url: url) //2
//        let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
//        avAssetImageGenerator.appliesPreferredTrackTransform = true //4
//        let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
//        do {
//          let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
//          let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
//          DispatchQueue.main.async { //8
//            completion(thumbNailImage) //9
//          }
//        } catch {
//          print(error.localizedDescription) //10
//          DispatchQueue.main.async {
//            completion(nil) //11
//          }
//        }
//      }
//    }
    func getpost(){
        
        
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCall1post.Request(postid: postid)
                let requestService = SCService<ApiCall1post.Request,ApiCall1post.Response>()
                requestService.request("http://3.143.193.45:1337/api/getpostbyid", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
                   
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == true{
                        print(response.data)
                            self.PostLikes.text = "\(response.data![0].likecount)Likes"
                            self.NumberOfComments_Post.text = "View all \(response.data![0].commentcount) comments"
                        }else{
                           // self.showPromptAlert("Sorry!", message: response.message);
                        }
                        //self.performSegue(withIdentifier: "MainNav", sender: self)
                    case .failure(let error):
                        debugPrint(error)
                        
                        print(error)
        
                    }
        
        
                }
        
    }
    @IBAction func videoPlayerBtn(_ sender: Any) {
        delegate?.playVideo(videoUrl: self.imageStrr , caption : self.caption)
    }

    
    @IBAction func likeBtnAction(_ sender: Any) {
       
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCallLikes.Request(postid: postid)
                let requestService = SCService<ApiCallLikes.Request,ApiCallLikes.Response>()
                requestService.request("http://3.143.193.45:1337/api/likepost", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
        
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == true{
                        if response.message != "Post Disliked" {
                            self.likebtnoutlet.setImage(UIImage(named: "likeFilled"), for: .normal)
                        }else{
                            self.likebtnoutlet.setImage(UIImage(named: "11222"), for: .normal)
                        }
                        self.getpost()
                        }else{
                           // self.showPromptAlert("Sorry!", message: response.message);
                        }
                        //self.performSegue(withIdentifier: "MainNav", sender: self)
                    case .failure(let error):
                        debugPrint(error)
                      
                        print(error)
        
                    }
        
        
                }
        
    }
    
    
    
    
    
    @IBAction func commentSendBtn(_ sender: Any) {
        //let header = ["authorization":"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwYTRhYjEzNWIwZjFmNTlmNzUzYjVlOSIsImlhdCI6MTYyMTQwNDQzNSwiZXhwIjoxNjIzOTk2NDM1fQ.Y3gw3jQ8Ll48eoYraNKZNU4zQv3Nqcizoixvm2gHLg0"]
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCallAddComment.Request(comment: CommentTextField.text ?? "", postid: postid)
                let requestService = SCService<ApiCallAddComment.Request,ApiCallAddComment.Response>()
                requestService.request("http://3.143.193.45:1337/api/comment", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
        
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == true{
                        self.getpost()
                        //self.performSegue(withIdentifier: "MainNav", sender: self)
                        }else{
                          //  self.showPromptAlert("Sorry!", message: response.message);
                        }
                    case .failure(let error):
                        debugPrint(error)
                        
                        print(error)
        
                    }
        
        
                }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
