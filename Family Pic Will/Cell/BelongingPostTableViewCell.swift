//
//  BelongingPostTableViewCell.swift
//  Family Pic Will
//
//  Created by Apple on 09/05/2021.
//

import UIKit

class BelongingPostTableViewCell: UITableViewCell {

    @IBOutlet weak var belongingPostTime: UILabel!
    @IBOutlet weak var belongingViewComentLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configuredData(){
        belongingPostTime.font = UIFont(name: "Roboto-Regular", size: 12)
        belongingViewComentLabel.font = UIFont(name: "Montserrat-Thin", size: 14)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
