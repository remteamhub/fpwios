//
//  AlbumTableViewCell.swift
//  familywithwall
//
//  Created by Apple on 18/05/2021.
//

import UIKit
import AVKit
import AVFoundation
protocol AlbumDataCell: AnyObject{
    func buttonTap(AlbumId:String,type:String,Albumaname: String)
    func deleteBtn(AlbumId:String,Xtype:String)
}

class AlbumTableViewCell: UITableViewCell {
    var albuumID : String = ""
    var type : String = ""
    var delegate : AlbumDataCell?
    var albumn : String = ""
    @IBOutlet weak var albumImageView: UIView!
    @IBOutlet weak var ImagesNumber: UILabel!
    
    @IBOutlet weak var albumCell: UIButton!
    @IBOutlet weak var albumView: UIView!
    @IBOutlet weak var albumName: UILabel!
    @IBOutlet weak var albumImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tabelViewTapped))
        albumView.addGestureRecognizer(gestureRecognizer)
        albumImage.layer.cornerRadius = albumImage.frame.size.width / 2
        albumImage.clipsToBounds = true
        albumImageView.layer.cornerRadius = albumImageView.frame.size.width / 2
        albumImageView.clipsToBounds = true
    }
    @objc func tabelViewTapped(){
        print("working")
         delegate?.buttonTap(AlbumId:albuumID,type:type, Albumaname: albumn)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        shadow(vw: albumImageView)
        // Configure the view for the selected state
    }
    @IBAction func AlbumBtn(_ sender: Any) {
      print("abc")
        delegate?.deleteBtn(AlbumId:albuumID, Xtype: type)
    }
    
  
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)

        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }

        return nil
    }
    func checkVideo(imageStr: String){
        
        if imageStr == ""{
            albumImage.image = UIImage(named: "_add_image-512")
        }else{
            albumImage.isHidden = false
            let url = URL(string: imageStr)
            if let thumbnailImage = getThumbnailImage(forUrl: url!) {
                albumImage.image = thumbnailImage
                }
            
        }
    }
    func checkImage(imageStr: String){
        
        if imageStr == ""{
            albumImage.image = UIImage(named: "_add_image-512")
        }else{
            albumImage.isHidden = false
            let url = URL(string: imageStr)
            let data = try? Data(contentsOf: url!)

            if let imageData = data {
                albumImage.image = UIImage(data: imageData)
            }
            
        }
    }
    func shadow(vw: UIView) {
        vw.layer.shadowPath = UIBezierPath(rect: vw.bounds).cgPath
        vw.layer.shadowRadius = 6
        
        vw.layer.shadowOffset = .zero
        vw.layer.shadowOpacity = 0.9
    }

}
