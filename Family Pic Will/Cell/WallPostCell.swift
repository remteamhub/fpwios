//
//  WallPostCell.swift
//  familywithwall
//
//  Created by Apple on 23/04/2021.
//

import UIKit
import SwiftyJSON
import Alamofire
import AVKit
import AVFoundation
import SwiftUI
protocol DataForCcell : AnyObject{
    func buttonTap(possttId:String,imageSt:String,postText:String, postype:String)
    func seguetolikers(possttId:String)
    func seguetocomments(possttId:String)
    func playVideo(videoUrl: String , caption : String)
    func imageviewer(imageStr: String, caption : String)
}

class WallPostCell: UITableViewCell {
    var caption : String = ""
    var postid: String = ""
    var delegate : DataForCcell?
    var imageStrr : String = ""
    var typee: String = ""
    var isliked: Int = 0
    var SECOND_MILLIS: Int = 1000
    var MINUTE_MILLIS: Int = 60
    var HOUR_MILLIS: Int = 60
    var DAY_MILLIS: Int = 24
    var count1: Int = 0
    var count2: Int = 0
    var post: Posts!
    var user: User?
    @IBOutlet weak var loadingPost: UIActivityIndicatorView!
    @IBOutlet weak var likebtnoutlet: UIButton!
    @IBOutlet weak var UserImage: UIImageView!
    @IBOutlet weak var CommentTextField: UITextField!
    @IBOutlet weak var VideoPlayerOutlet: UIButton!
 
    @IBOutlet weak var likeCommentStack: UIStackView!
    @IBOutlet weak var commentBtnOutlet: UIButton!
    @IBOutlet weak var UserImage_Comment: UIImageView!
    @IBOutlet weak var postDescriptionLabel: UILabel!
    @IBOutlet weak var postIdLabel: UILabel!
    @IBOutlet weak var UserName: UILabel!
    
    @IBOutlet weak var optionBtnOutlet: UIButton!
    @IBOutlet weak var PostTime: UILabel!
    
    @IBOutlet weak var PostLikes: UILabel!
    @IBOutlet weak var NumberOfComments_Post: UILabel!
    @IBOutlet weak var PostImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.MINUTE_MILLIS = self.MINUTE_MILLIS * self.SECOND_MILLIS
        self.HOUR_MILLIS = self.HOUR_MILLIS * self.MINUTE_MILLIS
        self.DAY_MILLIS = self.DAY_MILLIS * self.HOUR_MILLIS
        self.getuser()
        self.loadingPost.startAnimating()
        self.loadingPost.isHidden = true
        UserImage.layer.cornerRadius = UserImage.frame.size.width / 2
        UserImage.clipsToBounds = true
        UserImage_Comment.layer.cornerRadius = UserImage_Comment.frame.size.width / 2
        UserImage_Comment.clipsToBounds = true
        
//        self.UserImage_Comment.sd_setImage(with: URL(string: self.user!.profilepic ), placeholderImage: UIImage(named: "user2.png"))
        let tap = UITapGestureRecognizer(target: self, action: #selector((tapFunctionOnComment)))
        NumberOfComments_Post.addGestureRecognizer(tap)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector((tapFunctionOnLikes)))
        PostLikes.addGestureRecognizer(tap2)
        let imagetap = UITapGestureRecognizer(target: self, action: #selector((callimageviewer)))
        PostImage.isUserInteractionEnabled = true
        PostImage.addGestureRecognizer(imagetap)
        
    }
    func setimageZoomer(){
        
        
    }
    func getuser(){
        if let data = UserDefaults.standard.data(forKey: "USER"){
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(User.self, from: data)
                print(user.self)
                
                self.user = user
               
                
            }catch{
                print("Error")
            }
        }
    }
    
    func currentTimeInMilliSeconds()-> Int
        {
            let currentDate = Date()
            let since1970 = currentDate.timeIntervalSince1970
            return Int(since1970 * 1000)
        }
    func getTime(`var` time: Int)-> String{
        var timm : Int = time
        if timm < 1000000000000 {
            timm  = timm * 1000
        }
        var now = currentTimeInMilliSeconds()
        if (timm > now || timm <= 0) {
            return " "
        }
        var diff = now - timm
        if (diff < MINUTE_MILLIS) {
                return "just now";
            } else if (diff < 2 * MINUTE_MILLIS) {
                return "a minute ago";
            } else if (diff < 60 * MINUTE_MILLIS) {
                return "\(diff / MINUTE_MILLIS) minutes ago";
            } /*else if (diff < 90 * MINUTE_MILLIS) {
                return "an hour ago";
            }*/ else if (diff < 24 * HOUR_MILLIS) {
                return "\(diff / HOUR_MILLIS) hours ago";
            } else if (diff < 48 * HOUR_MILLIS) {
                return "yesterday";
            } else {
                return "\(diff / DAY_MILLIS) days ago";
            }
        return ""
    }
//    func populateCell(){
//
//            self.isliked = self.post.isliked ?? 0
//        self.VideoPlayerOutlet.isHidden = true
//
//            if self.post.type == 1 || self.post.type == 0{
//                self.checkImage(imageStr: self.post.image)
//            }
//            if self.post.type == 2{
//                self.checkVideo(imageStr: self.post.video, thuumb: self.post.thumb)
//            }
//            self.UserImage_Comment.sd_setImage(with: URL(string: self.user!.profilepic ), placeholderImage: UIImage(named: "user2.png"))
//            if self.post.posterimage != "" && self.post.posterimage != nil{
//                let url = URL(string: self.post.posterimage ?? "")
//                let data = try? Data(contentsOf: url!)
//            self.UserImage.image = UIImage(data: data!)
//
//            }
//
//
//
//            self.checkOptionBtn(PosterId: self.post.posterid, CurrentId: self.user!.id)
//        //self.delegate = self
//            self.typee = "\(self.post.type)"
//            self.postIdLabel.text = self.post.id
//            self.postid = self.post.id
//
//            if self.post.isliked == 1 {
//            self.likebtnoutlet.setImage(UIImage(named: "likeFilled"), for: .normal)
//        }else{
//            self.likebtnoutlet.setImage(UIImage(named: "11222"), for: .normal)
//        }
//            self.NumberOfComments_Post.text = "View all \(self.post.commentcount) comments"
//            self.PostLikes.text = "\(self.post.likecount)Likes"
//            self.UserName.text = self.post.postername
//
//            self.postDescriptionLabel.text = self.post.text
//            self.PostTime.text = self.getTime(var: self.post.createdAt)
//        }

    
    @objc func tapFunctionOnComment(sender:UITapGestureRecognizer) {
            print("tap working")
        let title:String = postid
        delegate?.seguetocomments(possttId: title)
        
        }
    @objc func callimageviewer(sender:UITapGestureRecognizer){
        print("123")
        delegate?.imageviewer(imageStr: imageStrr, caption: caption)
    }
    
    @objc func tapFunctionOnLikes(sender:UITapGestureRecognizer) {
        let title:String = postid
       
        delegate?.seguetolikers(possttId: title)
       
        }
    func checkOptionBtn(PosterId: String, CurrentId: String){
        if PosterId == CurrentId {
            optionBtnOutlet.isHidden = false
        }
        if PosterId != CurrentId{
            self.optionBtnOutlet.isHidden = true
        }
    }
    func checkUserImage(imageStr: String){
        
        if imageStr == ""{
            UserImage.image = UIImage(named: "userprofile")
        }else{
           
           
            let url = URL(string: imageStr)
      
                let data = try? Data(contentsOf: url!)
                   // self.PostImage.image = UIImage(data: data!)
                   // self.VideoPlayerOutlet.isHidden = true
                if let imageData = data {
                    UserImage.image = UIImage(data: imageData)
                
            }
            
        }
    }
    func checkImage(imageStr: String){
        self.imageStrr = imageStr
        if imageStr == ""{
            PostImage.isHidden = true
//            addConstraints()
        }else{
            PostImage.isHidden = false
           
            let url = URL(string: imageStr)
      
                let data = try? Data(contentsOf: url!)
                   // self.PostImage.image = UIImage(data: data!)
                   // self.VideoPlayerOutlet.isHidden = true
                if let imageData = data {
                PostImage.image = UIImage(data: imageData)
                VideoPlayerOutlet.isHidden = true
            }
            
        }
    }
//    func addConstraints1(){
//
//        var constraints = [NSLayoutConstraint]()
//        likeCommentStack.translatesAutoresizingMaskIntoConstraints = false
//        constraints.append(likeCommentStack.topAnchor.constraint(equalTo: postDescriptionLabel.bottomAnchor, constant: 8))
//        NSLayoutConstraint.activate(constraints)
//        print("qqqqqqqqqqqqqqqqqqqqqqqqqq")
//
//    }
//    func addConstraints(imageStrr: String){
//
//        var constraints = [NSLayoutConstraint]()
//        if imageStrr == ""{
//            likeCommentStack.translatesAutoresizingMaskIntoConstraints = false
//            constraints.append(likeCommentStack.topAnchor.constraint(equalTo: postDescriptionLabel.bottomAnchor, constant: 8))
//            let con = likeCommentStack.topAnchor.constraint(equalTo: postDescriptionLabel.bottomAnchor, constant: 8)
//            con.priority = UILayoutPriority(999)
//            NSLayoutConstraint.activate(constraints)
//
//        }else if imageStrr != ""{
//            var constraints = [NSLayoutConstraint]()
//            likeCommentStack.translatesAutoresizingMaskIntoConstraints = false
//            constraints.append(likeCommentStack.topAnchor.constraint(equalTo: PostImage.bottomAnchor, constant: 8))
//            let con = likeCommentStack.topAnchor.constraint(equalTo: PostImage.bottomAnchor, constant: 8)
//            con.priority = UILayoutPriority(1000)
//            NSLayoutConstraint.activate(constraints)
//
//        }
//
//    }
    
    @IBAction func commentBtn(_ sender: Any) {
        print("tap working")
    let title:String = postid
    delegate?.seguetocomments(possttId: title)
    }
    
    
    
    
    
    @IBAction func videoPlayerBtn(_ sender: Any) {
        delegate?.playVideo(videoUrl: self.imageStrr , caption : self.caption)
    }
    
    
    
    func checkVideo(imageStr: String, thuumb: String){
        self.imageStrr = imageStr
        if imageStr == ""{
            PostImage.isHidden = true
        }else{
            PostImage.isHidden = false
            VideoPlayerOutlet.isHidden = false
          
          //  vidInIt(videoUrl : thuumb)
            let url = URL(string: thuumb)
          let data = try? Data(contentsOf: url!)
         // self.PostImage.image = UIImage(data: data!)
                   
                        
                    
                    
                
            

            if let imageData = data {
                PostImage.image = UIImage(data: imageData)
            }
            
        }
    }
    
    
    func vidInIt(videoUrl : String) {
          let url = URL(string: videoUrl)
        self.getThumbnailImageFromVideoUrl(url: url!){ (thumbnailImage) in
          self.PostImage.image = thumbnailImage
        }
      }
      //MARK -thumbnail
      func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
          let asset = AVAsset(url: url) //2
          let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
          avAssetImageGenerator.appliesPreferredTrackTransform = true //4
          let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
          do {
            let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
            let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
            DispatchQueue.main.async { //8
              completion(thumbNailImage) //9
            }
          } catch {
            print(error.localizedDescription) //10
            DispatchQueue.main.async {
              completion(nil) //11
            }
          }
        }
      }
    func getpost(){
        
        
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCall1post.Request(postid: postid)
                let requestService = SCService<ApiCall1post.Request,ApiCall1post.Response>()
                requestService.request("http://3.143.193.45:1337/api/getpostbyid", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
                   
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == true{
                        print(response.data)
                        
                        //    print(response.data.)
                            self.PostLikes.text = "\(response.data![0].likecount)Likes"
                            self.NumberOfComments_Post.text = "View all \(response.data![0].commentcount) comments"
                        }else{
                          //  self.showPromptAlert("Sorry!", message: response.message);
                        }
                        //self.performSegue(withIdentifier: "MainNav", sender: self)
                    case .failure(let error):
                        debugPrint(error)
                        
                        print(error)
        
                    }
        
        
                }
        
    }

    
    @IBAction func likeBtnAction(_ sender: Any) {
        
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCallLikes.Request(postid: postid)
                let requestService = SCService<ApiCallLikes.Request,ApiCallLikes.Response>()
                requestService.request("http://3.143.193.45:1337/api/likepost", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == true{
                        if response.message != "Post Disliked" {
                            self.likebtnoutlet.setImage(UIImage(named: "likeFilled"), for: .normal)
                        }else{
                            self.likebtnoutlet.setImage(UIImage(named: "11222"), for: .normal)
                        }
                        
                        self.getpost()
                        }else{
                           // self.showPromptAlert("Sorry!", message: response.message);
                        }
                        //self.performSegue(withIdentifier: "MainNav", sender: self)
                    case .failure(let error):
                        debugPrint(error)
                        print(error)
                    }
                }
        
    }
    
    
    
    
    
    @IBAction func commentSendBtn(_ sender: Any) {
        //let header = ["authorization":"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwYTRhYjEzNWIwZjFmNTlmNzUzYjVlOSIsImlhdCI6MTYyMTQwNDQzNSwiZXhwIjoxNjIzOTk2NDM1fQ.Y3gw3jQ8Ll48eoYraNKZNU4zQv3Nqcizoixvm2gHLg0"]
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCallAddComment.Request(comment: CommentTextField.text ?? "", postid: postid)
                let requestService = SCService<ApiCallAddComment.Request,ApiCallAddComment.Response>()
                requestService.request("http://3.143.193.45:1337/api/comment", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
        
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == true{
                        self.getpost()
                        self.CommentTextField.text?.removeAll()
                        }else{
                           // self.showPromptAlert("Sorry!", message: response.message);
                        }
                        //self.performSegue(withIdentifier: "MainNav", sender: self)
                    case .failure(let error):
                        debugPrint(error)
                        
                        print(error)
        
                    }
        
        
                }
    }
    @IBAction func postOptionBtn(_ sender: Any) {
        
        let title:String = postid
        delegate?.buttonTap(possttId: title, imageSt: imageStrr, postText: postDescriptionLabel.text ?? "", postype: typee)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

