//
//  NotificationViewCell.swift
//  familywithwall
//
//  Created by Apple on 26/04/2021.
//

import UIKit

class NotificationViewCell: UITableViewCell {
    
    @IBOutlet weak var notificationCellview: UIView!
    
    
    @IBOutlet weak var notificationlabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userImage.layer.cornerRadius = userImage.frame.size.width / 2
        userImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateNotificationCell(by check: Bool){
        
        if check{
            notificationCellview.backgroundColor = UIColor(red: 230/255.0, green: 242/255.0, blue: 255/255.0, alpha: 1.0)
            
            
            
        }else{
            notificationCellview.backgroundColor = UIColor(red: 248/255.0, green: 248/255.0, blue: 248/255.0, alpha: 1.0)
            
        }
        
    }

}
