//
//  SideOptionTableViewCell.swift
//  FPW
//
//  Created by Apple on 30/04/2021.
//

import UIKit

class SideOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var labelOptionName: UILabel!
   
    @IBOutlet weak var imageOption: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelOptionName.font = UIFont(name: "Roboto-Regular", size: 35)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
