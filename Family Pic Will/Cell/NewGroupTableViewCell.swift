//
//  NewGroupTableViewCell.swift
//  Family Pic Will
//
//  Created by mac on 9/9/21.
//

import UIKit

class NewGroupTableViewCell: UITableViewCell {
//    typealias selectionCallBack = (_ selectedIndex: Int) -> Void
//    var callBack:selectionCallBack?
    
    
    var returnValue: (( _ value: Bool)->())?
        override func awakeFromNib() {
            super.awakeFromNib()
            var gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(cellViewTapped))
            view.isUserInteractionEnabled = true
            view.addGestureRecognizer(gestureRecognizer)
        }
    
    @objc func cellViewTapped(){
        if(self.isselected == false)
        {
         //   radio.setImage(UIImage(named: "image1.png"), for: UIControl.State.normal)
            isselected = true
            check.text = "\u{f00c}"
            returnValue?(isselected)
            //self.callBack?(1)
        }
        else
        {
          //  radio.setImage(UIImage(named: "image.png"), for: UIControl.State.normal)
            isselected = false
            check.text = "\u{f563}"
            returnValue?(isselected)
            //self.callBack?(2)
        }
        
    }

    @IBOutlet weak var memberImage: UIImageView!
    @IBOutlet weak var memberName: UILabel!
    @IBOutlet weak var radio: UIButton!
    @IBOutlet weak var check: UILabel!
    public var isselected = false
    public var member: User?
   
    @IBOutlet weak var view: UIView!
    @IBAction func selectbutton(_ sender: Any) {
        
        if(self.isselected == false)
        {
         //   radio.setImage(UIImage(named: "image1.png"), for: UIControl.State.normal)
            isselected = true
            check.text = "\u{f00c}"
            returnValue?(isselected)
            //self.callBack?(1)
        }
        else
        {
          //  radio.setImage(UIImage(named: "image.png"), for: UIControl.State.normal)
            isselected = false
            check.text = "\u{f563}"
            returnValue?(isselected)
            //self.callBack?(2)
        }
        
        
        
    }
    

    

}
