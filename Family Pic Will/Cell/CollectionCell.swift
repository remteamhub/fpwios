//
//  CollectionCell.swift
//  Bdaynotif
//
//  Created by Coder Crew on 5/20/21.
//

import UIKit
protocol CollectionDataCell: AnyObject{
    func buttonTap(Id:String,Xtype:String)

}
class CollectionCell: UICollectionViewCell {
    var image_Video: String = ""
    var delegate : CollectionDataCell?
    var type : String = ""
    var caption : String = ""
    var id : String = ""
    @IBOutlet weak var CollectionImg: UIImageView!
    
    @IBOutlet weak var removeMediaBtn: UIButton!
    
    
    
    @IBAction func Image_VideoDeleteBtn(_ sender: Any) {
    
        delegate?.buttonTap(Id:id,Xtype:type)
    }
    
}
