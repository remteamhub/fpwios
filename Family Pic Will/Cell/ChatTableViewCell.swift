//
//  MessageTableViewCell.swift
//  familywithwall
//
//  Created by Apple on 28/04/2021.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    @IBOutlet weak var messageBackgroundView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messageTime: UILabel!
    var trailingConstraint: NSLayoutConstraint!
    var leadingConstraint: NSLayoutConstraint!
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        messageLabel.text = nil
        trailingConstraint.isActive = false
        leadingConstraint.isActive = false
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateMessageCell(by message: MessageData){
        messageBackgroundView.layer.cornerRadius = 16
        messageBackgroundView.clipsToBounds = true
        trailingConstraint = messageBackgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
        leadingConstraint = messageBackgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20)
        messageLabel.text = message.text
        if message.isFirstUser{
            messageBackgroundView.backgroundColor = UIColor(red: 89/255.0, green: 164/255.0, blue: 248/255.0, alpha: 1.0)
            trailingConstraint.isActive = true
            messageLabel.textAlignment = .left
            
            
        }else{
            messageBackgroundView.backgroundColor = UIColor(red: 230/255.0, green: 229/255.0, blue: 235/255.0, alpha: 1.0)
            messageLabel.textColor = UIColor.black
            leadingConstraint.isActive = true
            messageLabel.textAlignment = .left
        }
        
    }

}
