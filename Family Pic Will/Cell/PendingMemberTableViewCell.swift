//
//  PendingMemberTableViewCell.swift
//  Family Pic Will
//
//  Created by Apple on 21/05/2021.
//

import UIKit

class PendingMemberTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var memberNameLabel: UILabel!
    @IBOutlet weak var acceptLabel: UILabel!
    @IBOutlet weak var declineLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        memberNameLabel.font = UIFont(name: "Open Sans Regular", size: 18)
        //acceptLabel.font = UIFont(name: "Roboto-Regular", size: self.font.pointSize)
        //declineLabel.font = UIFont(name: "Roboto-Regular", size: self.font.pointSize)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
