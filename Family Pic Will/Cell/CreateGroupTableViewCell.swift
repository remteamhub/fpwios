//
//  CreateGroupTableViewCell.swift
//  Family Pic Will
//
//  Created by mac on 9/10/21.
//

import UIKit

class CreateGroupTableViewCell: UITableViewCell {

    @IBOutlet weak var MemberImage: UIImageView!
    @IBOutlet weak var MemberName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
