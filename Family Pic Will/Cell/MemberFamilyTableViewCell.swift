//
//  MemberFamilyTableViewCell.swift
//  FPW
//
//  Created by Apple on 29/04/2021.
//

import UIKit
import Alamofire
import SDWebImage
protocol DataForCell : AnyObject{
    func buttonTap(title:Int)
}
class MemberFamilyTableViewCell: UITableViewCell {
    var delegate : DataForCell?
    @IBOutlet weak var nameMember: UILabel!
    @IBOutlet weak var imageViewMember: UIImageView!
    @IBOutlet weak var dropdownMember: UIButton!
    @IBOutlet weak var viewMember: UIView!
    var row:Int?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    func configuredData(row : Int, member : User){
        self.row = row
        nameMember.font = UIFont(name: "Open Sans Regular", size: 20)
        //nameMember.text = "\(row)"
        if member.profilepic != "" {
            imageViewMember.sd_setImage(with: URL(string: member.profilepic ), placeholderImage: UIImage(named: "placeholder.png"))
        }
//        AF.download(member.profilepic ?? "").responseData { response in
//              switch response.result {
//              case let .success(value):
//                //UserDefaults.standard.setValue(value, forKey: "image")
//                 let image = UIImage(data: value)
//                self.imageViewMember.image = image
//              case let .failure(error):
//                return
//              }
//            }
    }
    @IBAction func dropdownMemberClicked(_ sender: Any) {
        //let title:String = "\(row ?? 0)"
        delegate?.buttonTap(title: row!)
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    

}
