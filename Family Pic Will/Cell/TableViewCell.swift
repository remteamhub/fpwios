//
//  TableViewCell.swift
//  Family Pic Will
//
//  Created by mac on 9/1/21.
//

import UIKit
//protocol DataForCccell : AnyObject{
//
//    func playVideo(videoUrl: String , receiver : String, albumname: String)
//}
class TableViewCell: UITableViewCell {
    //MARK:- Outlets
//    var delegate : DataForCccell?
//    var videoUrl = ""
//    var receiver = ""
//    var albumname = ""
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var playbutton: UIButton!
    @IBOutlet weak var chatLabel: UILabel!
    @IBOutlet weak var chatImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        chatLabel.layer.cornerRadius = 6
        chatLabel.layer.masksToBounds = true
        // Initialization code
        playbutton.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    @IBAction func playBtnAction(_ sender: Any) {
//        delegate?.playVideo(videoUrl: self.videoUrl , receiver: self.receiver , albumname: self.albumname)
//    }
   
}

