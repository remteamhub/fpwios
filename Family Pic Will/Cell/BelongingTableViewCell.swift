//
//  BelongingTableViewCell.swift
//  Family Pic Will
//
//  Created by Apple on 09/05/2021.
//

import UIKit

class BelongingTableViewCell: UITableViewCell {
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var viewCommentLabel: UILabel!
    @IBOutlet weak var beloningPostTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configuredData(){
        beloningPostTime.font = UIFont(name: "Roboto-Regular", size: 12)
        viewCommentLabel.font = UIFont(name: "Montserrat-Thin", size: 14)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
