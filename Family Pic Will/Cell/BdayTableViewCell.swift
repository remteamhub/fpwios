//
//  BdayTableViewCell.swift
//  Bdaynotif
//
//  Created by Coder Crew on 4/29/21.
//

import UIKit
protocol BirthdayDataForcell : AnyObject{
    func buttonTap(UserId:String, familyCode:String)
    
}
class BdayTableViewCell: UITableViewCell {
    @IBOutlet weak var BdayView: UIView!
    @IBOutlet weak var Bdaycellimage: UIImageView!
    @IBOutlet weak var Bdaycelllabel: UILabel!
    @IBOutlet weak var nameLable: UILabel!
    var delegate : BirthdayDataForcell?
    var userid: String = ""
    var familycode: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Bdaycellimage.layer.cornerRadius =   25  //Bdaycellimage.frame.size.width / 2
        Bdaycellimage.clipsToBounds = true
        
    }
    func checkImage(imageStr: String){
        
        if imageStr == ""{
            Bdaycellimage.isHidden = true
        }else{
            Bdaycellimage.isHidden = false
            let url = URL(string: imageStr)
            let data = try? Data(contentsOf: url!)

            if let imageData = data {
                Bdaycellimage.image = UIImage(data: imageData)
                
            }
            
        }
    }
    @IBAction func wishBtnAction(_ sender: Any) {
        
        delegate?.buttonTap(UserId: userid, familyCode: familycode)
   
        
        
        
    }
   

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
