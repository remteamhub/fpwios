//
//  CommentsCell.swift
//  familywithwall
//
//  Created by Apple on 03/05/2021.
//

import UIKit
import SwiftyJSON
import Alamofire
protocol DataForCell2 : AnyObject{
    func buttonTap(commentId:String)
}
class CommentsCell: UITableViewCell {
    var commentid: String = ""
    var delegate : DataForCell2?
    @IBOutlet weak var userimage: UIImageView!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userimage.layer.cornerRadius = userimage.frame.size.width / 2
        userimage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func dropdownMemberClicked(_ sender: Any) {
        let title:String = commentid
        delegate?.buttonTap(commentId: title)
        
    }
  
    
    //need
    func checkOptionBtn(PosterId: String, CommenterId: String){
        if PosterId == CommenterId {
            commentButton.isHidden = false
        }else{
            commentButton.isHidden = true
        }
    }
    
   
    
}
