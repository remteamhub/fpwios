//
//  SuggestedUserTableViewCell.swift
//  Family Pic Will
//
//  Created by Apple on 20/05/2021.
//

import UIKit

class SuggestedUserTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        userName.font = UIFont(name: "Montserrat-SemiBold", size: 15)
        cellView.layer.borderWidth = 1
        cellView.layer.borderColor = UIColor(red: 235/255, green: 235/255, blue: 235/255,alpha: 1).cgColor
        // Configure the view for the selected state
    }

}
