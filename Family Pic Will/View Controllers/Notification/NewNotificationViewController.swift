//
//  NewNotificationViewController.swift
//  Family Pic Will
//
//  Created by Apple on 16/09/2021.
//

import UIKit
import SwiftyJSON
import Alamofire
class NewNotificationViewController: UIViewController {
    var image: String = ""
    var caption: String = ""
    var videoUrl: String = ""
    var SECOND_MILLIS: Int = 1000
    var MINUTE_MILLIS: Int = 60
    var HOUR_MILLIS: Int = 60
    var DAY_MILLIS: Int = 24
    var user: User?
    var posttid : String = ""
    var labelll : String = "Notification"
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var sendCommentButton: UIButton!
    @IBOutlet weak var addComment: UITextField!
    @IBOutlet weak var commentProfileImage: UIImageView!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var timePosted: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    var postid : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.SECOND_MILLIS = 1000
        self.MINUTE_MILLIS = 60
        self.HOUR_MILLIS = 60
        self.DAY_MILLIS = 24
        self.MINUTE_MILLIS = self.MINUTE_MILLIS * self.SECOND_MILLIS
        self.HOUR_MILLIS = self.HOUR_MILLIS * self.MINUTE_MILLIS
        self.DAY_MILLIS = self.DAY_MILLIS * self.HOUR_MILLIS
        getpost()
        getuser()
        let tap2 = UITapGestureRecognizer(target: self, action: #selector((tapFunctionOnLikes)))
        likesLabel.addGestureRecognizer(tap2)
        let tap = UITapGestureRecognizer(target: self, action: #selector((tapFunctionOnComment)))
        commentsLabel.addGestureRecognizer(tap)
        let imagetap = UITapGestureRecognizer(target: self, action: #selector((imageviewer)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(imagetap)
        self.commentProfileImage.sd_setImage(with: URL(string: self.user!.profilepic ), placeholderImage: UIImage(named: "user2.png"))
        commentProfileImage.layer.cornerRadius = commentProfileImage.frame.size.width / 2
        commentProfileImage.clipsToBounds = true
    }

    @objc func imageviewer(){
       
    
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "photoShow") as? detailedImg else {
              return
            }
        menuViewController.name = image
        menuViewController.captionTxt = caption
        menuViewController.albumname = "Notification"
        menuViewController.modalPresentationStyle = .overFullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
    }
    
    @objc func tapFunctionOnComment(){
        
            posttid = postid
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "commentStoryboardID") as? CommentsViewController else {
                  return
                }
           
            
            menuViewController.postId = postid
            menuViewController.user = self.user
            menuViewController.labelll = labelll
            menuViewController.modalPresentationStyle = .overFullScreen
             self.present(menuViewController, animated: true, completion: nil)

        

    }
    
    @objc func tapFunctionOnLikes(){
        
        posttid = postid
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "likeStoryboardID") as? PostLikesViewController else {
              return
            }
       
        
        menuViewController.postId = posttid
        menuViewController.labelll = labelll
        menuViewController.modalPresentationStyle = .overFullScreen
         self.present(menuViewController, animated: true, completion: nil)
        
    }
    
    func getuser(){
        if let data = UserDefaults.standard.data(forKey: "USER"){
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(User.self, from: data)
                
                self.user = user
               
                
            }catch{
            }
        }
    }
    
    @IBAction func toComments(_ sender: Any) {
        
            posttid = postid
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "commentStoryboardID") as? CommentsViewController else {
                  return
                }
           
            
            menuViewController.postId = postid
            menuViewController.user = self.user
            menuViewController.labelll = labelll
            menuViewController.modalPresentationStyle = .overFullScreen
            
             
             self.present(menuViewController, animated: true, completion: nil)

        
        
    }
    


    
    @IBAction func likeBtn(_ sender: Any) {
        
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCallLikes.Request(postid: postid)
                let requestService = SCService<ApiCallLikes.Request,ApiCallLikes.Response>()
                requestService.request("http://3.143.193.45:1337/api/likepost", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == true{
                        if response.message != "Post Disliked" {
                            self.likeButton.setImage(UIImage(named: "likeFilled"), for: .normal)
                        }else{
                            self.likeButton.setImage(UIImage(named: "11222"), for: .normal)
                        }
                        
                        self.getpost()
                        }else{
                        }
                    case .failure(let error):
                        debugPrint(error)
                        print(error)
                    }
                }
    }
    
    
    
    @IBAction func backButton(_ sender: Any) {
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "BirthdayStoryboardID")
        as? BirthDayViewController else {
            return
        }
        
        let presentingVC = self.presentingViewController
        menuViewController.modalPresentationStyle = .fullScreen
        dismiss(animated: true) {
            presentingVC!.present(menuViewController, animated: true,completion: nil)
        }
    }
    func currentTimeInMilliSeconds()-> Int
        {
            let currentDate = Date()
            let since1970 = currentDate.timeIntervalSince1970
            return Int(since1970 * 1000)
        }
    func getTime(`var` time: Int)-> String{
        var timm : Int = time
        if timm < 1000000000000 {
            timm  = timm * 1000
        }
        var now = currentTimeInMilliSeconds()
        if (timm > now || timm <= 0) {
            return " "
        }
        var diff = now - timm
        if (diff < MINUTE_MILLIS) {
                return "just now";
            } else if (diff < 2 * MINUTE_MILLIS) {
                return "a minute ago";
            } else if (diff < 60 * MINUTE_MILLIS) {
                return "\(diff / MINUTE_MILLIS) minutes ago";
            } /*else if (diff < 90 * MINUTE_MILLIS) {
                return "an hour ago";
            }*/ else if (diff < 24 * HOUR_MILLIS) {
                return "\(diff / HOUR_MILLIS) hours ago";
            } else if (diff < 48 * HOUR_MILLIS) {
                return "yesterday";
            } else {
                return "\(diff / DAY_MILLIS) days ago";
            }
        return ""
    }
    
    func getpost(){
        
        
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCall1post.Request(postid: self.postid)
                let requestService = SCService<ApiCall1post.Request,ApiCall1post.Response>()
                requestService.request("http://3.143.193.45:1337/api/getpostbyid", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
                   
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == true{
                        print(response.data)
                            self.image = response.data![0].image
                            self.caption = response.data![0].caption ?? ""
                            self.videoUrl = response.data![0].video
                            self.timePosted.text = self.getTime(var: response.data![0].createdAt)
                            self.likesLabel.text = "\(response.data![0].likecount)Likes"
                            self.commentsLabel.text = "View all \(response.data![0].commentcount) comments"
                            self.userName.text = response.data![0].postername
                            
                            self.profileImage.sd_setImage(with: URL(string: response.data![0].posterimage! ), placeholderImage: UIImage(named: "userprofile.png"))
                            self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2
                            self.profileImage.clipsToBounds = true
                            self.textLabel.text = response.data![0].text
                            if(response.data![0].type == 1)
                            {
                                self.imageView.sd_setImage(with: URL(string: response.data![0].image ), placeholderImage: UIImage(named: "userprofile.png"))
                                self.playButton.isHidden = true
                            }
                            else if (response.data![0].type == 2)
                            {
                                self.imageView.sd_setImage(with: URL(string: response.data![0].thumb ), placeholderImage: UIImage(named: "userprofile.png"))
                                self.playButton.isHidden = false
                            }
                            else
                            {
                                self.imageView.isHidden = true
                                self.playButton.isHidden = true
                            }
                        }else{
                          //  self.showPromptAlert("Sorry!", message: response.message);
                        }
                        //self.performSegue(withIdentifier: "MainNav", sender: self)
                    case .failure(let error):
                        debugPrint(error)
                        
                        print(error)
        
                    }
        
        
                }
        
    }
    
    
    
    @IBAction func commentSendBtn(_ sender: Any) {
    
            let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
            let like = ApiCallAddComment.Request(comment: addComment.text ?? "", postid: postid)
                    let requestService = SCService<ApiCallAddComment.Request,ApiCallAddComment.Response>()
                    requestService.request("http://3.143.193.45:1337/api/comment", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                        (result) in
            
                        switch result {
                        case .success(let response):
                            debugPrint(response)
                            print(response.status)
                            print(response.message)
                            if response.status == true{
                            self.getpost()
                            self.addComment.text?.removeAll()
                            }else{
                            }
                        case .failure(let error):
                            debugPrint(error)
                            
                            print(error)
            
                        }
            
            
                    }
        }
    
    
    @IBAction func playBtnTap(_ sender: Any) {
        
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "VideoPlayerViewController") as? VideoPlayerViewController else {return}
        menuViewController.urlString = String (videoUrl)
        menuViewController.captionTxtVideo = caption
        menuViewController.modalPresentationStyle = .overFullScreen
        self.present(menuViewController, animated: true, completion: nil)
        
    }
    
    }
    
    
    
    

   


