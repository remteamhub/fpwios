//
//  NotificationViewController.swift
//  familywithwall
//
//  Created by Apple on 26/04/2021.
//

import UIKit

class NotificationViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var NotificationTabelView: UITableView!
    
    
    var notifications: [NotificationData] = [NotificationData(userimage: "windranger", username: "Wind Ranger", checked: true, notification: "Like"),NotificationData(userimage: "windranger", username: "Wind Ranger", checked: true, notification: "Comment"),NotificationData(userimage: "qop1", username: "Queen Of Pain", checked: false, notification: "Like")]
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationTabelView.delegate = self
        NotificationTabelView.dataSource = self
        // Do any additional setup after loading the view.
    }
    @IBAction func notificationoptionbtn(_ sender: Any) {
        postOptionAction(var: "Delete")
    }
    func postOptionAction(`var` str1:String) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: str1, style: .default, handler: nil))
        
        
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        //If you want work actionsheet on ipad then you have to use popoverPresentationController to present the actionsheet, otherwise app will crash in iPad
//        switch UIDevice.current.userInterfaceIdiom {
//        case .pad:
//            alert.popoverPresentationController?.sourceView = sender
//            alert.popoverPresentationController?.sourceRect = sender.bounds
//            alert.popoverPresentationController?.permittedArrowDirections = .up
//        default:
//            break
//        }
        
        self.present(alert, animated: true, completion: nil)
        
      }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = NotificationTabelView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationViewCell
        
        
        let attributsBold = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .regular)]
            let attributsNormal = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16, weight: .bold)]
        let attributedString = NSMutableAttributedString(string: notifications[indexPath.row].username, attributes:attributsNormal)
        let boldStringPart = NSMutableAttributedString(string: " \(notifications[indexPath.row].notification) your post", attributes:attributsBold)
            attributedString.append(boldStringPart)
        
        cell.userImage.image = UIImage(named: notifications[indexPath.row].userimage)
          
        cell.notificationlabel.attributedText = attributedString
        cell.updateNotificationCell(by: notifications[indexPath.row].checked)

        return cell
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }

}
