//
//  CustomVPViewController.swift
//  Family Pic Will
//
//  Created by Apple on 29/06/2021.
//

import UIKit
import AVFoundation
import AVKit

class CustomVPViewController: AVPlayerViewController, AVPlayerViewControllerDelegate {

    
    var playerView = AVPlayer()
    var playerViewController = AVPlayerViewController()
    
    @IBOutlet weak var captionLabel: UILabel!
    var urlString : String = ""
    var captionTxtVideo : String = ""
    var albumId: String  = ""
    var albumtype: String = ""
    var albumname: String = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.showsPlaybackControls = true
        print("view did load \(urlString)")
    //    playvideo(videoUrl: urlString)
        addVideoPlayer(videoUrl: URL(string: urlString)!)
        
        
        
      
        
        
//        captionLabel.text = captionTxtVideo
    }
    
//
//    func playVideo(){
//
//        let videoURL = URL(string: urlString)
//        let player = AVPlayer(url: videoURL!)
////        let playerController = AVPlayerViewController()
////        playerController.player = player
////        playerController.delegate = self
////
//        let playerLayer = AVPlayerLayer(player: player)
//
//
//
//        playerLayer.frame = self.view.bounds
//        playerLayer.videoGravity = .resizeAspect
//
//        self.view.layer.addSublayer(playerLayer)
//        player.play()
//    }
    
    func playvideo(videoUrl : String) {
        
        print("play video .... \(videoUrl)")
        let url: URL = URL(string: videoUrl)!
        playerView = AVPlayer(url: url)
        self.player = playerView
        self.player?.play()
        
        
        
        
//
//        let lbl = UILabel(frame: CGRect(x: self.view.frame.size.width / 2, y: self.view.frame.size.height / 2, width: 100, height: 250))
//        lbl.textAlignment = .center //For center alignment
//        lbl.text = "This is my label fdsjhfg sjdg dfgdfgdfjgdjfhg jdfjgdfgdf end..."
//        lbl.textColor = .white
//        lbl.backgroundColor = .lightGray//If required
//        lbl.font = UIFont.systemFont(ofSize: 30)
//
//        //To display multiple lines in label
//        lbl.numberOfLines = 0 //If you want to display only 2 lines replace 0(Zero) with 2.
//        lbl.lineBreakMode = .byWordWrapping //Word Wrap
//        // OR
//        lbl.lineBreakMode = .byCharWrapping //Charactor Wrap
//
//        lbl.sizeToFit()//If required
        
        
        
        
        
        
//        // CGRectMake has been deprecated - and should be let, not var
//         let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 80))
//
//         // you will probably want to set the font (remember to use Dynamic Type!)
//         label.font = UIFont.preferredFont(forTextStyle: .footnote)
//
//         // and set the text color too - remember good contrast
//         label.textColor = .white
//
//         // may not be necessary (e.g., if the width & height match the superview)
//         // if you do need to center, CGPointMake has been deprecated, so use this
//         label.center = CGPoint(x: 160, y: 284)
//
//         // this changed in Swift 3 (much better, no?)
//         label.textAlignment = .center
//
//         label.text = "I am a test label dsfkjs fsdjkfjsfhs lk sd f"
//
//         self.view.addSubview(label)
        
        
       // let playerLayer = AVPlayerLayer(player: player)
        
//        let label1 = CATextLayer()
//                label1.frame = labelFarme
//                label1.string = "some string"
//                layer1.addSublayer(label)
        
//        let label = UILabel()
//        label.font = UIFont(name: "Helvetica-Bold", size: 50)
//        label.text = "Hello sdjahfj hfjkh adsljfh asdfa"
//        label.backgroundColor = .white
//        label.textColor = UIColor.red
//        label.isHidden = false
        
        
        
//
//        let label3 = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
//        label3.center = CGPoint(x: 160, y: 285)
//        label3.textAlignment = .center
//        label3.text = "I'm a test label"
        

        

       // playerLayer.addSublayer(label.layer)
        
      //  playerLayer.addSublayer(CATextLayer(layer: "Strinf anfid sanfioasndiofnas iofnioa"))
        
      
        
        
    
        //playerViewController.player = playerView
       
       // self.present(playerViewController, animated: true)
        //self.playerViewController.player?.play()
        
   
        
        
        
        
        
        

    }
    
    func addVideoPlayer(videoUrl: URL) {
        let player = AVPlayer(url: videoUrl)
        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        layer.backgroundColor = UIColor.white.cgColor
        layer.frame = view.bounds
        layer.videoGravity = .resizeAspectFill
        view.layer.sublayers?
            .filter { $0 is AVPlayerLayer }
            .forEach { $0.removeFromSuperlayer() }
        view.layer.addSublayer(layer)
        self.player = player
        self.player?.play()
        
        
        
        
//        print("play video .... \(videoUrl)")
//        let url: URL = URL(string: videoUrl)!
//        playerView = AVPlayer(url: url)
//        self.player = playerView
//        self.player?.play()
    }
}
