//
//  detailedImg.swift
//  Bdaynotif
//
//  Created by Coder Crew on 5/21/21.
//

import UIKit

class detailedImg: UIViewController {
    
    
    var membername = ""
    var memberimage = ""
    var name = ""
    var captionTxt = ""
    var albumId: String  = ""
    var albumtype: String = ""
    var albumname: String = ""
    var member : User?
    var receiver: String = ""

    @IBOutlet weak var caption: UILabel!
    
    
    @IBOutlet weak var Imgdetai: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: name)
        let data = try? Data(contentsOf: url!)
        print("data ==== \(String(describing: data))")
        
        print("checking for caption ========= \(data)")

        
        
        if let imageData = data {
            print("image data == \(imageData)")
            Imgdetai.image = UIImage(data: imageData)
            Imgdetai.isUserInteractionEnabled = true
            let pinchMethod = UIPinchGestureRecognizer(target: self, action: #selector(pinchImage(sender:)))
            Imgdetai.addGestureRecognizer(pinchMethod)
            //if(captionTxt.count > 0){
                
            caption.text = captionTxt
            print("Caption Text : \(captionTxt)")
                
            //}
        }
        
        
        navigationItem.title = ""
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtn(_ sender: Any) {
        if (self.albumname == "fromchat"){
//            self.dismiss(animated: true, completion: nil)
            self.gobacktoChat()
            
        }
        else if self.albumname == "ursa1998"{
            self.dismiss(animated: true, completion: nil)
        }else{
        self.gobacktoAlbum()
        }
    }
    
    @objc func pinchImage(sender: UIPinchGestureRecognizer) {

    if let scale = (sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale)) {
      guard scale.a > 1.0 else { return }
      guard scale.d > 1.0 else { return }
      sender.view?.transform = scale
      sender.scale = 1.0
     }
    }
    
    func gobacktoChat(){
        
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "Chatstoryboardid") as? ChatViewController else {
              return
            }
        menuViewController.receiver = receiver
        menuViewController.membername = self.membername
        menuViewController.memberimage = self.memberimage
        menuViewController.modalPresentationStyle = .fullScreen
        self.present(menuViewController, animated: true, completion: nil)
    }
    
    func gobacktoAlbum(){
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "AlbumImagesStoryBoardID") as? VCgallery else {
              return
            }
        menuViewController.albumID = albumId
        menuViewController.albumType = albumtype
        menuViewController.albumname = albumname
        menuViewController.captionString = captionTxt
        menuViewController.modalPresentationStyle = .fullScreen
        if(self.member != nil)
        {
            menuViewController.member=self.member
        }
         
         self.present(menuViewController, animated: true, completion: nil)
    }
}
