//
//  UserMediaViewController.swift
//  Family Pic Will
//
//  Created by mac on 8/17/21.
//

import UIKit
import SwiftyJSON
import Alamofire
import AVKit
import AVFoundation
class UserMediaViewController: UIViewController {
    @IBOutlet weak var PhotoView: UIView!
    @IBOutlet weak var VideoView: UIView!
    let transition = SlideInTransition()
    var user : User?
    var member: User?
    var images : [Image] = []
    var videos : [Image] = []
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var image5: UIImageView!
    @IBOutlet weak var image4: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image0: UIImageView!
    @IBOutlet weak var photoIcon: UIImageView!
    
    
    
    @IBOutlet weak var video5: UIImageView!
    @IBOutlet weak var video4: UIImageView!
    @IBOutlet weak var video3: UIImageView!
    @IBOutlet weak var video2: UIImageView!
    @IBOutlet weak var video1: UIImageView!
    @IBOutlet weak var video0: UIImageView!
    @IBOutlet weak var videoIcon: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        shadow(vw: PhotoView)
        shadow(vw: VideoView)
        transition.delegate = self
        self.loading.isHidden = false
        self.loading.startAnimating()
        getMedia()
        // Do any additional setup after loading the view.
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.openScreenAlbumPhoto))
        tapGesture2.numberOfTapsRequired = 1
        PhotoView.addGestureRecognizer(tapGesture2)
        
        let tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(self.openScreenAlbumVideo))
        tapGesture3.numberOfTapsRequired = 1
        VideoView.addGestureRecognizer(tapGesture3)
    }
    
    func checkimage(`var` ImgCount: Int){
        if ImgCount == 6{
            photoIcon.isHidden = true
            image0.isHidden = false
            if let imageData = showimage(var: images[0].image ?? "") {
                image0.image = UIImage(data: imageData as! Data)
            }
            image1.isHidden = false
            if let imageData = showimage(var: images[1].image ?? "") {
                image1.image = UIImage(data: imageData as! Data)
            }
            image2.isHidden = false
            if let imageData = showimage(var: images[2].image ?? "") {
                image2.image = UIImage(data: imageData as! Data)
            }
            image3.isHidden = false
            if let imageData = showimage(var: images[3].image ?? "") {
                image3.image = UIImage(data: imageData as! Data)
            }
            image4.isHidden = false
            if let imageData = showimage(var: images[4].image ?? "") {
                image4.image = UIImage(data: imageData as! Data)
            }
            image5.isHidden = false
            if let imageData = showimage(var: images[5].image ?? "") {
                image5.image = UIImage(data: imageData as! Data)
            }
            
        }else if ImgCount > 0 && ImgCount < 6{
            photoIcon.isHidden = false
            if let imageData = showimage(var: images[ImgCount-1].image ?? "") {
                photoIcon.image = UIImage(data: imageData as! Data)
            }
        }
        self.loading.isHidden = true
    }
    func showimage(`var` imageStr: String) -> Any?{
        
        let url = URL(string: imageStr)
        let data = try? Data(contentsOf: url!)

        return data
    }
    
    func checkvideo(`var` VideoCount: Int){
        if VideoCount == 6{
            videoIcon.isHidden = true
            video0.isHidden = false
            vidInIt(videoUrl : videos[0].video!, PostImage: self.video0)
            video1.isHidden = false
            vidInIt(videoUrl : videos[1].video!, PostImage: self.video1)
            video2.isHidden = false
            vidInIt(videoUrl : videos[2].video!, PostImage: self.video2)
            video3.isHidden = false
            vidInIt(videoUrl : videos[3].video!, PostImage: self.video3)
            video4.isHidden = false
            vidInIt(videoUrl : videos[4].video!, PostImage: self.video4)
            video5.isHidden = false
            vidInIt(videoUrl : videos[5].video!, PostImage: self.video5)
            
        }else if VideoCount > 0 && VideoCount < 6{
            videoIcon.isHidden = false
            vidInIt(videoUrl : videos[VideoCount-1].video!, PostImage: self.videoIcon)
        }
        self.loading.isHidden = true
    }
    func vidInIt(videoUrl : String,PostImage: UIImageView ) {
          let url = URL(string: videoUrl)
        self.getThumbnailImageFromVideoUrl(url: url!){ (thumbnailImage) in
          PostImage.image = thumbnailImage
        }
      }
      //MARK -thumbnail
      func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
          let asset = AVAsset(url: url) //2
          let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
          avAssetImageGenerator.appliesPreferredTrackTransform = true //4
          let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
          do {
            let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
            let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
            DispatchQueue.main.async { //8
              completion(thumbNailImage) //9
            }
          } catch {
            print(error.localizedDescription) //10
            DispatchQueue.main.async {
              completion(nil) //11
            }
          }
        }
      }
    
        func getMedia(){
            //let like = Apicallgetmediamembers.Request(email: self.member!.email, relation: (user)!)
            //guard var id = user.id
            print(self.member?.id)
            //print(self.user?.city)
            //let myString = ApiCallGetMedia.Request(uid:self.user!.id)
        
            //var param=["uid":member?.id]
            let uid = ApiCallGetMedia.Request(uid: self.member!.id)
            
            let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
                    print(header)
                    let requestService = SCService<ApiCallGetMedia.Request,ApiCallGetMedia.Response>()
            requestService.request("http://3.143.193.45:1337/api/getmediaofuser", method: .post, parameters: uid, encoder: JSONParameterEncoder(), header: header){
                        (result) in
            
                        switch result {
                        case .success(let response):
                            debugPrint(response)
                            print(response.status)
                            print(response.message)
                            if response.status == true{
                            print(response.data)
                                self.images = response.data!.image
                                self.videos = response.data!.video
                            self.checkimage(var: self.images.count)
                            self.checkvideo(var: self.videos.count)
                            print(self.images.count)
                            }else{
                                self.showPromptAlert("Sorry!", message: response.message);
                            }
                            
                            //self.performSegue(withIdentifier: "MainNav", sender: self)
                        case .failure(let error):
                            debugPrint(error)
                           
                            print(error)
                        
                    }
            }
    }
    
    
    
    
    
    
    
    @IBAction func openSideMenuClicked(_ sender: Any) {
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "ProfileStoryboardId") else {
              return
            }
        menuViewController.modalPresentationStyle = .fullScreen
        present(menuViewController, animated: true)
            
            
    }
    @objc func openScreenAlbumPhoto(){
        print("CLicked")
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "AlbumStoryboardId") as? AlbumsViewController else {
              return
            }
        menuViewController.member = self.member
        menuViewController.type = "1"
            menuViewController.modalPresentationStyle = .overFullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
            
    }
    @objc func openScreenAlbumVideo(){
        print("CLicked")
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "AlbumStoryboardId") as? AlbumsViewController else {
              return
            }
        menuViewController.member = self.member
        menuViewController.type = "2"
        menuViewController.modalPresentationStyle = .overFullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
            
    }
    
    
    func shadow(vw: UIView) {
        vw.layer.shadowPath = UIBezierPath(rect: vw.bounds).cgPath
        vw.layer.shadowRadius = 2
        
        vw.layer.shadowOffset = .zero
        vw.layer.shadowOpacity = 0.5
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
}
extension UserMediaViewController : UIViewControllerTransitioningDelegate{
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = true
    return transition
  }
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = false
    return transition
  }
}
extension UserMediaViewController : CloseSideMenu{
    func closeSideMenu(){
        dismiss(animated: true, completion: nil)
    }
}

