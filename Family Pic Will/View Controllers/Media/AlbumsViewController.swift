//
//  AlbumsViewController.swift
//  familywithwall
//
//  Created by Apple on 18/05/2021.
//

import UIKit
import SwiftyJSON
import Alamofire

class AlbumsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    var albuuumID : String = ""
    var x = 1
    var member : User?
    @IBOutlet weak var addMediaBtn: UIButton!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBAction func backClicked(_ sender: Any) {
    

    self.goback()
    }
    func goback(){
        
        if(member != nil){
            guard let menuViewController = storyboard?.instantiateViewController(identifier: "UserMediaStoryboardId") as? UserMediaViewController else {
                  return
                }
            
            
            menuViewController.modalPresentationStyle = .fullScreen
            menuViewController.member = member
             
             self.present(menuViewController, animated: true, completion: nil)
        }
        else{
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "MediaStoryboardId") as? MediaViewController else {
              return
            }
        
        
            menuViewController.modalPresentationStyle = .fullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
        }
    }
    var type : String = ""
    var albums: [Album] = []

    @IBOutlet weak var albumTabelView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if member != nil {
            addMediaBtn.isHidden = true
        }
        albumTabelView.delegate = self
        albumTabelView.dataSource = self
        getalbum()
        self.loading.isHidden = false
        self.loading.startAnimating()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func AddImage_AlbumBtn(_ sender: Any) {
       //AddNewAlbum_ImageStoryBoardID
        print("working")
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "AddNewAlbum") as? Form2VC else {
              return
            }
        menuViewController.x = self.x
        menuViewController.type = type
        menuViewController.modalPresentationStyle = .fullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
        
    }
    func getalbum(){
        //let par
        //var parameters = ApiCallGetAlbums.Request(type: type)
        print(self.member?.id)
        if(self.member != nil)
        {
            
          let  parameters = ApiCallGetAlbums.Request(type: type,uid:self.member?.id)
            let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
                print(header)
              //let parameters = ApiCallGetAlbums.Request(type: type)
                let requestService = SCService<ApiCallGetAlbums.Request,ApiCallGetAlbums.Response>()
                requestService.request("http://3.143.193.45:1337/api/getalbumsofuser", method: .post, parameters: parameters, encoder: JSONParameterEncoder(), header: header){
                 (result) in
                 switch result {
                 case .success(let response):
                  debugPrint(response)
                  print(response.status)
                  print(response.message)
                  if response.status == true{
                      self.albums = response.data!
                  print(self.albums)
                  self.albumTabelView.reloadData()
                    
                    //self.remove()
                  }else{
                      self.showPromptAlert("Sorry!", message: response.message);
                  }
                  self.loading.isHidden = true
                  //self.performSegue(withIdentifier: "MainNav", sender: self)
                 case .failure(let error):
                  debugPrint(error)
                 
                  self.loading.isHidden = true
                  print(error)
                 }
                }
        }
        
        else{
      let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
          print(header)
        let parameters = ApiCallGetAlbums.Request(type: type)
          let requestService = SCService<ApiCallGetAlbums.Request,ApiCallGetAlbums.Response>()
          requestService.request("http://3.143.193.45:1337/api/getalbums", method: .post, parameters: parameters, encoder: JSONParameterEncoder(), header: header){
           (result) in
            print(result)
           switch result {
           case .success(let response):
            debugPrint(response)
            print(response.status)
            print(response.message)
            if response.status == true{
                self.albums = response.data!
            print(self.albums)
            self.albumTabelView.reloadData()
            //self.remove()
            }else{
                self.showPromptAlert("Sorry!", message: response.message);
            }
            self.loading.isHidden = true
            //self.performSegue(withIdentifier: "MainNav", sender: self)
           case .failure(let error):
            debugPrint(error)
           
            self.loading.isHidden = true
            print(error)
           }
          }
        }
    }
    func remove(){
        var coun = 0
        for count in 0...albums.count-1{
            
            if albums[coun].image == ""{
                albums.remove(at: coun)
                coun-=1
            }
            coun+=1
        }
        self.albumTabelView.reloadData()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "albumCell", for: indexPath) as! AlbumTableViewCell
        if (member != nil){
            cell.albumCell.isHidden = true
            
        }
        
        if albums[indexPath.row].type == 1 {
        cell.checkImage(imageStr: albums[indexPath.row].image)
        
    }
        if albums[indexPath.row].type == 2 {
            
            print("===================== \(String(describing: albums[indexPath.row].thumb))")
            cell.checkImage(imageStr: albums[indexPath.row].thumb!)
    }
        cell.albumn = albums[indexPath.row].albumname
        cell.delegate = self
        cell.albuumID = albums[indexPath.row].id
        cell.type = "\(albums[indexPath.row].type)"
        cell.albumName.text = albums[indexPath.row].albumname
            
        
        //cell.ImagesNumber.text = "\(albums[indexPath.row].count)"
        return cell
    }
    

    

}
extension AlbumsViewController : AlbumDataCell{
    
    
    func buttonTap(AlbumId: String, type: String, Albumaname: String) {
        print("checked")
        //AlbumImagesStoryBoardID
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "AlbumImagesStoryBoardID") as? VCgallery else {
              return
            }
        menuViewController.typee = self.type
        menuViewController.albumID = AlbumId
        menuViewController.albumType = type
        menuViewController.albumname = Albumaname
        if(member != nil)
        {
            menuViewController.member = member
        }
        menuViewController.modalPresentationStyle = .overFullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
    }
    func deleteBtn(AlbumId:String, Xtype: String){
        self.loading.isHidden = false
        self.delete(id: AlbumId, type: Xtype)
        
        //AlbumImagesStoryBoardID
      
    }
    func delete(id: String, type: String){
        print(id)
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCallDeleteAlbum.Request(id: id, type: type)
                let requestService = SCService<ApiCallDeleteAlbum.Request,ApiCallDeleteAlbum.Response>()
                requestService.request("http://3.143.193.45:1337/api/deletealbum", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in

                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == true{

                            self.getalbum()
                            
                        }else{
                            self.showPromptAlert("Sorry!", message: response.message);
                        }
                        
                        //self.performSegue(withIdentifier: "MainNav", sender: self)
                    case .failure(let error):
                        debugPrint(error)

                        print(error)

                    }


                }
    }

}
