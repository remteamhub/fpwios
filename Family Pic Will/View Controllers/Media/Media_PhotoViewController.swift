//
//  Media_PhotoViewController.swift
//  familywithwall
//
//  Created by Apple on 05/05/2021.
//

import UIKit

class Media_PhotoViewController: UIViewController {

    @IBOutlet weak var AlbumView: UIView!
    @IBOutlet weak var MyphotoView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        shadow(vw: AlbumView)
        shadow(vw: MyphotoView)
        // Do any additional setup after loading the view.
    }
    
    func shadow(vw: UIView) {
        vw.layer.shadowPath = UIBezierPath(rect: vw.bounds).cgPath
        vw.layer.shadowRadius = 2
        
        vw.layer.shadowOffset = .zero
        vw.layer.shadowOpacity = 0.5
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
