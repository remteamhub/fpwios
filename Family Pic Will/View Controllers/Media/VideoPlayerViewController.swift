//
//  VideoPlayerViewController.swift
//  Family Pic Will
//
//  Created by Apple on 29/06/2021.
//

import UIKit
import AVFoundation
import AVKit

class VideoPlayerViewController: UIViewController {

    var memberimage = ""
    var membername = ""
    var url : String = ""
    var urlString : String = ""
    var captionTxtVideo : String = ""
    var albumId: String  = ""
    var albumtype: String = ""
    var albumname: String = ""
    var playercontroller = AVPlayerViewController()
    var member : User?
    var receiver: String = ""


    @IBOutlet weak var captionLabel: UILabel!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()

        print("urlString ..... \(urlString)")
        playingVideo()
//
//        print("url ..... \(url)")
//        print("vied did load")
//        if urlString == ""{
//            playingVideo()
//
//        }
//        else{
//            print("vied did load url === ")
//            playingURLVideo()
//
//        }
//
//        playVideo()
        print("cccaaaaaaaaapppppppttttiiiiooonnnnn  ====  \(captionTxtVideo)")
       captionLabel.text = captionTxtVideo
    }
//
//    func playVideo(){
//
//        let videoURL = URL(string: urlString)
//        let player = AVPlayer(url: videoURL!)
//        let playerLayer = AVPlayerLayer(player: player)
//
//        playerLayer.frame = self.view.bounds
//
//        playerLayer.videoGravity = .resizeAspect
//
//        self.view.layer.addSublayer(playerLayer)
//        player.play()
//    }
    
    
    func playingVideo(){
        let videoURL = URL(string: urlString)
//        playercontroller.pl
        let player = AVPlayer(url: videoURL!)
        player.rate = 1 //auto play
//
//        CGPoint superCenter = CGPointMake(CGRectGetMidX([superview bounds]), CGRectGetMidY([superview bounds]))
//
        let playerFrame = CGRect(x: 0, y: view.frame.height/9, width: view.frame.width, height: view.frame.height/1.6)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        playerViewController.view.frame = playerFrame

        addChild(playerViewController)
        view.addSubview(playerViewController.view)
        playerViewController.didMove(toParent: self)
        
        playercontroller.showsPlaybackControls = true
    }
    func killVideoPlayer(){
//            self.isAlreadyPlaying          = NO
            
            self.playercontroller.player?.pause()
            
            self.playercontroller.player  = nil
            
            let audioSession = AVAudioSession.sharedInstance()

            do
            {
                try audioSession.setActive(false, options: .notifyOthersOnDeactivation)
                try audioSession.setCategory(.soloAmbient)
            }
            catch
            {
                print("Audio session failed")
            }
            
            self.playercontroller.dismiss(animated: true, completion: nil)
        }
    
    
    func playingURLVideo(){
        
        ("playing video called")
        let videoURL = URL(string: url)
        let player = AVPlayer(url: videoURL!)
        player.rate = 1 //auto play
//
//        CGPoint superCenter = CGPointMake(CGRectGetMidX([superview bounds]), CGRectGetMidY([superview bounds]))
//
        let playerFrame = CGRect(x: 0, y: view.frame.height/9, width: view.frame.width, height: view.frame.height/1.6)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        playerViewController.view.frame = playerFrame

        addChild(playerViewController)
        view.addSubview(playerViewController.view)
        playerViewController.didMove(toParent: self)
        
        playercontroller.showsPlaybackControls = true
    }

    @IBAction func goBackButton(_ sender: Any) {
        if albumname == "fromchat"{
            gobacktochat()
        }else{
        self.dismiss(animated: true, completion: nil)
        }
//        if self.albumname == "ursa1998"{
//            self.dismiss(animated: true, completion: nil)
//        }else{
//        self.gobacktoAlbum()
//        }
    }
    
    func gobacktoAlbum(){
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "AlbumImagesStoryBoardID") as? VCgallery else {
              return
            }
        menuViewController.albumID = albumId
        menuViewController.albumType = albumtype
        menuViewController.albumname = albumname
        menuViewController.captionString = captionTxtVideo
        menuViewController.modalPresentationStyle = .overFullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
    }
    func gobacktochat(){
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "Chatstoryboardid") as? ChatViewController else {
              return
            }
        let presentingVC = self.presentingViewController
        menuViewController.receiver = receiver
        menuViewController.membername = self.membername
        menuViewController.memberimage = self.memberimage
        menuViewController.videoUrl = ""
        menuViewController.modalPresentationStyle = .fullScreen
        dismiss(animated: true) {
            presentingVC!.present(menuViewController, animated: true,completion: nil)
        }
        
        }

}

