//
//  VCgallery.swift
//  Bdaynotif
//
//  Created by Coder Crew on 5/20/21.
//

import UIKit
import SwiftyJSON
import Alamofire
import AVKit
import AVFoundation
class VCgallery: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, CollectionDataCell {
    @IBOutlet weak var addMediaBtn: UIButton!
    var albumID : String = ""
    var typee = ""
    var member : User?
    var albumType : String = ""
    var countt : Int = 0
    var albumname: String = ""
    var captionString : String = ""
    var playerViewController = AVPlayerViewController()
    var playerView = AVPlayer()
    var type : String = ""
    var x = 2
    var VGC = VideoGalleryVC()
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var AlbumName: UILabel!
    
    
    var array:[AlbumData] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        if(member != nil){
            
            addMediaBtn.isHidden = true
            
        }
        myCollectionView.dataSource = self
        myCollectionView.delegate = self
        self.loading.isHidden = false
        self.loading.startAnimating()
        print(albumID)
        getAlbumImagesorVideos()
        self.AlbumName.text = albumname
        let itemSize = UIScreen.main.bounds.width/3 - 1
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: itemSize, height: itemSize)
        
        layout.minimumInteritemSpacing = 1
        layout.minimumLineSpacing = 1
        
        myCollectionView.collectionViewLayout = layout
    }
    @IBAction func AddImage_AlbumBtn(_ sender: Any) {
       //AddNewAlbum_ImageStoryBoardID
        print("working")
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "AddNewAlbum") as? Form2VC else {
              return
            }
        menuViewController.x = self.x
        menuViewController.type = typee
        menuViewController.modalPresentationStyle = .fullScreen
        
         self.present(menuViewController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func backBtn(_ sender: Any) {
        
        self.gobacktoAlbum()
    }
    
    func gobacktoAlbum(){
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "AlbumStoryboardId") as? AlbumsViewController else {
              return
            }
        
        menuViewController.type = albumType
            menuViewController.modalPresentationStyle = .overFullScreen
        if(self.member != nil)
        {
            menuViewController.member = self.member
        }
         
         self.present(menuViewController, animated: true, completion: nil)
    }
    
    func getAlbumImagesorVideos(){
        print(albumID)
       let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
               print(header)
       let parameters = ApiCallGetAlbumsImagesorVideos.Request(id: albumID,type: albumType)
               let requestService = SCService<ApiCallGetAlbumsImagesorVideos.Request,ApiCallGetAlbumsImagesorVideos.Response>()
               requestService.request("http://3.143.193.45:1337/api/getalbum", method: .post, parameters: parameters, encoder: JSONParameterEncoder(), header: header){
                (result) in
                switch result {
                case .success(let response):
                 debugPrint(response)
                 print(response.status)
                 print(response.message)
                
                    
                    if response.status == true{
                        
                        
                        print("checking in data k response aa raha hai k nahi ===========  \(String(describing: response.data))")
                        self.array = response.data!
                    self.myCollectionView.reloadData()
                    }else{
                        self.showPromptAlert("Sorry!", message: response.message);
                    }
                    self.loading.isHidden = true
                        
                case .failure(let error):
                 debugPrint(error)
                 print(error)
                }
               }
   
     }
    //Number of views
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count

    }
    
  //Populate view
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionCell
        cell.delegate = self
    
        print("==+++++++++++++ ==== == == \(String(describing: array[indexPath.row].thumb))")
        cell.image_Video = array[indexPath.row].thumb!
        cell.type = array[indexPath.row].albumid
        cell.caption = array[indexPath.row].caption!
        cell.id = array[indexPath.row].id
        if member != nil {
            cell.removeMediaBtn.isHidden = true
        }
        if albumType == "1"{
            let url = URL(string: array[indexPath.row].image!)
            let data = try? Data(contentsOf: url!)

            if let imageData = data {
                cell.CollectionImg.image = UIImage(data: imageData)
            }
        }
        if albumType == "2"{
            cell.CollectionImg.isHidden = false
            let url = URL(string: array[indexPath.row].thumb!)
            let data = try? Data(contentsOf: url!)

            if let imageData = data {
                cell.CollectionImg.image = UIImage(data: imageData)
            }
           //             if let thumbnailImage = getThumbnailImage(forUrl: url!) {
             //               cell.CollectionImg.image = thumbnailImage
               //             }
        }
        
        
        return cell
        
    }
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)

        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }

        return nil
    }
//    func checkVideo(imageStr: String){
//
//        if imageStr == ""{
//            albumImage.image = UIImage(named: "_add_image-512")
//        }else{
//            albumImage.isHidden = false
//            let url = URL(string: imageStr)
//            if let thumbnailImage = getThumbnailImage(forUrl: url!) {
//                albumImage.image = thumbnailImage
//                }
//
//        }
//    }
    func playVideo(videoUrl: String) {
        
      //  VGC.playVideo(videoUrl: videoUrl)
        playvideo(videoUrl : videoUrl)
        
    }
    
    func playvideo(videoUrl : String) {
        let url: URL = URL(string: videoUrl)!
        playerView = AVPlayer(url: url)
        playerViewController.player = playerView
        self.present(playerViewController, animated: true)
        self.playerViewController.player?.play()
        //[self.playerViewController.view addSubview:self.commentLabel];
        
        
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if albumType == "1"{
            guard let menuViewController = storyboard?.instantiateViewController(identifier: "photoShow") as? detailedImg else {
                  return
                }
            menuViewController.name = array[indexPath.row].image!
            menuViewController.albumId = albumID
            menuViewController.albumtype = albumType
            menuViewController.albumname = albumname
            menuViewController.captionTxt = array[indexPath.row].caption!
            menuViewController.modalPresentationStyle = .overFullScreen
            if(self.member != nil)
            {
                menuViewController.member = self.member
            }
             
             self.present(menuViewController, animated: true, completion: nil)
        }
        if albumType == "2"{
            
            
            guard let menuViewController = storyboard?.instantiateViewController(identifier: "VideoPlayerViewController") as? VideoPlayerViewController else {return}
            menuViewController.urlString = array[indexPath.row].video!
            menuViewController.albumId = albumID
            menuViewController.albumtype = albumType
            menuViewController.albumname = albumname
            menuViewController.captionTxtVideo = array[indexPath.row].caption!
            menuViewController.modalPresentationStyle = .fullScreen
            if(self.member != nil)
            {
                menuViewController.member = self.member
            }


             self.present(menuViewController, animated: true, completion: nil)

            
            
        }
       
        
        
    }
    

    func buttonTap(Id: String, Xtype: String) {
        
        self.delete(id: Id, type: Xtype)
    }

   
    func delete(id: String, type: String){
        print("salman")
        print(id)
        print(type)
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCallDeleteImage_Video.Request(vpid: id, type:self.albumType)
                let requestService = SCService<ApiCallDeleteImage_Video.Request,ApiCallDeleteImage_Video.Response>()
                requestService.request("http://3.143.193.45:1337/api/deletepv", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
        
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == true {
                        
                        self.getAlbumImagesorVideos()
                        }else{
                            self.showPromptAlert("Sorry!", message: response.message);
                        }
                        self.loading.isHidden = true
                        //self.performSegue(withIdentifier: "MainNav", sender: self)
                    case .failure(let error):
                        debugPrint(error)
                        
                        print(error)
        
                    }
        
        
            }
        
    }
    
}

//extension VCgallery : CollectionDataCell{
    
    
    
//}




