//
//  FormVC.swift
//  Bdaynotif
//
//  Created by Coder Crew on 5/21/21.
//

import UIKit
import DropDown

import AVKit
import MobileCoreServices
import AVFoundation
import SwiftyJSON
import Alamofire
class Form2VC: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var vwDrop: UIView!
    @IBOutlet weak var vwDropDown: UIView!
    @IBOutlet weak var LblTitle: UILabel!
    @IBOutlet weak var AlbumName: UITextField!
    
    @IBOutlet weak var caption: UITextField!
    @IBOutlet weak var FormLabel: UILabel!
    @IBOutlet weak var AddBtnOutlet: UIButton!
    @IBOutlet weak var AddImage_VideoBtn: UIButton!
    @IBOutlet weak var postimage: UIImageView!
    @IBOutlet weak var playBtnOutlet: UIButton!
    @IBOutlet weak var DropdownButton: UIButton!
    var x = 0
    var matchesarray : [Any] = []
    var captionString : String = "default String to check"
    var type: String = ""
    var imagePickerController = UIImagePickerController()
    var videoURL : URL?
    var playerViewController = AVPlayerViewController()
      var playerView = AVPlayer()
    let dropDown = DropDown()
    var albums: [Album] = []
    var imagePicker = UIImagePickerController()
    var videoAndImageReview = UIImagePickerController()
    var selectedIndex: Int = 0
    var dropDownValues : [String] = ["Create New Album"]
    override func viewDidLoad() {
        super.viewDidLoad()
        print(type)
        
        
        if type == "1"{
            self.FormLabel.text = "Add Image"
        }
        if type == "2"{
            self.AddImage_VideoBtn.setTitle("Add Video", for: .normal)
            self.FormLabel.text = "Add Video"
        }
        getalbum()
        imagePicker.delegate = self
        imagePickerController.delegate = self
        dropDown.anchorView = vwDrop
        self.AddBtnOutlet.isHidden = true
        self.loading.startAnimating()
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.direction = .bottom
        dropDown.cellHeight = 40
        dropDown.direction = .any
        DropDown.appearance().textColor = UIColor.black
        DropDown.appearance().selectedTextColor = UIColor.systemBlue
        DropDown.appearance().textFont = UIFont.systemFont(ofSize: 14)
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.lightGray
        DropDown.appearance().cellHeight = 40
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            if index != 0{
                AlbumName.isUserInteractionEnabled = false
            }else{
                AlbumName.isUserInteractionEnabled = true
            }
            if index == 0{
                AlbumName.text = ""
                AlbumName.placeholder = "Create New Album"
            }else{
                AlbumName.text = albums[index-1].albumname
            }
            
            
            self.selectedIndex = index
            self.LblTitle.text = dropDownValues[index]
        }
        

//         Do any additional setup after loading the view.
    }
   
    @IBAction func AddORcreate(_ sender: Any) {
        if selectedIndex == 0{
            self.loading.isHidden = false
            if  (   type == "1"   )
            {
                checkimage()
            }
            else if ( type == "2")
            {
                checkvideo()
            }
            else
            {
                
            }
            self.AddBtnOutlet.isHidden = true
            
        }else{
            self.loading.isHidden = false
            //checkvideo()
            if  (   type == "1"   )
            {
                checkimage()
            }
            else if ( type == "2")
            {
                checkvideo()
            }
            else
            {
                
            }
            self.AddBtnOutlet.isHidden = true
            
        }
        
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.gobacktoAlbum()
        
    }
    
    func gobacktoAlbum(){
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "AlbumStoryboardId") as? AlbumsViewController else {
           return
          }
        menuViewController.type = type
          menuViewController.modalPresentationStyle = .overFullScreen
         self.present(menuViewController, animated: true, completion: nil)
      }
    
    func checkimage(){
        
        
        let imageData = (postimage.image?.jpegData(compressionQuality: 0.8))!

    let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
    let parameter = ["models": "nudity,wad","api_user": "406594658", "api_secret": "7x3uRdHtFyaNg4HHd5C7"] as [String : Any]
    
    AF.upload(multipartFormData: { multipartFormData in
        for (key, value) in parameter {
            if let temp = value as? String {
                           multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                       }
           
            print("parameter")
           
        }
        
            print("checked")
        if self.type == "1"{
            multipartFormData.append(imageData, withName: "media" , fileName: "file.png", mimeType: "image/png")
        }
                
    }, to: "https://api.sightengine.com/1.0/check.json",method: .post , headers: header)
        .responseJSON(completionHandler: { (response) in
                    print(parameter)
                    print(response)
                    
                    if let err = response.error{
                        print(err)
                        //self.postBtnOutlet.isHidden = false
                        return
                    }
                    print("Succesfully uploaded")
                    
            let json = response.data
                    print("1")
            print(json)
            response
            
                    if (json != nil)
                    {
                        let jsonObject = JSON(json!)
                        print(jsonObject)
                        let jsonObjectNudity = jsonObject["nudity"]
                        print(jsonObjectNudity)
                        let raw = jsonObjectNudity["raw"]
                        print(raw)
                        let partial = jsonObjectNudity["partial"]
                        print(partial)
                        let safe = jsonObjectNudity["safe"]
                        print(safe)
                        
                        if raw > safe {
                            self.showPromptAlert("Image Alert", message: "Selected images is against our privacy policies", okTitle: "ok")
                            self.loading.isHidden = true
                            
                        } else if self.type == "1" {
                            self.checkcaption()
                        }
                        
                        
                        
                        self.loading.isHidden = true
                       // self.goback()
                    }
                })
    
    }
    
    func checkvideo(){
        
        
      //  let imageData = (postimage.image?.jpegData(compressionQuality: 0.8))!

    let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
    let parameter = ["models": "nudity,wad","api_user": "406594658", "api_secret": "7x3uRdHtFyaNg4HHd5C7"] as [String : Any]
    
    AF.upload(multipartFormData: { multipartFormData in
        for (key, value) in parameter {
            if let temp = value as? String {
                           multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                       }
           
            print("parameter")
           
        }
        
            print("checked")
        if self.type == "2"{
            multipartFormData.append(self.videoURL! as URL, withName: "media", fileName: "video.mp4", mimeType: "video/mp4")
        }
                
    }, to: "https://api.sightengine.com/1.0/video/check-sync.json",method: .post , headers: header)
        .responseJSON(completionHandler: { (response) in
                    print(parameter)
                    print(response)
                    
                               if let err = response.error{
                                print(err)
                               // self.postBtnOutlet.isHidden = false
                                 return
                    }
                                   print("Succesfully uploaded")
                    
                                     let json = response.data
                                     print("1")
                                    print(json)
                                     response
            
                    if (json != nil)
                    {
                        let jsonObject = JSON(json!)
                        print("jsonObject")
                        print(jsonObject)
                        let jsonObjectData = jsonObject["data"]
                        print("jsonObjectData")
                        print(jsonObjectData)
                        let jsonObjectframes = jsonObjectData["frames"]
                        print(jsonObjectframes)
                        let jsonObjectNudity = jsonObjectframes[0]["nudity"]
                        
                        
                        var x:Bool = true
                        
                        for var i in (0..<jsonObjectframes.count)
                        {
                            if( jsonObjectframes[i]["nudity"]["raw"] > jsonObjectframes[i]["nudity"]["safe"] )
                            {
                                x = false
                                
                                self.showPromptAlert("Video Alert", message: "Selected Video is against our privacy policies", okTitle: "ok")
                                self.loading.isHidden = true
                                break
                            }
                            
                        }
                        if ( x == true ){
                            
                                self.checkcaption()
                        }
                        
                    }
                })
    
    }
    
    
    func checkcaption() {
        if(caption.text != "")
        {
            let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
            let parameter = ["models": "text","api_user": "406594658", "api_secret": "7x3uRdHtFyaNg4HHd5C7", "lang": "en", "mode": "standard", "text": caption.text!] as [String : Any]
            
            AF.upload(multipartFormData: { multipartFormData in
                            for (key, value) in parameter {
                                if let temp = value as? String {
                                    multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                                }
                            }
                           
                        }, to: "https://api.sightengine.com/1.0/text/check.json",method: .post , headers: header)
                            .responseJSON(completionHandler: { (response) in
                                
                                        if let err = response.error{
                                            
                                            return
                                        }
                                print(response)
                                
                                        let json = response.data
                                
                                        if (json != nil)
                                        {
                                            
                                            let jsonObject = JSON(json!)
                                            print(jsonObject)
                                            let jsonObjectprofanity = jsonObject["profanity"]
                                            print(jsonObjectprofanity)
                                            let jsonObjectmatches = jsonObjectprofanity["matches"]
                                            print(jsonObjectmatches)
                                            
                                        
                                            self.matchesarray = jsonObjectmatches.arrayObject! as [Any]
                                            print(self.matchesarray)
                                            if self.matchesarray.count>0 {
                                                self.showPromptAlert("Text Alert", message: "Selected Text is against our privacy policies", okTitle: "ok")
                                                self.loading.isHidden = true
                                               // self.goback()
                                                
                                            } else {
                                                self.createNewAlbum()
                                            }

                                            
                                          
                                        }
                                    })
        }
        else
        {
            self.createNewAlbum()
        }
}
    
    
    
    func createNewAlbum(){
        
        captionString = caption.text!
        
        print("%^%^%^^&*&*&^%^&$^^*&(*^&*%$*$^^*&* ====== \(captionString)")
            let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let parameter = ["type": type, "albumname": AlbumName.text,"caption": captionString] as [String : Any]
              let imageData = (postimage.image?.jpegData(compressionQuality: 0.8))!
              print("q")
              print(imageData as Any)
                 AF.upload(multipartFormData: { multipartFormData in
                  for (key, value) in parameter {
                   if let temp = value as? String {
                       multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                      }
                   print("parameter")
                  }
                   print("checked")
                  if self.type == "1"{
                   multipartFormData.append(imageData, withName: "image" , fileName: "file.png", mimeType: "image/png")
                  }
                  if self.type == "2"{
                    multipartFormData.append(self.videoURL! as URL, withName: "video", fileName: "video.mp4", mimeType: "video/mp4")
                  }
                 }, to: "http://3.143.193.45:1337/api/addalbum",method: .post , headers: header)
                  .responseJSON(completionHandler: { (response) in
                     print(parameter)
                     print(response)
                     if let err = response.error{
                      print(err)
                      return
                     }
                     print("Succesfully uploaded")
                     let json = response.data
                     print("1")
                   print(json)
                     if (json != nil)
                     {
                      let jsonObject = JSON(json!)
                      print(jsonObject)
                        self.gobacktoAlbum()
                        self.loading.isHidden = true
                     }
                    })
      }
    func addimageorvideoinAlbum(){
        
        captionString = caption.text!
        
        print(captionString)
       let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let parameter = ["type": type, "albumid": albums[selectedIndex-1].id,"caption": captionString ]
         let imageData = (postimage.image?.jpegData(compressionQuality: 0.8))!
         print("q")
         print(imageData as Any)
            AF.upload(multipartFormData: { multipartFormData in
             for (key, value) in parameter {
              if let temp = value as? String {
                  multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                 }
              print("parameter")
             }
              print("checked")
             if self.type == "1"{
              multipartFormData.append(imageData, withName: "image" , fileName: "file.png", mimeType: "image/png")
             }
             if self.type == "2"{
                multipartFormData.append(self.videoURL! as URL, withName: "video", fileName: "video.mp4", mimeType: "video/mp4")
             }
            }, to: "http://3.143.193.45:1337/api/add",method: .post , headers: header)
             .responseJSON(completionHandler: { (response) in
                print(parameter)
                print(response)
                if let err = response.error{
                 print(err)
                    
                 return
                }
                print("Succesfully uploaded")
                let json = response.data
                print("1")
              print(json)
                if (json != nil)
                {
                 let jsonObject = JSON(json!)
                 print(jsonObject)
                    self.gobacktoAlbum()
                    self.loading.isHidden = true
                }
               })
     }
    
    func getalbum(){
      let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
          print(header)
        let parameters = ApiCallGetAlbums.Request(type: type)
          let requestService = SCService<ApiCallGetAlbums.Request,ApiCallGetAlbums.Response>()
          requestService.request("http://3.143.193.45:1337/api/getalbums", method: .post, parameters: parameters, encoder: JSONParameterEncoder(), header: header){
           (result) in
           switch result {
           case .success(let response):
            debugPrint(response)
            print(response.status)
            print(response.message)
            if response.status == true{
                self.albums = response.data!
            print(self.albums)
            self.populateArray()
            print(self.dropDownValues)
            self.dropDown.reloadInputViews()
            }else{
                self.showPromptAlert("Sorry!", message: response.message);
            }
            self.loading.isHidden = true
            //self.performSegue(withIdentifier: "MainNav", sender: self)
           case .failure(let error):
            debugPrint(error)
            print("1")
            print("1")
            print("1")
            print("1")
            print(error)
           }
          }
    }
    @IBAction func dropButtonAction(_ sender: Any) {
       dropDown.show()


    }
    func populateArray(){
        for album in albums {
            dropDownValues.append(album.albumname)
        }
        self.dropDown.dataSource = dropDownValues
    }
    
    
    @IBAction func PlayBtn(_ sender: Any) {
        playvideo(videoUrl : "\(videoURL)")
    }
    

    @IBAction func SelectImageBtn(_ sender: Any) {
        if type == "1"{
            wishBtnAction(var: "Camera", var: "Gallery")
        }
        if type == "2"{
            
            videoBtnAction(var: "Camera", var: "Gallery")
        }
    }
    
    
    func videoBtnAction(`var` str1:String, `var` str2:String) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: str1, style: .default, handler: { _ in
            self.RecordAction()
        }))
        
        alert.addAction(UIAlertAction(title: str2, style: .default, handler: { _ in
            self.openImgVideoPicker()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
    
        
        self.present(alert, animated: true, completion: nil)
        
      }
    
    
    
    
    
    
    
    //video
    func RecordAction() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            print("Camera Available")
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.mediaTypes = [kUTTypeMovie as String]
            imagePicker.allowsEditing = true
             
            self.present(imagePicker, animated: true, completion: nil)
          } else {
            let alert = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
          }
        }
      //capture
      func imagePickerController(_ picker: UIImagePickerController,
                      didFinishPickingMediaWithInfo info: [String : Any]) {
        print("123")
          dismiss(animated: true, completion: nil)
          guard
            let mediaType = info[UIImagePickerController.InfoKey.mediaType.rawValue] as? String,
            mediaType == (kUTTypeMovie as String),
            let url = info[UIImagePickerController.InfoKey.mediaURL.rawValue] as? URL,
            UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path)
            else {
              return
          }

          // Handle a movie capture
          UISaveVideoAtPathToSavedPhotosAlbum(
            url.path,
            self,
            #selector(video(_:didFinishSavingWithError:contextInfo:)),
            nil)
        }
        @objc func video(_ videoPath: String, didFinishSavingWithError error: Error?, contextInfo info: AnyObject) {
          let title = (error == nil) ? "Success" : "Error"
          let message = (error == nil) ? "Video was saved" : "Video failed to save"
          let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
          present(alert, animated: true, completion: nil)
        }
      ///
      //videopick
      func openImgVideoPicker() {
        print("11")
        imagePickerController.sourceType = .savedPhotosAlbum
           imagePickerController.delegate = self
           imagePickerController.mediaTypes = [kUTTypeMovie as String]
           present(imagePickerController, animated: true, completion: nil)
        }

    
    func wishBtnAction(`var` str1:String, `var` str2:String) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: str1, style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: str2, style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
       
        
        self.present(alert, animated: true, completion: nil)
        
      }
    
    

    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
                //If you dont want to edit the photo then you can set allowsEditing to false
                imagePicker.allowsEditing = true
                imagePicker.delegate = self
            self.present(imagePicker, animated: true,completion: nil)
            }
            else{
                let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        //MARK: - Choose image from camera roll
        
        func openGallary(){
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            //If you dont want to edit the photo then you can set allowsEditing to false
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true,completion: nil)

        }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if type == "2"{
            print("22")
            videoURL = info[.mediaURL] as! URL
            
            do {
                print("33")
                let asset = AVURLAsset(url: videoURL! as! URL , options: nil)
                let imgGenerator = AVAssetImageGenerator(asset: asset)
                imgGenerator.appliesPreferredTrackTransform = true
                let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
                let thumbnail = UIImage(cgImage: cgImage)
                self.postimage.image = thumbnail
                self.playBtnOutlet.isHidden = true
                self.AddBtnOutlet.isHidden = false
            } catch let error {
                print("*** Error generating thumbnail: \(error.localizedDescription)")
            }
            self.dismiss(animated: true, completion: nil)
        }
        if type == "1"{
            print("456")
            picker.dismiss(animated: true, completion: nil)

            if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
                postimage.image = editedImage
                self.AddBtnOutlet.isHidden = false
                }
        }
       
    }
    

    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.isNavigationBarHidden = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func playvideo(videoUrl : String) {
        let url: URL = URL(string: videoUrl)!
        playerView = AVPlayer(url: url)
        playerViewController.player = playerView
        self.present(playerViewController, animated: true)
        self.playerViewController.player?.play()
    }
    
    func vidInIt(videoUrl : String) {
          let url = URL(string: videoUrl)
        self.getThumbnailImageFromVideoUrl(url: url!){ (thumbnailImage) in
          self.postimage.image = thumbnailImage
        }
      }
      //MARK -thumbnail
      func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
          let asset = AVAsset(url: url) //2
          let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
          avAssetImageGenerator.appliesPreferredTrackTransform = true //4
          let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
          do {
            let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
            let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
            DispatchQueue.main.async { //8
              completion(thumbNailImage) //9
            }
          } catch {
            print(error.localizedDescription) //10
            DispatchQueue.main.async {
              completion(nil) //11
            }
          }
        }
      }

    
}

