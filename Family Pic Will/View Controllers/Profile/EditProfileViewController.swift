//
//  EditProfileViewController.swift
//  FPW
//
//  Created by Apple on 29/04/2021.
//

import UIKit
import Alamofire
import CountryPickerView
import DropDown
import SwiftyJSON
import SDWebImage
protocol  RefereshProfile : AnyObject{
    func refereshProfile()
}
class EditProfileViewController: UIViewController {
    public var user : User?
    public let country = CountryPickerView()
    
    var delegate : RefereshProfile?
    var selected : Int?
    var selectedCountry:String!
    var flag = 2
    var fromVerificationScreen : Bool = false
    var picker = UIImagePickerController()
    var swiftyJsonVar = JSON()
    var date : String?
    var menuString : [String] = []
    
    let datePicker = UIDatePicker()
    let menu : DropDown = {
        let menu = DropDown()
        menu.dataSource = []
        return menu
      }()
    let menu2 : DropDown = {
        let menu = DropDown()
        menu.dataSource = []
        return menu
      }()
    
    @IBOutlet weak var doneLabel: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var checkMale: UIButton!
    @IBOutlet weak var checkFemale: UIButton!
    @IBOutlet weak var nameTextview: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var selectCountryButton: UIButton!
    @IBOutlet weak var postalCodeTextField: UITextField!
    @IBOutlet weak var dobtextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var stateTextview: UITextField!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailAddressLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var postalCodelLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var maleLabel: UILabel!
    @IBOutlet weak var femaleLabel: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(red: 38/255, green: 56/255, blue: 75/255,alpha: 1)
        indicator.isHidden = true
        nameLabel.font = UIFont(name: "Open Sans Regular", size: 15)
        emailAddressLabel.font = UIFont(name: "Open Sans Regular", size: 15)
        cityLabel.font = UIFont(name: "Open Sans Regular", size: 15)
        stateLabel.font = UIFont(name: "Open Sans Regular", size: 15)
        postalCodelLabel.font = UIFont(name: "Open Sans Regular", size: 15)
        countryLabel.font = UIFont(name: "Open Sans Regular", size: 15)
        dateOfBirthLabel.font = UIFont(name: "Open Sans Regular", size: 15)
        genderLabel.font = UIFont(name: "Open Sans Regular", size: 15)
        maleLabel.font = UIFont(name: "Open Sans Regular", size: 15)
        femaleLabel.font = UIFont(name: "Open Sans Regular", size: 15)
        nameTextview.font = UIFont(name: "Montserrat-Thin", size: 14)
        nameTextview .textColor = .white
        nameTextview.attributedPlaceholder = NSAttributedString(string: "Name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        emailTextField.font = UIFont(name: "Montserrat-Thin", size: 14)
        emailTextField.textColor = .white
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email Address",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        cityTextField.font = UIFont(name: "Montserrat-Thin", size: 14)
        cityTextField.textColor = .white
        cityTextField.attributedPlaceholder = NSAttributedString(string: "City",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        stateTextview.font = UIFont(name: "Montserrat-Thin", size: 14)
        stateTextview.textColor = .white
        stateTextview.attributedPlaceholder = NSAttributedString(string: "State",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        postalCodeTextField.font = UIFont(name: "Montserrat-Thin", size: 14)
        postalCodeTextField.textColor = .white
        postalCodeTextField.attributedPlaceholder = NSAttributedString(string: "Postal code",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        postalCodeTextField.keyboardType = .numberPad
        selectCountryButton.titleLabel?.font = UIFont(name: "Montserrat-Thin", size: 14)
        selectCountryButton.titleLabel?.textColor = .white
        dobtextField.font = UIFont(name: "Montserrat-Thin", size: 14)
        dobtextField.textColor = .white
        doneLabel.titleLabel?.font = UIFont(name: "Montserrat-Thin", size: 15)
        refereshProfile()
        cityTextField.allowsEditingTextAttributes = false
        menu2.selectionAction = { index,title in
            self.cityTextField.text = title
        }
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(openDropdown2))
        gesture2.numberOfTapsRequired = 1
        cityTextField.addGestureRecognizer(gesture2)
        menu2.anchorView = cityTextField.textInputView
       AF.request("https://gist.githubusercontent.com/ahmu83/38865147cf3727d221941a2ef8c22a77/raw/c647f74643c0b3f8407c28ddbb599e9f594365ca/US_States_and_Cities.json",method: .get).validate().responseJSON{ (response) in
          //print("aaa")
          //debugPrint(response)
          //print("aaa")
        switch response.result{
        case .success(let value):
            self.swiftyJsonVar = JSON(response.value!)
              print(self.stateTextview.text as Any)
              let a = self.swiftyJsonVar[(self.stateTextview.text)!].arrayObject ?? ["Select State first"]
              self.menu2.dataSource = a as! [String]
            break
        case .failure(let error):
            self.swiftyJsonVar = JSON()
            break
        }
              
        }
        
        
        if fromVerificationScreen {
            backButton.isHidden = true
        }
        if let data = UserDefaults.standard.data(forKey: "USER"){
              do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(User.self, from: data)
                print(user.self)
                self.user = user
                //self.userName.text = user.name
                //self.userImage.image = UIImage(named: user.profilepic)
              }catch{
                print("Error")
              }
            }
        createDatePicker()
        nameTextview.text = user?.name
        emailTextField.text = user?.email
        cityTextField.text = user?.city
        postalCodeTextField.text = user?.zip
        //countryTextField.text = user?.country ?? ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd-MM-yyyy"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMMM yyyy"

        if let date = dateFormatterGet.date(from: user!.dob) {
            self.dobtextField.text = dateFormatterPrint.string(from: date)
        } else {
           print("There was an error decoding the string")
        }
        
        flag = user?.gender ?? 2
        if flag == 1 {
            checkMale.isSelected = true
            checkFemale.isSelected = false
        }
        else if flag == 0 {
            checkFemale.isSelected = true
            checkMale.isSelected = false
        }
        stateTextview.text = user?.state
        /*AF.download(self.user?.profilepic ?? "").responseData { response in
            switch response.result {
            case let .success(value):
                //UserDefaults.standard.setValue(value, forKey: "image")
                 let image = UIImage(data: value)
                 self.userImageView.image = image
            case let .failure(error):
                return
            }
        }*/
        self.userImageView.sd_setImage(with: URL(string: self.user!.profilepic ), placeholderImage: UIImage(named: "userprofile.png"))
        
        let requestService = SCService <ApiCallGetCountryAndCity.Request , ApiCallGetCountryAndCity.Response> ()
            requestService.request("https://countriesnow.space/api/v0.1/countries/states", method: .get, parameters: nil, encoder: JSONParameterEncoder()){ [self](result) in
              switch result{
              case .success(let response):
                
                for item in response.data! {
                    if item.name == "United States" {
                        for item in item.states{
                      menuString.append(item.name)
                    }
                    menu.dataSource = menuString
                    
                  }
                }
                //debugPrint(response)
              case .failure(let error):
                debugPrint(error)
              }
            }
        let gesture = UITapGestureRecognizer(target: self, action: #selector(openDropdown))
            gesture.numberOfTapsRequired = 1
            stateTextview.addGestureRecognizer(gesture)
            menu.anchorView = stateTextview.textInputView
            stateTextview.allowsEditingTextAttributes = false
        menu.selectionAction = { index,title in
            self.stateTextview.text = title
            //UserDefaults.standard.set(title, forKey: "STATE")
            //print(self.swiftyJsonVar)
            self.cityTextField.text = "Select City"
            if self.swiftyJsonVar.exists(){
                if let resData = self.swiftyJsonVar[title].arrayObject {
                     self.menu2.dataSource = resData as! [String]
                }
            }
        }
        /*selectedCountry = user?.country ?? ""
        if selectedCountry != nil{
            selectCountryButton.setTitle("  \(user?.country ?? "United States")", for: .normal)
        }*/
        // Do any additional setup after loading the view.
        //var countrypicker = countryPickerView()
    }
    func refereshProfile(){
        indicator.isHidden = false
        self.indicator.startAnimating()
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallProfile.Request,ApiCallProfile.Response>()
        requestService.request("http://3.143.193.45:1337/api/profile", method: .post, parameters: nil, encoder: JSONParameterEncoder(), header: header){ [self](result) in
            switch result{
            case .success(let response):
                self.indicator.isHidden = true
                
                debugPrint(response)
                self.user = response.data?.user
                self.nameTextview.text = response.data!.user.name
                self.emailTextField.text = response.data!.user.email
                self.cityTextField.text = response.data!.user.city
                
                self.postalCodeTextField.text = response.data!.user.zip
                self.dobtextField.text = response.data!.user.dob
                //countryTextField.text = user?.country ?? ""
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "dd-MM-yyyy"
                
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "dd MMMM yyyy"

                if let date = dateFormatterGet.date(from: response.data!.user.dob) {
                    self.dobtextField.text = dateFormatterPrint.string(from: date)
                } else {
                   print("There was an error decoding the string")
                }
                self.stateTextview.text = user?.state
                self.flag = response.data!.user.gender ?? 2
                if flag == 1 {
                    checkMale.isSelected = true
                    checkFemale.isSelected = false
                }
                else if flag == 0 {
                    checkFemale.isSelected = true
                    checkMale.isSelected = false
                }
                self.userImageView.sd_setImage(with: URL(string: response.data!.user.profilepic ), placeholderImage: UIImage(named: "userprofile.png"))
                //self.member = response.data.members
                //self.familymembers.reloadData()
                //self.userNameLabel.text = response.data.user.name
                ///self.userDobLabel.text = response.data.user.dob
                //                let dateFormatterGet = DateFormatter()
                //                dateFormatterGet.dateFormat = "dd-MM-yyyy"
                //
                //                let dateFormatterPrint = DateFormatter()
                //                dateFormatterPrint.dateFormat = "dd MMMM yyyy"
                //
                //                if let date = dateFormatterGet.date(from: response.data.user.dob) {
                //                    self.userDobLabel.text = dateFormatterPrint.string(from: date)
                //                } else {
                //                   print("There was an error decoding the string")
                //                }
                //if response.data.user.country != "" && response.data.user.state != "" {
                //    self.userCountryStateLabel.text = "\(response.data.user.city) , \(response.data.user.country)"
                //}
                /*AF.download(self.user!.profilepic).responseData { response in
                    switch response.result {
                    case let .success(value):
                        //UserDefaults.standard.setValue(value, forKey: "image")
                         let image = UIImage(data: value)
                         self.userProfileImage.image = image
                    case let .failure(error):
                        return
                    }
                }*/
                do {
                    let encoder = JSONEncoder()
                    let data = try encoder.encode(response.data!.user)
                    UserDefaults.standard.setValue(data, forKey: "USER")
                } catch{
                    print("Error")
                }
                //self.userProfileImage.sd_setImage(with: URL(string: self.user!.profilepic ), placeholderImage: UIImage(named: "userprofile.png"))
                
            case .failure(let error):
                debugPrint(error)
                self.indicator.isHidden = true
            }
            
        }
    }
    func makeAlert(AlertTilte: String, AlertMessage: String){
        let alert = UIAlertController(title: AlertTilte, message: AlertMessage, preferredStyle: UIAlertController.Style.alert)
        let Okbutton = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) { (UIAlertAction) in
            if self.fromVerificationScreen {
                guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "ProfileStoryboardId")  as? ProfileViewController else {
                    return
                }
                menuViewController.modalPresentationStyle = .fullScreen
                self.present(menuViewController, animated: true,completion: nil)
            }
            else{
                self.dismiss(animated: true, completion: nil)
            }
        }
        alert.addAction(Okbutton)
        self.present(alert, animated: true, completion: nil)
    }
    func makeAlert2(AlertTilte: String, AlertMessage: String){
        let alert = UIAlertController(title: AlertTilte, message: AlertMessage, preferredStyle: UIAlertController.Style.alert)
        let Okbutton = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default)
        alert.addAction(Okbutton)
        self.present(alert, animated: true, completion: nil)
    }
    /// Date label toolbar
    func createToolBar() -> UIToolbar {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donepressed))
        toolbar.setItems([doneBtn], animated: true)
        return toolbar
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    /// Date input keyboard
    func createDatePicker(){
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = .date
        dobtextField.inputView = datePicker
        dobtextField.inputAccessoryView = createToolBar()
    }
    /// Date toolbar action button functionality
    @objc func donepressed(){
        let currentdate = Date()
        let dateFormattor1 = DateFormatter()
        dateFormattor1.dateStyle = .medium
        dateFormattor1.timeStyle = .none
        let dateFormattor = DateFormatter()
        dateFormattor.dateStyle = .medium
        dateFormattor.timeStyle = .none
        if datePicker.date > currentdate{
            //self.dobtextField.text = "\(currentdate)"
            //self.dobtextField.text = dateFormattor.string(from: currentdate)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            let year: String = dateFormatter.string(from: currentdate)
            
            dateFormatter.dateFormat = "MM"
            let month: String = dateFormatter.string(from: currentdate)
            dateFormatter.dateFormat = "MMMM"
            let monthName: String = dateFormatter.string(from: currentdate)
            
            
            dateFormatter.dateFormat = "dd"
            let day: String = dateFormatter.string(from: currentdate)
            
            date = "\(day)-\(month)-\(year)"
            dobtextField.text = "\(day) \(monthName) \(year)"
            view.endEditing(true)
        }
        else {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            let year: String = dateFormatter.string(from: self.datePicker.date)
            dateFormatter.dateFormat = "MM"
            let month: String = dateFormatter.string(from: self.datePicker.date)
            dateFormatter.dateFormat = "MMMM"
            let monthName: String = dateFormatter.string(from: self.datePicker.date)
            dateFormatter.dateFormat = "dd"
            let day: String = dateFormatter.string(from: self.datePicker.date)
            date = "\(day)-\(month)-\(year)"
            
            
            dobtextField.text = "\(day) \(monthName) \(year)"
            view.endEditing(true)
        }
    }
    
    
    
    @IBAction func selectCountry(_ sender: Any) {
        //print("clicked")
        
        country.dataSource = self
        country.delegate = self
        //country.showCountriesList(from: self)
        
    }
    
    
    @IBAction func checkMale(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            checkFemale.isSelected = true
            flag = 0
        }
        else {
            sender.isSelected = true
            checkFemale.isSelected = false
            flag = 1
        }
        print(flag)
    }
    
    @IBAction func checkFemale(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            checkMale.isSelected = true
            flag = 1
            
        }
        else {
            sender.isSelected = true
            checkMale.isSelected = false
            flag = 0
        }
        print(flag)
    }
    @objc func openDropdown() {
        menu.show()
      }
    
    
    @IBAction func doneClicked(_ sender: Any) {
        if nameTextview.text == "" {
            makeAlert2(AlertTilte: "Name Missing", AlertMessage: "Enter a valid name")
        }
        else if nameTextview.text!.count <= 3 {
            makeAlert2(AlertTilte: "Name Error", AlertMessage: "Name too short")
        }
        else if emailTextField.text == "" {
            makeAlert2(AlertTilte: "Email Missing", AlertMessage: "Enter a valid email")
        }
        
        
        
        else if selectedCountry == "" {
            let alert = UIAlertController(title:"",message: "Please Select A Country", preferredStyle: .alert)
            let uiOkAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) { (UIAlertAction) in
                
            }
            alert.addAction(uiOkAction)
            self.present(alert, animated: true, completion: nil)
        }
        else if stateTextview.text == "" {
            self.makeAlert2(AlertTilte: "Error", AlertMessage: "Please select a state")
        }
        else if cityTextField.text == "Select City" || cityTextField.text == "" {
            self.makeAlert2(AlertTilte: "Error", AlertMessage: "Please select a city")
        }
        else if postalCodeTextField.text == "" {
            makeAlert2(AlertTilte: "Postal Code Missing", AlertMessage: "Enter valid postal code")
        }
        
        else {
            indicator.isHidden = false
            indicator.startAnimating()
            let parameter = ApiCallUpdateProfile.Request(name: nameTextview.text!, password: "", zip: postalCodeTextField.text!, state: stateTextview.text!, country: "United States", dob: dobtextField.text!, city: cityTextField.text!, email: emailTextField.text!, gender: flag)
        print(flag)
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallUpdateProfile.Request,ApiCallUpdateProfile.Response>()
        requestService.request("http://3.143.193.45:1337/api/updateprofile", method: .post, parameters: parameter, encoder: JSONParameterEncoder(), header: header){ [self](result) in
            switch result{
            case .success(let response):
                debugPrint(response)
                indicator.isHidden = true
                if response.status == true{
                do {
                let encoder = JSONEncoder()
                    user?.name = parameter.name
                              user?.email = parameter.email
                              user?.city = parameter.city
                              user?.state = parameter.state
                              user?.country = parameter.country
                              user?.zip = parameter.zip
                              user?.dob = parameter.dob
                              user?.gender = parameter.gender
                let data = try encoder.encode(user)
                UserDefaults.standard.setValue(data, forKey: "USER")
                } catch{
                    print("Error")
                }
                self.delegate?.refereshProfile()
                self.makeAlert(AlertTilte: "Status", AlertMessage: response.message)
                //delegate?.refereshProfile()
                }else{
                    self.showPromptAlert("Sorry!", message: response.message);
                }
            case .failure(let error):
                debugPrint(error)
                indicator.isHidden = true
            }
        }
    }
}
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @IBAction func cancelClicked(_ sender: Any) {
        self.delegate?.refereshProfile()
        dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func cameraImageClicked(_ sender: Any) {
        
        CameraAlert(AlertTilte: "Select option", AlertMessage: "")
    }
    @objc func openDropdown2() {
        menu2.show()
      }
    }
extension EditProfileViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var selectedImage: UIImage?
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        userImageView.image = image
        let imageData = (userImageView.image?.jpegData(compressionQuality: 0.8))!
        let headers : HTTPHeaders = ["Content-Type": "application/json","authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
                // https://stackoverflow.com/a/40521003
        dismiss(animated: true, completion: nil)
        indicator.isHidden = false
        indicator.startAnimating()
        AF.upload(multipartFormData: {  MultipartFormData in
            MultipartFormData.append(imageData, withName: "profilepic" , fileName: "profilepic.jpeg" , mimeType: "image/jpeg")
        }, to: "http://3.143.193.45:1337/api/profilepic", headers: headers)
        .response { response in
            //response.data
            let date = response.data
            switch response.result{
            case let .success(value):
                self.indicator.isHidden = true
                    let jsonDecoder = JSONDecoder()
                    do{
                        let apiResponseLogin : ApiCallProfilePic.Response = try jsonDecoder.decode(ApiCallProfilePic.Response.self, from:date!)
                        print(apiResponseLogin)
                        if apiResponseLogin.status{
                            let alert = UIAlertController(title:"Update Profile Pic",message: apiResponseLogin.message, preferredStyle: .alert)
                            let uiOkAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) { (UIAlertAction) in
                                do {
                                                let encoder = JSONEncoder()
                                    self.user?.profilepic = apiResponseLogin.data!.profilepic
                                    let data = try encoder.encode(self.user)
                                                UserDefaults.standard.setValue(data, forKey: "USER")
                                                  print(UserDefaults.standard.object(forKey: "USER"))
                                                } catch{
                                                  print("Error")
                                                }
                            }
                            alert.addAction(uiOkAction)
                            self.present(alert, animated: true, completion: nil)
                        }else{
                            let alert = UIAlertController(title:"Update Profile Pic",message: "Failed to Upload", preferredStyle: .alert)
                            let uiOkAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) { (UIAlertAction) in
                               
                            }
                            alert.addAction(uiOkAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }catch{
                        print(error)
                    }
                
                break
            case let .failure(error):
                self.indicator.isHidden = true
                let alert = UIAlertController(title:"Update Profile Pic",message:error.errorDescription, preferredStyle: .alert)
                let uiOkAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) { (UIAlertAction) in
                    
                }
                alert.addAction(uiOkAction)
                self.present(alert, animated: true, completion: nil)
                
                break
                
            }
                
            
                debugPrint(response)
        }
    }
    func CameraAlert(AlertTilte: String, AlertMessage: String){
        let alert = UIAlertController(title: AlertTilte, message: AlertMessage, preferredStyle: UIAlertController.Style.alert)
        let Camerabutton = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) { [self] UIAlertAction in
            
            self.picker.sourceType = .camera
            self.picker.delegate = self
            self.present(self.picker, animated: true)
            
        }
        let Gallerybutton = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) { UIAlertAction in
            
            self.picker.sourceType = .photoLibrary
            self.picker.delegate = self
            self.present(self.picker, animated: true)
            
        }
        alert.addAction(Camerabutton)
        alert.addAction(Gallerybutton)
        self.present(alert, animated: true, completion: nil)
    }
}
extension EditProfileViewController  : CountryPickerViewDelegate,CountryPickerViewDataSource{
  
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        selected = 1
        selectedCountry = country.name
        selectCountryButton.setTitle("  \(country.name)", for:.normal )
    }
    
    
}
