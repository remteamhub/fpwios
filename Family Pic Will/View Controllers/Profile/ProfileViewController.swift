//
//  ProfileViewController.swift
//  FPW
//
//  Created by Apple on 29/04/2021.
//
import Foundation
import UIKit
import Alamofire
import SDWebImage

class ProfileViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource
                             ,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    @IBOutlet weak var InfoView: UIView!
    @IBOutlet weak var userProfileImage: UIImageView!
    var selectedMember : Int?
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    var member : [User] = []
    var membersDisplay : [User] = []
    var memberProfile : User?
    var user: User?
    var delegate :RefereshProfile?
    let transition = SlideInTransition()
    var type: String = ""
    var memberid = ""
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBAction func dismissClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func openSideMenuClicked(_ sender: Any) {
        if(memberProfile != nil)
        {
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "ProfileStoryboardId")  as? ProfileViewController else {
                return
            }
            

            //menuViewController.member = member[indexPath.row]
            menuViewController.modalPresentationStyle = .fullScreen
            self.present(menuViewController, animated: true,completion: nil)
        }
        else
        {
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "SideMenuStoryboardId") else {
              return
            }
            menuViewController.modalPresentationStyle = .fullScreen
            menuViewController.transitioningDelegate = self
            present(menuViewController, animated: false)
        }
        }
    @IBAction func toEditProfileClicked(_ sender: Any) {
        if user == nil {
            
            return
        }
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "EditProfileStoryboardId") as? EditProfileViewController else {
            return
        }
        menuViewController.user = self.user
        menuViewController.delegate = self
        menuViewController.modalPresentationStyle = .fullScreen
        self.present(menuViewController, animated: true, completion: nil)
    }

    

    @IBOutlet weak var titleBarLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userCountryStateLabel: UILabel!
    @IBOutlet weak var familymembers: UITableView!
    @IBOutlet weak var userDobLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        refereshProfile()
        self.familymembers.reloadData()

        if(memberProfile != nil)
        {
            let image2:UIImage? = UIImage(named: "back button.png")

            backButton.setImage(image2, for: UIControl.State.normal)
            editButton.isHidden=true
        }
        InfoView.dropShadow()
        
        transition.delegate = self
        if let data = UserDefaults.standard.data(forKey: "USER"){
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(User.self, from: data)
                print(user.self)
                self.user = user
            }catch{
                print("Error")
            }
        }
        indicator.isHidden = true
        userNameLabel.font = UIFont(name: "Open Sans Regular", size: 17)
        userDobLabel.font = UIFont(name: "Open Sans Regular", size: 17)
        titleBarLabel.font = UIFont(name: "Roboto-Regular", size: 20)
        familymembers.delegate = self
        familymembers.dataSource = self
        
        familymembers.separatorStyle = .none
        familymembers.showsVerticalScrollIndicator = false
        // Do any additional setup after loading the view.
        
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    func refereshProfile(){
        if(memberProfile != nil)
        {
            let param = ApiCallProfile.Request(nuser:self.memberProfile?.email)
            indicator.isHidden = false
            self.indicator.startAnimating()
            let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
            let requestService = SCService<ApiCallProfile.Request,ApiCallProfile.Response>()
            requestService.request("http://3.143.193.45:1337/api/getmemberprofile", method: .post, parameters: param, encoder: JSONParameterEncoder(), header: header){(result) in
                switch result{
                case .success(let response):
                    self.indicator.isHidden = true
                    if response.status == true{
                    debugPrint(response)
                        self.user = response.data?.user
                        self.member = response.data!.members

                        let x=self.member.count - 1
                        for index in 0...x
                        {
                            if(self.member[index].blocked == 0 && self.member[index].deceased == 0)
                            {
                                
                                self.membersDisplay.append(self.member[index])
                            }
                        }
                        self.member=self.membersDisplay
                    self.familymembers.reloadData()
                        self.userNameLabel.text = response.data!.user.name
                        self.userDobLabel.text = response.data!.user.dob
                 
                        if response.data!.user.country != "" && response.data!.user.state != "" {
                            self.userCountryStateLabel.text = "\(response.data!.user.city) , \(response.data!.user.country)"
                    }
                    do {
                        let encoder = JSONEncoder()
                        let data = try encoder.encode(response.data!.user)
                    } catch{
                        print("Error")
                    }
                    self.userProfileImage.sd_setImage(with: URL(string: self.user!.profilepic ), placeholderImage: UIImage(named: "userprofile.png"))
                    }else{
                        self.showPromptAlert("Sorry!", message: response.message);
                    }
                case .failure(let error):
                    debugPrint(error)
                    self.indicator.isHidden = true
                }
            }
        
        }
        else
        {
        indicator.isHidden = false
        self.indicator.startAnimating()
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallProfile.Request,ApiCallProfile.Response>()
        requestService.request("http://3.143.193.45:1337/api/profile", method: .post, parameters: nil, encoder: JSONParameterEncoder(), header: header){(result) in
            switch result{
            case .success(let response):
                self.indicator.isHidden = true
                if response.status == true{
                    UserDefaults.standard.setValue(response.data?.user.id, forKey: "uid")
                    UserDefaults.standard.setValue(response.data?.user.fcount, forKey: "fcount")
                    UserDefaults.standard.setValue(response.data?.user.plan, forKey: "plan")
                    UserDefaults.standard.setValue(response.data?.user.familyCode, forKey: "currentFamilyCode")

                debugPrint(response)
                    self.user = response.data?.user
                    self.member = response.data!.members
                    
                    print(self.member)
                    let x=self.member.count - 1
                    if(x > 0){
                    for index in 0...x
                    {
                        if(self.member[index].blocked == 0 && self.member[index].deceased == 0 )
                        {
                            self.membersDisplay.append(self.member[index])
                        }
                    }
                    }
                    self.member=self.membersDisplay

                self.familymembers.reloadData()
                    self.userNameLabel.text = response.data!.user.name
                    self.userDobLabel.text = response.data!.user.dob
             
                    if response.data!.user.country != "" && response.data!.user.state != "" {
                        self.userCountryStateLabel.text = "\(response.data!.user.city) , \(response.data!.user.country)"
                }
                do {
                    let encoder = JSONEncoder()
                    let data = try encoder.encode(response.data!.user)
                    UserDefaults.standard.setValue(data, forKey: "USER")
                } catch{
                    print("Error")
                }
                self.userProfileImage.sd_setImage(with: URL(string: self.user!.profilepic ), placeholderImage: UIImage(named: "userprofile.png"))
                }else{
                    self.showPromptAlert("Sorry!", message: response.message);
                }
            case .failure(let error):
                debugPrint(error)
                self.indicator.isHidden = true
            }
        }
    }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Clicked")
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "ProfileStoryboardId")  as? ProfileViewController else {
            return
            
        }
        
        menuViewController.memberProfile = member[indexPath.row]
//        menuViewController.member = member[indexPath.row]
        menuViewController.modalPresentationStyle = .fullScreen
        self.present(menuViewController, animated: true,completion: nil)

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return member.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MemberFamilyTableViewCell
        if(memberProfile != nil)
        {
            if(member[indexPath.row].relation != ""){
                var relation = "(" + member[indexPath.row].relation!
                relation = relation + ")"
                self.memberid = member[indexPath.row].id
                print("salmansohail\(memberid)")
                cell.nameMember.text = "\(member[indexPath.row].name)  \(relation ?? "")";
                cell.imageViewMember.sd_setImage(with: URL(string: self.member[indexPath.row].profilepic ), placeholderImage: UIImage(named: "userprofile.png"))
                
        }
            else
            {
                cell.nameMember.text = member[indexPath.row].name
                
                cell.imageViewMember.sd_setImage(with: URL(string: self.member[indexPath.row].profilepic ), placeholderImage: UIImage(named: "userprofile.png"))
            }
        cell.dropdownMember.isHidden=true
        }
        else
        {
            cell.nameMember.text = member[indexPath.row].name
            cell.dropdownMember.isHidden=false
            cell.imageViewMember.sd_setImage(with: URL(string: self.member[indexPath.row].profilepic ), placeholderImage: UIImage(named: "userprofile.png"))
        }
        selectedMember = indexPath.row
        cell.configuredData(row: indexPath.row, member: member[indexPath.row])
        cell.delegate = self
        cell.selectionStyle = .none
        if cell.dropdownMember.isHidden == true {
            
            cell.isUserInteractionEnabled = false
            
        }
        return cell
    }
}

extension ProfileViewController : UIViewControllerTransitioningDelegate{
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = true
    return transition
  }
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = false
    return transition
  }
}
extension ProfileViewController : CloseSideMenu{
    func closeSideMenu(){
        dismiss(animated: true, completion: nil)
    }
}
extension ProfileViewController : DataForCell,RefereshProfile{
  func buttonTap(title:Int){
    print("\(title)")
    selectedMember = title
    performSegue(withIdentifier: "dialogmember", sender: self)
  }
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "dialogmember"{
      let secondViewControler = segue.destination as! DialogMemberViewController
      secondViewControler.member =  member[selectedMember!]
       
    }
  }
}
extension UIView {
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = .zero
        layer.shadowRadius = 5
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}

