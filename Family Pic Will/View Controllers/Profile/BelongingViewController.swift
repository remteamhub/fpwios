//
//  BelongingViewController.swift
//  FPW
//
//  Created by Apple on 04/05/2021.
//
import UIKit
import SwiftyJSON
import Alamofire
import AVKit
import MobileCoreServices
import AVFoundation
class BelongingViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate,UITextViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    var posttid : String = ""
    var SECOND_MILLIS: Int = 1000
    var MINUTE_MILLIS: Int = 60
    var HOUR_MILLIS: Int = 60
    var DAY_MILLIS: Int = 24
    var nextpage: Int = 0
    var pagecheck: String = ""
   
    var labelll : String = "backtoMembersProfile"
   
    var imgpicker = UIImagePickerController()
    var captionString : String = "default string caption "
    
    var posts: [Posts] = []
    var playerViewController = AVPlayerViewController()
      var playerView = AVPlayer()
    
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var userNamelabel: UILabel!
    @IBOutlet weak var userCountryCity: UILabel!
    @IBOutlet weak var cancelClicked: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var user: User?
    var member : User?
    var password : String?
    var delegete : RefereshProfile?
    @IBAction func cancalClickedBtn(_ sender: Any) {
        delegete?.refereshProfile()
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "ProfileStoryboardId") as? ProfileViewController else {
            return
        }
        let presentingVC = self.presentingViewController
        menuViewController.modalPresentationStyle = .fullScreen
        dismiss(animated: true) {
            presentingVC!.present(menuViewController, animated: true,completion: nil)
        }
    }
    @IBOutlet weak var belongTableview: UITableView!
    func getuser(){
        if let data = UserDefaults.standard.data(forKey: "USER"){
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(User.self, from: data)
                print(user.self)
                
                self.user = user
               
                
            }catch{
                print("Error")
            }
        }
    }
    func currentTimeInMilliSeconds()-> Int
        {
            let currentDate = Date()
            let since1970 = currentDate.timeIntervalSince1970
            return Int(since1970 * 1000)
        }
    func getTime(`var` time: Int)-> String{
        var timm : Int = time
        if timm < 1000000000000 {
            timm  = timm * 1000
        }
        var now = currentTimeInMilliSeconds()
        if (timm > now || timm <= 0) {
            return " "
        }
        var diff = now - timm
        if (diff < MINUTE_MILLIS) {
                return "just now";
            } else if (diff < 2 * MINUTE_MILLIS) {
                return "a minute ago";
            } else if (diff < 60 * MINUTE_MILLIS) {
                return "\(diff / MINUTE_MILLIS) minutes ago";
            } /*else if (diff < 90 * MINUTE_MILLIS) {
                return "an hour ago";
            }*/ else if (diff < 24 * HOUR_MILLIS) {
                return "\(diff / HOUR_MILLIS) hours ago";
            } else if (diff < 48 * HOUR_MILLIS) {
                return "yesterday";
            } else {
                return "\(diff / DAY_MILLIS) days ago";
            }
        return ""
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        belongTableview.delegate = self
        belongTableview.dataSource = self
        belongTableview.separatorStyle = .none
        belongTableview.showsVerticalScrollIndicator = false
        self.indicator.isHidden = false
        self.MINUTE_MILLIS = self.MINUTE_MILLIS * self.SECOND_MILLIS
        self.HOUR_MILLIS = self.HOUR_MILLIS * self.MINUTE_MILLIS
        self.DAY_MILLIS = self.DAY_MILLIS * self.HOUR_MILLIS
        // Do any additional setup after loading the view.
        getuser()
        self.userProfileImage.sd_setImage(with: URL(string: self.member!.profilepic ), placeholderImage: UIImage(named: "userprofile.png"))
        userNamelabel.text = member?.name
        if member?.country != "" {
            userCountryCity.text = "\(member?.city ?? "") , \(member?.country ?? "")"
        }else{
            userCountryCity.text = ""
        }
        
        getMemberUser()
        
    }
    
    func getMemberUser(){
        indicator.isHidden = false
        self.indicator.startAnimating()
        
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallGetProfileById.Request,ApiCallGetProfileById.Response>()
        let param = ApiCallGetProfileById.Request(id: member!.id, page: nextpage)
        requestService.request("http://3.143.193.45:1337/api/getprofilebyid2", method: .post, parameters: param, encoder: JSONParameterEncoder(), header: header){(result) in
            switch result{
            case .success(let response):
                if response.status{
                
                
                    self.member?.createdAt = response.data!.createdAt
                    self.member?.updatedAt = response.data!.updatedAt
                    self.member?.email = response.data!.email
                    self.member?.dob  = response.data!.dob
                    self.member?.family = response.data!.family
                    self.member?.familyCode = response.data!.familyCode
                    self.member?.fcmToken = response.data!.fcmToken
                    self.member?.gender = response.data!.gender
                    self.member?.id = response.data!.id
                    self.member?.zip = response.data!.zip
                    self.member?.name = response.data!.name
                    self.member?.profilepic = response.data!.profilepic
                    self.member?.state = response.data!.state
                    self.member?.san = response.data!.san
                    self.password = response.data?.pasword
                    self.member?.verify = response.data!.verify
                    self.member?.country = response.data!.country
                    self.member?.city = response.data!.city
                    self.posts.append(contentsOf: response.data!.wall.posts)
                    self.pagecheck = response.data!.wall.pages.pages
                print(self.posts)
                self.belongTableview.reloadData()
                debugPrint(response)
                
                self.userProfileImage.sd_setImage(with: URL(string: self.member!.profilepic), placeholderImage: UIImage(named: "userprofile.png"))
                self.userNamelabel.text = self.member?.name
                if self.member?.country != "" {
                    self.userCountryCity.text = "\(self.member?.city ?? "") , \(self.member?.country ?? "")"
                }else{
                    self.userCountryCity.text = ""
                }
                }else{
                    self.showPromptAlert("Sorry!", message: response.message);
                }
                self.indicator.isHidden = true
            case .failure(let error):
                debugPrint(error)
                self.indicator.isHidden = true
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       let lastItem = self.posts.count - 1
       if indexPath.row == lastItem {
           print("IndexRow\(indexPath.row)")
        if self.pagecheck != "" {
            self.nextpage = self.nextpage + 1
            getMemberUser()
              //Get data from Server
           }
       }
   }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
   
      }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        captionString = posts[indexPath.row].caption!
    //print("CAPTIOPNPIAON ICAPTIONCAPTIONCAPTIONCAPTION ][][][][][] === \(captionString)")
        
        
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.indicator.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            // show up your view controller
            self.indicator.isHidden = true
            
        }
        
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostProfileCell", for: indexPath) as! MemberProfilePostTableViewCell
        cell.islikedd = posts[indexPath.row].isliked!
        cell.VideoPlayerOutlet.isHidden = true
        if posts[indexPath.row].type == 1 || posts[indexPath.row].type == 0{
            cell.PostImage.image = UIImage(named: "media_phto" )
            cell.PostImage.load(urlString: self.posts[indexPath.row].image)
        }
        if posts[indexPath.row].type == 2{
            cell.PostImage.image = UIImage(named: "media_video" )
            cell.PostImage.load(urlString: self.posts[indexPath.row].thumb)
        }
        cell.UserImage_Comment.image = UIImage(named: self.user!.profilepic ?? "user2.png")
        
        
        cell.delegate = self
        cell.typee = "\(posts[indexPath.row].type)"
                cell.postid = posts[indexPath.row].id
        
        if posts[indexPath.row].isliked == 1 {
            cell.likebtnoutlet.setImage(UIImage(named: "likeFilled"), for: .normal)
        }else{
            cell.likebtnoutlet.setImage(UIImage(named: "11222"), for: .normal)
        }
        cell.caption = posts[indexPath.row].caption!
        captionString = posts[indexPath.row].caption!
        cell.NumberOfComments_Post.text = "View all \(posts[indexPath.row].commentcount) comments"
        cell.PostLikes.text = "\(posts[indexPath.row].likecount)Likes"
        cell.UserName.text = posts[indexPath.row].postername
        if(self.posts[indexPath.row].posterimage == "")
        {
            cell.UserImage.image = UIImage(named:  "user2.png")
        }
        else{
        cell.UserImage.load(urlString: self.posts[indexPath.row].posterimage! )
        }
        
//        if posts[indexPath.row].posterimage != "" && posts[indexPath.row].posterimage != nil{
//            let url = URL(string: posts[indexPath.row].posterimage ?? "")
//        let data = try? Data(contentsOf: url!)
//
//        if let imageData = data {
//            cell.UserImage.image = UIImage(data: imageData)
//        }}
        cell.postDescriptionLabel.text = posts[indexPath.row].text
        cell.PostTime.text = self.getTime(var: posts[indexPath.row].createdAt)
        return cell
    }
    

    func imageviewer(imageStr: String , caption : String){
        zoomimage(IMage: imageStr, caption: caption)
    }
    
    func zoomimage(IMage: String, caption: String){
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "photoShow") as? detailedImg else {
              return
            }
        menuViewController.name = IMage
        menuViewController.captionTxt = caption
        menuViewController.albumname = "ursa1998"
        menuViewController.modalPresentationStyle = .overFullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
    }
}

extension BelongingViewController : PofilePostsDataForCcell{
    func playVideo(videoUrl: String , caption : String) {
        
        playvideo(videoUrl: videoUrl, caption: caption)
        
    
    }
    
    func playvideo(videoUrl : String , caption : String) {
        
        
   //     print ("function called playvideo caption ====== \(captionString)")
        
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "VideoPlayerViewController") as? VideoPlayerViewController else {return}
     //   playVideo(videoUrl: array[indexPath.row].video!)
        
//            guard let menuViewController = storyboard?.instantiateViewController(identifier: "CustomVPViewController") as? CustomVPViewController else {return}
//

        menuViewController.urlString = String (videoUrl)
        
        menuViewController.captionTxtVideo = caption
        menuViewController.modalPresentationStyle = .overFullScreen


         self.present(menuViewController, animated: true, completion: nil)
        
        
//        let url: URL = URL(string: videoUrl)!
//        playerView = AVPlayer(url: url)
//        playerViewController.player = playerView
//        self.present(playerViewController, animated: true)
//        self.playerViewController.player?.play()
//
//        let videoURL = URL(string: videoUrl)!
//        let player = AVPlayer(url: videoURL)
//        player.rate = 1 //auto play
////
////        CGPoint superCenter = CGPointMake(CGRectGetMidX([superview bounds]), CGRectGetMidY([superview bounds]))
////
//        let playerFrame = CGRect(x: 0, y: view.frame.height/9, width: view.frame.width, height: view.frame.height/1.6)
//        let playerViewController = AVPlayerViewController()
//        playerViewController.player = player
//        playerViewController.view.frame = playerFrame
//
//        addChild(playerViewController)
//        view.addSubview(playerViewController.view)
//        playerViewController.didMove(toParent: self)
//
//        playerViewController.showsPlaybackControls = true
    }
    
   
    func seguetocomments(possttId : String) {
        posttid = possttId
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "commentStoryboardID") as? CommentsViewController else {
              return
            }
       
        menuViewController.member = member
        menuViewController.postId = posttid
        menuViewController.user = self.user
        menuViewController.labelll = labelll
        menuViewController.modalPresentationStyle = .overFullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)

    }
    
    func seguetolikers(possttId : String){
        posttid = possttId
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "likeStoryboardID") as? PostLikesViewController else {
              return
            }
       
        menuViewController.member = member
        menuViewController.postId = posttid
        menuViewController.labelll = labelll
        
        menuViewController.modalPresentationStyle = .overFullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
        
    }
   
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
}


