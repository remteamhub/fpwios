//
//  SideMenuViewController.swift
//  FPW
//
//  Created by Apple on 30/04/2021.
//

import UIKit
import Alamofire

class SideMenuViewController: UIViewController , UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var userImage: UIImageView!
    var user : User?
    var option : [String] = [" Profile"," News Feed"," Wall"," Media"," Messages"," Notifications"," Settings"," Abuse Report"," Subscription Plan","About Us"," Logout"]
    var imageOption : [String] = ["profile1","newsfeed123","wall1","media","message 1","Notification","setting","Reporticon","payment icon","infoo","logout"]
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return option.count
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    @IBAction func closeSideMenu(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SideOptionTableViewCell
        cell.labelOptionName.text = option[indexPath.row]
        cell.imageOption.image = UIImage(named: imageOption[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //dismiss(animated: true, completion: nil)
        switch indexPath.row {
        case 0:
            //dismiss(animated: true, completion: nil)
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "ProfileStoryboardId") else {
                return
            }
            let presentingVC = self.presentingViewController
            menuViewController.modalPresentationStyle = .fullScreen
            dismiss(animated: false) {
                presentingVC!.present(menuViewController, animated: false,completion: nil)
            }
            break
            case 1:
                //dismiss(animated: true, completion: nil)
                guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "WallPostStoryboardId")
                as? WallPostViewController else {
                    return
                }
                let presentingVC = self.presentingViewController
                menuViewController.labelll = "NewsFeed"
                menuViewController.modalPresentationStyle = .fullScreen
                menuViewController.user = self.user
                dismiss(animated: true) {
                    presentingVC!.present(menuViewController, animated: true,completion: nil)
                }
                break//MessageStoryboardId
        case 2://WallPostStoryboardId
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "WallPostStoryboardId")
            as? WallPostViewController else {
                return
            }
            let presentingVC = self.presentingViewController
            menuViewController.labelll = "Wall"
            menuViewController.modalPresentationStyle = .fullScreen
            menuViewController.user = self.user
            dismiss(animated: true) {
                presentingVC!.present(menuViewController, animated: true,completion: nil)
            }
            break//MediaStoryboardId
        case 3://WallPostStoryboardId
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "MediaStoryboardId")
            as? MediaViewController else {
                return
            }
            let presentingVC = self.presentingViewController
            menuViewController.modalPresentationStyle = .fullScreen
            menuViewController.user = self.user
            dismiss(animated: true) {
                presentingVC!.present(menuViewController, animated: true,completion: nil)
            }
            
            
            case 4:
                //dismiss(animated: true, completion: nil)
                guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "MessageStoryboardId") as? MessageViewController else {
                    return
                }
                let presentingVC = self.presentingViewController
                menuViewController.modalPresentationStyle = .fullScreen
                menuViewController.user = self.user
                
                dismiss(animated: true) {
                    presentingVC!.present(menuViewController, animated: true,completion: nil)
                }
                break
        case 5:
            //dismiss(animated: true, completion: nil)
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "BirthdayStoryboardID") as? BirthDayViewController else {
                return
            }
            let presentingVC = self.presentingViewController
            menuViewController.modalPresentationStyle = .fullScreen
            dismiss(animated: true) {
                presentingVC!.present(menuViewController, animated: true,completion: nil)
            }
            break
        case 6:
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "SettingStoryboardId") else {
                return
            }
            let presentingVC = self.presentingViewController
            menuViewController.modalPresentationStyle = .fullScreen
            dismiss(animated: true) {
                presentingVC!.present(menuViewController, animated: true) {
                    //self.dismiss(animated: true, completion: nil)
                }
            }
            
        break
            
         case 7:
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "AbuseReportStoryboardId") else {
                return
            }
            let presentingVC = self.presentingViewController
            menuViewController.modalPresentationStyle = .fullScreen
            dismiss(animated: true) {
                presentingVC!.present(menuViewController, animated: true) {
                    //self.dismiss(animated: true, completion: nil)
                }
            }
            break
            
        case 8:
           guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "SubscriptionStoryboardId")
           else {
               return
           }
           let presentingVC = self.presentingViewController
           menuViewController.modalPresentationStyle = .fullScreen
           dismiss(animated: true) {
               presentingVC!.present(menuViewController, animated: true) {
                   //self.dismiss(animated: true, completion: nil)
               }
           }
            
            break
        case 9:
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "AboutUsStoryboardId") else {
                return
            }
            let presentingVC = self.presentingViewController
            menuViewController.modalPresentationStyle = .fullScreen
            dismiss(animated: true) {
                presentingVC!.present(menuViewController, animated: true) {
                    //self.dismiss(animated: true, completion: nil)
                }
            }
            
            break
        case 10:
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "SplashStoryboardID") else {
                return
            }
//            UserDefaults.standard.setValue(nil, forKey: "mail")
//            UserDefaults.standard.setValue(nil, forKey: "token")
//            UserDefaults.standard.setValue(nil, forKey: "USER")
//            UserDefaults.standard.setValue(nil, forKey: "fcount")
//            UserDefaults.standard.setValue(nil, forKey: "plan")
//            UserDefaults.standard.string(forKey: "currentFamilyCode")
            let presentingVC = self.presentingViewController
            menuViewController.modalPresentationStyle = .fullScreen
            dismiss(animated: true) {
                presentingVC!.present(menuViewController, animated: true) {
                    //self.dismiss(animated: true, completion: nil)
                }
            }
            UserDefaults.standard.setValue(nil, forKey: "mail")
            UserDefaults.standard.setValue(nil, forKey: "token")
            UserDefaults.standard.setValue(nil, forKey: "USER")
            UserDefaults.standard.setValue(nil, forKey: "fcount")
            UserDefaults.standard.setValue(nil, forKey: "plan")
            UserDefaults.standard.setValue(nil, forKey: "currentFamilyCode")
            
            break
            
        default :
            break
        }
        
    }
    
    @IBOutlet weak var sideMenuOption: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let data = UserDefaults.standard.data(forKey: "USER"){
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(User.self, from: data)
                print(user.self)
                self.userName.text = user.name
                self.user = user
                //self.userImage.image = UIImage(named: user.profilepic)
                /*AF.download(user.profilepic).responseData { response in
                    switch response.result {
                    case let .success(value):
                        //UserDefaults.standard.setValue(value, forKey: "image")
                         let image = UIImage(data: value)
                         self.userImage.image = image
                    case let .failure(error):
                        return
                    }
                }*/
                self.userImage.sd_setImage(with: URL(string: user.profilepic ), placeholderImage: UIImage(named: "userprofile.png"))
            }catch{
                print("Error")
            }
        }
        userName.font = UIFont(name: "OpenSans-Light", size: 21)
        sideMenuOption.delegate = self
        sideMenuOption.dataSource = self
        // Do any additional setup after loading the view.
        sideMenuOption.separatorStyle = .none
        sideMenuOption.showsVerticalScrollIndicator = false
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
