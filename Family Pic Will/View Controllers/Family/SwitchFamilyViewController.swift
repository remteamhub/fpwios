//
//  SwitchFamilyViewController.swift
//  FPW
//
//  Created by Apple on 29/04/2021.
//

import UIKit
import Alamofire
class SwitchFamilyViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var titleBarLabel: UILabel!
    @IBAction func backClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    var familyList:[Family]  = []
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        familyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FamilyTableViewCell
        let family = familyList[indexPath.row]
        cell.selectionStyle = .none
        cell.familyLabel.text = family.family
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var param = ApiCallChangeFmaily.Request(familyCode: familyList[indexPath.row].familyCode)
        //param?.familyCode = familyList[indexPath.row].familyCode
        print(param)
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallChangeFmaily.Request,ApiCallChangeFmaily.Response>()
        requestService.request("http://3.143.193.45:1337/api/changefamily", method: .post, parameters: param, encoder: JSONParameterEncoder(), header: header){(result) in
            switch result{
            case .success(let response):
                if response.status == true{
                debugPrint(response)
                UserDefaults.standard.set(response.data.familyCode, forKey: "currentFamilyCode")
                guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "ProfileStoryboardId") else {
                        return
                      }
                      let presentingVC = self.presentingViewController
                      menuViewController.modalPresentationStyle = .fullScreen
                      self.dismiss(animated: true) {
                        presentingVC!.present(menuViewController, animated: true) {
                          //self.dismiss(animated: true, completion: nil)
                        }
                      }
                }else{
                    self.showPromptAlert("Sorry!", message: response.message);
                }
            case .failure(let error):
                debugPrint(error)
            }
            
        }
    }

    @IBOutlet weak var familyListTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 38/255, green: 56/255, blue: 75/255,alpha: 1)
        indicator.isHidden = false
        indicator.startAnimating()
        titleBarLabel.font = UIFont(name: "Roboto-Regular", size: 20)
        familyListTableview.dataSource = self
        familyListTableview.delegate = self
        // Do any additional setup after loading the view.
        familyListTableview.separatorStyle = .none
        familyListTableview.showsVerticalScrollIndicator = false
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallGetFamilies.Request,ApiCallGetFamilies.Response>()
        requestService.request("http://3.143.193.45:1337/api/getfamilies", method: .post, parameters: nil, encoder: JSONParameterEncoder(), header: header){ [self](result) in
            switch result{
            case .success(let response):
                self.indicator.isHidden = true
                if response.status == true{
                debugPrint(response)
                    self.familyList = response.data!
                var a = 0
                for item in response.data! {
                   
                    if item.familyCode == UserDefaults.standard.string(forKey: "currentFamilyCode") {
                        self.familyList.remove(at: a)
                          }
                    a = a + 1
                        }
                
                self.familyListTableview.reloadData()
                }else{
                    self.showPromptAlert("Sorry!", message: response.message);
                }
            case .failure(let error):
                debugPrint(error)
                self.indicator.isHidden = true
            }
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    @objc func buttonBackTap(){
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
