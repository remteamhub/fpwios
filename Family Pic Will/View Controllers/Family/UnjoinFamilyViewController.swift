//
//  UnjoinFamilyViewController.swift
//  FPW
//
//  Created by Apple on 05/05/2021.
//

import UIKit
import Alamofire
class UnjoinFamilyViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate{
    
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var titleBarLabel: UILabel!
    var familyList : [Family] = []
    var delegate : RelodeFamilyList?
    @IBOutlet weak var tableViewFamily: UITableView!
    @IBAction func backClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 38/255, green: 56/255, blue: 75/255,alpha: 1)
        indicator.isHidden = true
        indicator.startAnimating()
        titleBarLabel.font = UIFont(name: "Roboto-Regular", size: 20)
        tableViewFamily.dataSource = self
        tableViewFamily.delegate = self
        // Do any additional setup after loading the view.
        tableViewFamily.separatorStyle = .none
        tableViewFamily.showsVerticalScrollIndicator = false
        
        loadFmailyLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    func loadFmailyLoad() {
        indicator.isHidden = false
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallGetFamilies.Request,ApiCallGetFamilies.Response>()
        requestService.request("http://3.143.193.45:1337/api/getfamilies", method: .post, parameters: nil, encoder: JSONParameterEncoder(), header: header){(result) in
            switch result{
            case .success(let response):
                debugPrint(response)
                if response.status == true{
                self.indicator.isHidden = true
                    self.familyList = response.data!
                var a = 0
                for item in response.data! {
                   
                    if item.familyCode == UserDefaults.standard.string(forKey: "currentFamilyCode") {
                        self.familyList.remove(at: a)
                          }
                    a = a + 1
                        }
                self.tableViewFamily.reloadData()
                }else{
                    self.showPromptAlert("Sorry!", message: response.message);
                }
            case .failure(let error):
                debugPrint(error)
                self.indicator.isHidden = true
            }
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return familyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! UnjoinFamilyTableViewCell
        cell.familyName.text = familyList[indexPath.row].family
        cell.configuredData()
        cell.selectionStyle = .none
        print(familyList[indexPath.row].familyCode)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //performSegue(withIdentifier: "unjoinFamilyDialog", sender: nil)
        guard let dialogViewController = self.storyboard?.instantiateViewController(identifier: "DialogUnjoinStoryboardId") as? DialogUnjoinFamilyViewController else {
            return
        }
        dialogViewController.delegate = self
        dialogViewController.family = familyList[indexPath.row]
        //dialogViewController.modalPresentationStyle = .fullScreen
        self.present(dialogViewController, animated: true,completion: nil)
    }
}
extension UnjoinFamilyViewController : RelodeFamilyList{
    func refereshFamily(){
        loadFmailyLoad()
    }
}
