//
//  WallPostViewController.swift
//  familywithwall
//
//  Created by Apple on 23/04/2021.
//

import UIKit
import SwiftyJSON
import Alamofire
import AVKit
import MobileCoreServices
import AVFoundation
import AuthenticationServices
class WallPostViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate,UITextViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var matchesarray : [Any] = []
    var SECOND_MILLIS: Int = 1000
    var MINUTE_MILLIS: Int = 60
    var HOUR_MILLIS: Int = 60
    var DAY_MILLIS: Int = 24
    var nextpage: Int = 0
    var checkReload: Bool = false
    var pagecheck: String = ""
    var posttid : String = ""
    var posttype: Int = 1
    var posttext = ""
    var imagesttt : String = ""
    var postTexttt : String = ""
    var postType : String = ""
    var labelll : String = ""
    var uploadtype: String = ""
    var captionString : String = "default string caption "
    var imgpicker = UIImagePickerController()
    var videoURL : URL?
    var videoAndImageReview = UIImagePickerController()
    var user: User?
    var playerViewController = AVPlayerViewController()
    var playerView = AVPlayer()
    var fdate: Int = 0
    var imagePicker = UIImagePickerController()
    var posts: [Posts] = []
    var coont = 3
    var imageUrl: URL?
    
    let transition = SlideInTransition()
    let screenHeight = UIScreen.main.bounds.height
    let scrollViewContentHeight = 1200 as CGFloat
    
    private var selectedimage: UIImage?
    
    @IBOutlet weak var Wallimage: UIImageView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var WallTabelView: UITableView!
    @IBOutlet weak var AddPost_UserImage: UIImageView!
    @IBOutlet weak var AddPost_TextView: UITextView!
    @IBOutlet weak var newsfeed_walllabel: UILabel!
    @IBAction func openSideMenuClicked(_ sender: Any) {
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "SideMenuStoryboardId") else {
              return
            }
        menuViewController.modalPresentationStyle = .overCurrentContext
        menuViewController.transitioningDelegate = self
        present(menuViewController, animated: true)
            
    }
    
    func getposts(`var` Url: String , var pageNo: Int){
        self.loading.isHidden = false
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let parameter = ApiCallPosts.Request(page: self.nextpage )
                let requestService = SCService<ApiCallPosts.Request,ApiCallPosts.Response>()
                requestService.request("\(Url)", method: .post, parameters: parameter, encoder: JSONParameterEncoder(), header: header){
                    (result) in
        
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == true{
                            if self.checkReload == true{
                                self.checkReload = false
                                
                            }
                        self.posts.append(contentsOf: response.data!.posts)
                        self.pagecheck = response.data!.pages.pages
                            DispatchQueue.main.async{
                            self.WallTabelView.reloadData()
                            }
                        self.loading.isHidden = true
                            
                        }else{
                            self.showPromptAlert("Sorry!", message: response.message);
                        }
                    case .failure(let error):
                        debugPrint(error)
                                
                    }
        
        
                }
        
    }
   
    func getuser(){
        if let data = UserDefaults.standard.data(forKey: "USER"){
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(User.self, from: data)
                self.user = user
               
                
            }catch{
            }
        }
    }
    
    func currentTimeInMilliSeconds()-> Int
        {
            let currentDate = Date()
            let since1970 = currentDate.timeIntervalSince1970
            return Int(since1970 * 1000)
        }
    
    func getTime(`var` time: Int)-> String{
        var timm : Int = time
        if timm < 1000000000000 {
            timm  = timm * 1000
        }
        let now = currentTimeInMilliSeconds()
        if (timm > now || timm <= 0) {
           let diff =  timm - now
            if (diff < 2 * MINUTE_MILLIS) {
                return "a minute remaining";
            }else if (diff < 60 * MINUTE_MILLIS) {
                return "\(diff / MINUTE_MILLIS) minutes remaining";
            }else if (diff < 24 * HOUR_MILLIS) {
                return "\(diff / HOUR_MILLIS) hours remaining";
            }else if (diff < 48 * HOUR_MILLIS) {
                return "tomorrow";
            }else {
                return "\(diff / DAY_MILLIS) days remaining";
            }
//            return "Future Post "
        }
        let diff = now - timm
        if (diff < MINUTE_MILLIS) {
                return "just now";
            } else if (diff < 2 * MINUTE_MILLIS) {
                return "a minute ago";
            } else if (diff < 60 * MINUTE_MILLIS) {
                return "\(diff / MINUTE_MILLIS) minutes ago";
            } /*else if (diff < 90 * MINUTE_MILLIS) {
                return "an hour ago";
            }*/ else if (diff < 24 * HOUR_MILLIS) {
                return "\(diff / HOUR_MILLIS) hours ago";
            } else if (diff < 48 * HOUR_MILLIS) {
                return "yesterday";
            } else {
                return "\(diff / DAY_MILLIS) days ago";
            }
        
    }
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.WallTabelView.register(UINib(nibName: "WallPostImageTableViewCell", bundle: nil),forCellReuseIdentifier: "cellimage")
        self.WallTabelView.register(UINib(nibName: "WallPostTextTableViewCell", bundle: nil), forCellReuseIdentifier: "celltext")
        transition.delegate = self
        WallTabelView.rowHeight = UITableView.automaticDimension
        WallTabelView.estimatedRowHeight = 511
        self.loading.startAnimating()
        self.SECOND_MILLIS = 1000
        self.MINUTE_MILLIS = 60
        self.HOUR_MILLIS = 60
        self.DAY_MILLIS = 24
        self.nextpage = 0
        self.checkReload = false
        self.MINUTE_MILLIS = self.MINUTE_MILLIS * self.SECOND_MILLIS
        self.HOUR_MILLIS = self.HOUR_MILLIS * self.MINUTE_MILLIS
        self.DAY_MILLIS = self.DAY_MILLIS * self.HOUR_MILLIS
        if self.labelll == "Wall"{
            self.getposts(var: "http://3.143.193.45:1337/api/getpost1", var: nextpage)
        }else{
            self.Wallimage.isHidden = true
            self.getposts(var: "http://3.143.193.45:1337/api/getnewsfeed1", var: nextpage)
        }
        getuser()
        newsfeed_walllabel.text = labelll
        WallTabelView.delegate = self
        WallTabelView.dataSource = self
        imagePicker.delegate = self
        AddPost_TextView.delegate = self
        WallTabelView.separatorStyle = .none
        AddPost_UserImage.layer.cornerRadius = AddPost_UserImage.frame.size.width / 2
        AddPost_UserImage.clipsToBounds = true
       // self.AddPost_UserImage.image = UIImage(named: self.user!.profilepic ?? "user2.png" )
        if(self.user?.profilepic != "")
        {
            self.AddPost_UserImage.load(urlString: self.user!.profilepic)
        }
        else
        {
            self.AddPost_UserImage.image = UIImage(named: "user2.png")
            
        }
        
        //?? UIImage(named: "user2.png")
        //self.AddPost_UserImage.sd_setImage(with: URL(string: self.user!.profilepic ), placeholderImage: UIImage(named: "user2.png"))
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tabelViewTapped))
        WallTabelView.addGestureRecognizer(gestureRecognizer)
        AddPost_TextView.textColor = .lightGray
        AddPost_TextView.text = "What's on your mind?"
        
    }
    
    
    @objc func tabelViewTapped() {
        self.AddPost_TextView.endEditing(true)
    }
    
    func textViewDidBeginEditing (_ textView: UITextView) {
       
        
            if AddPost_TextView.text.isEmpty || AddPost_TextView.text == "What's on your mind?" {
                AddPost_TextView.text = nil
                AddPost_TextView.textColor = .black // YOUR PREFERED COLOR HERE
            }
        
        
        }
    
    func textViewDidEndEditing (_ textView: UITextView) {
            

            if AddPost_TextView.text.isEmpty {
                AddPost_TextView.textColor = UIColor.gray // YOUR PREFERED PLACEHOLDER COLOR HERE
                AddPost_TextView.text =  "What's on your mind?"
            }
        }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastSectionIndex = tableView.numberOfSections - 1
           let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
           if indexPath.row == indexPath.row {
              
           // self.loading.isHidden = false
//            let spinner = UIActivityIndicatorView(style: .medium)
//                    spinner.startAnimating()
//                    spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
//
//            self.WallTabelView.tableHeaderView = spinner
//               self.WallTabelView.tableHeaderView?.isHidden = false
           }
        
       let lastItem = self.posts.count - 1
       if indexPath.row == lastItem {
        if self.pagecheck != "" {
            self.loading.isHidden = false
            self.nextpage = self.nextpage + 1
            if self.labelll == "Wall"{
                self.getposts(var: "http://3.143.193.45:1337/api/getpost1", var: self.nextpage)
            }else{
                self.Wallimage.isHidden = true
                self.getposts(var: "http://3.143.193.45:1337/api/getnewsfeed1", var: self.nextpage)
            }
           }
        if self.pagecheck == ""{
            self.loading.isHidden = true
        }
       }
   }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return posts.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      
        let cell = WallTabelView.dequeueReusableCell(withIdentifier: "PostCell", for: indexPath) as! WallPostCell
        
            if (self.posts[indexPath.row].type == 1) && (self.posts[indexPath.row].text == ""){
                let cell = self.WallTabelView.dequeueReusableCell(withIdentifier: "cellimage", for: indexPath) as! WallPostImageTableViewCell
            //cell.checkImage(imageStr: self.posts[indexPath.row].image)
            //cell.checkUserImage(imageStr: self.posts[indexPath.row].posterimage!)
                cell.PostImage.image = UIImage(named: "media_phto" )
                cell.PostImage.load(urlString: self.posts[indexPath.row].image)
                cell.imageStrr = self.posts[indexPath.row].image
              //  cell.PostImage.image = UIImage(data: self.posts[indexPath.row].image)
                //cell.UserImage.image = UIImage(named: self.posts[indexPath.row].posterimage! ?? "user2.png")
                if(self.posts[indexPath.row].posterimage == "")
                {
                    cell.UserImage.image = UIImage(named:  "user2.png")
                }
                else{
                cell.UserImage.load(urlString: self.posts[indexPath.row].posterimage! )
                }
                cell.isliked = posts[indexPath.row].isliked ?? 0
            cell.VideoPlayerOutlet.isHidden = true
            //cell.UserImage_Comment.sd_setImage(with: URL(string: self.user!.profilepic ), placeholderImage: UIImage(named: "user2.png"))
                if(self.user!.profilepic != "")
                {
                    cell.UserImage_Comment.load(urlString: self.user!.profilepic)
                }
                else{
                    cell.UserImage_Comment.image = UIImage(named: "user2.png")
                }
           // cell.UserImage_Comment.image = UIImage(named: self.user!.profilepic ?? "user2.png")
            cell.caption = posts[indexPath.row].caption!
            captionString = posts[indexPath.row].caption!
            cell.checkOptionBtn(PosterId: posts[indexPath.row].posterid, CurrentId: user!.id)
            cell.delegate = self
            cell.typee = "\(posts[indexPath.row].type)"
            cell.setimageZoomer()
            cell.postid = posts[indexPath.row].id
            if posts[indexPath.row].isliked == 1 {
                cell.likebtnoutlet.setImage(UIImage(named: "likeFilled"), for: .normal)
            }else{
                cell.likebtnoutlet.setImage(UIImage(named: "11222"), for: .normal)
            }
            cell.NumberOfComments_Post.text = "View all \(posts[indexPath.row].commentcount) comments"
            cell.PostLikes.text = "\(posts[indexPath.row].likecount)Likes"
            cell.UserName.text = posts[indexPath.row].postername
            cell.PostTime.text = self.getTime(var: posts[indexPath.row].createdAt)
            self.posttype = self.posts[indexPath.row].type
            return cell
            }
            if (self.posts[indexPath.row].type == 2) && (self.posts[indexPath.row].text == ""){
                let cell = self.WallTabelView.dequeueReusableCell(withIdentifier: "cellimage", for: indexPath) as! WallPostImageTableViewCell
            //    cell.checkVideo(imageStr: self.posts[indexPath.row].video, thuumb: self.posts[indexPath.row].thumb)
           // cell.checkUserImage(imageStr: self.posts[indexPath.row].posterimage!)
                cell.PostImage.image = UIImage(named: "media_video" )
                cell.PostImage.load(urlString: self.posts[indexPath.row].thumb)
                cell.imageStrr = self.posts[indexPath.row].video
                    //.image = UIImage(named: self.posts[indexPath.row].thumb)
                //cell.UserImage.image = UIImage(named: self.posts[indexPath.row].posterimage! ?? "user2.png")
                if(self.posts[indexPath.row].posterimage == "")
                {
                    cell.UserImage.image = UIImage(named:  "user2.png")
                }
                else{
                cell.UserImage.load(urlString: self.posts[indexPath.row].posterimage! )
                }
                cell.isliked = posts[indexPath.row].isliked ?? 0
            cell.VideoPlayerOutlet.isHidden = false
                cell.UserImage_Comment.image = UIImage(named: self.user!.profilepic ?? "user2.png")
            //cell.UserImage_Comment.sd_setImage(with: URL(string: self.user!.profilepic ), placeholderImage: UIImage(named: "user2.png"))
            cell.caption = posts[indexPath.row].caption!
            captionString = posts[indexPath.row].caption!
            cell.checkOptionBtn(PosterId: posts[indexPath.row].posterid, CurrentId: user!.id)
            cell.delegate = self
            cell.typee = "\(posts[indexPath.row].type)"
            cell.setimageZoomer()
            cell.postid = posts[indexPath.row].id
            if posts[indexPath.row].isliked == 1 {
                cell.likebtnoutlet.setImage(UIImage(named: "likeFilled"), for: .normal)
            }else{
                cell.likebtnoutlet.setImage(UIImage(named: "11222"), for: .normal)
            }
            cell.NumberOfComments_Post.text = "View all \(posts[indexPath.row].commentcount) comments"
            cell.PostLikes.text = "\(posts[indexPath.row].likecount)Likes"
            cell.UserName.text = posts[indexPath.row].postername
            cell.PostTime.text = self.getTime(var: posts[indexPath.row].createdAt)
            self.posttype = self.posts[indexPath.row].type
            return cell
            }
        DispatchQueue.main.async {
        }
            if (self.posts[indexPath.row].type == 0){
            let cell = self.WallTabelView.dequeueReusableCell(withIdentifier: "celltext", for: indexPath) as! WallPostTextTableViewCell
                if(self.posts[indexPath.row].posterimage != ""){
                cell.UserImage.load(urlString: self.posts[indexPath.row].posterimage!)
                }
                else
                {
                    cell.UserImage.image = UIImage(named: "user2.png")
                }
            //cell.checkUserImage(imageStr: self.posts[indexPath.row].posterimage!)
                cell.isliked = self.posts[indexPath.row].isliked ?? 0
                if(self.user!.profilepic != "")
                {
                    cell.UserImage_Comment.load(urlString: self.user!.profilepic)
                }
                else{
                    cell.UserImage_Comment.image = UIImage(named: "user2.png")
                }
                //cell.UserImage_Comment.load(urlString: self.user!.profilepic)
        //cell.UserImage_Comment.sd_setImage(with: URL(string: self.user!.profilepic ), placeholderImage: UIImage(named: "user2.png"))
                cell.caption = self.posts[indexPath.row].caption!
                self.captionString = self.posts[indexPath.row].caption!
                cell.checkOptionBtn(PosterId: self.posts[indexPath.row].posterid, CurrentId: self.user!.id)
        cell.delegate = self
                cell.typee = "\(self.posts[indexPath.row].type)"
                cell.postid = self.posts[indexPath.row].id
                if self.posts[indexPath.row].isliked == 1 {
            cell.likebtnoutlet.setImage(UIImage(named: "likeFilled"), for: .normal)
        }else{
            cell.likebtnoutlet.setImage(UIImage(named: "11222"), for: .normal)
        }
                cell.NumberOfComments_Post.text = "View all \(self.posts[indexPath.row].commentcount) comments"
                cell.PostLikes.text = "\(self.posts[indexPath.row].likecount)Likes"
                cell.UserName.text = self.posts[indexPath.row].postername
                cell.postDescriptionLabel.text = self.posts[indexPath.row].text
        self.posttext = cell.postDescriptionLabel.text ?? ""
                print("post text is \(self.posttext)")
                cell.PostTime.text = self.getTime(var: self.posts[indexPath.row].createdAt)
        self.posttype = self.posts[indexPath.row].type
        return cell
        }
        DispatchQueue.main.async {
            
        
            if (self.posts[indexPath.row].type == 1){
                //cell.PostImage.image = UIImage(named: self.posts[indexPath.row].image)
                cell.PostImage.image = UIImage(named: "media_phto" )
                cell.PostImage.load(urlString: self.posts[indexPath.row].image)
                cell.imageStrr = self.posts[indexPath.row].image
               // cell.UserImage.image = UIImage(named: self.posts[indexPath.row].posterimage! ?? "user2.png")
           // cell.checkImage(imageStr: self.posts[indexPath.row].image)
                cell.VideoPlayerOutlet.isHidden = true
        }
            if (self.posts[indexPath.row].type == 2){
                cell.PostImage.image = UIImage(named: "media_video" )
                cell.PostImage.load(urlString: self.posts[indexPath.row].thumb)
                cell.imageStrr = self.posts[indexPath.row].video
               // cell.PostImage.image = UIImage(named: self.posts[indexPath.row].image)
               // cell.UserImage.image = UIImage(named: self.posts[indexPath.row].posterimage! ?? "user2.png")
            //cell.checkVideo(imageStr: self.posts[indexPath.row].video, thuumb: self.posts[indexPath.row].thumb)
                cell.VideoPlayerOutlet.isHidden = false
            }
        }
     //   cell.checkUserImage(imageStr: self.posts[indexPath.row].posterimage!)
        if(self.posts[indexPath.row].posterimage == "")
        {
            cell.UserImage.image = UIImage(named:  "user2.png")
        }
        else{
        cell.UserImage.load(urlString: self.posts[indexPath.row].posterimage! )
        }
        //cell.UserImage.image = UIImage(named: self.posts[indexPath.row].posterimage! ?? "user2.png")
        
        cell.isliked = posts[indexPath.row].isliked ?? 0
       
        if(self.user!.profilepic != "")
        {
            cell.UserImage_Comment.load(urlString: self.user!.profilepic)
        }
        else{
            cell.UserImage_Comment.image = UIImage(named: "user2.png")
        }
        //cell.UserImage_Comment.image = UIImage(named: self.user!.profilepic ?? "user2.png")
      //  cell.UserImage_Comment.sd_setImage(with: URL(string: self.user!.profilepic ), placeholderImage: UIImage(named: "user2.png"))
        cell.caption = posts[indexPath.row].caption!
        captionString = posts[indexPath.row].caption!
        cell.checkOptionBtn(PosterId: posts[indexPath.row].posterid, CurrentId: user!.id)
        cell.delegate = self
        cell.typee = "\(posts[indexPath.row].type)"
        cell.setimageZoomer()
        cell.postIdLabel.text = posts[indexPath.row].id
        cell.postid = posts[indexPath.row].id
        
        if posts[indexPath.row].isliked == 1 {
            cell.likebtnoutlet.setImage(UIImage(named: "likeFilled"), for: .normal)
        }else{
            cell.likebtnoutlet.setImage(UIImage(named: "11222"), for: .normal)
        }
        cell.NumberOfComments_Post.text = "View all \(posts[indexPath.row].commentcount) comments"
        cell.PostLikes.text = "\(posts[indexPath.row].likecount)Likes"
        cell.UserName.text = posts[indexPath.row].postername
        cell.postDescriptionLabel.text = posts[indexPath.row].text
        self.posttext = cell.postDescriptionLabel.text ?? ""
        print("post text is \(posttext)")
        cell.PostTime.text = self.getTime(var: posts[indexPath.row].createdAt)
        self.posttype = self.posts[indexPath.row].type
        
        print("post type is \(posttype)")
       return cell
        
    }
    
    @IBAction func PostBtn(_ sender: Any) {
        if((AddPost_TextView.text != "What's on your mind?") && (!AddPost_TextView.text.isEmpty)){
        self.loading.isHidden = false
            
            let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
            let parameter = ["models": "text","api_user": "406594658", "api_secret": "7x3uRdHtFyaNg4HHd5C7", "lang": "en", "mode": "standard", "text": AddPost_TextView.text!] as [String : Any]
            
            AF.upload(multipartFormData: { multipartFormData in
                            for (key, value) in parameter {
                                if let temp = value as? String {
                                    multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                                }
                            }
                           
                        }, to: "https://api.sightengine.com/1.0/text/check.json",method: .post , headers: header)
                            .responseJSON(completionHandler: { (response) in
                                
                                        if let err = response.error{
                                            
                                            return
                                        }
                                print(response)
                                
                                        let json = response.data
                                
                                        if (json != nil)
                                        {
                                            
                                            let jsonObject = JSON(json!)
                                            print(jsonObject)
                                            let jsonObjectprofanity = jsonObject["profanity"]
                                            print(jsonObjectprofanity)
                                            let jsonObjectmatches = jsonObjectprofanity["matches"]
                                            print(jsonObjectmatches)
                                            
                                        
                                            self.matchesarray = jsonObjectmatches.arrayObject! as [Any]
                                            print(self.matchesarray)
                                            if self.matchesarray.count>0 {
                                                self.showPromptAlert("Text Alert", message: "Selected Text is against our privacy policies", okTitle: "ok")
                                            } else {
                                                self.textpostapi()
                                            }
//                                            if let matches = JSON(jsonObjectmatches).arrayObject as? [String] {
//                                                self.matchesarray = matches
//                                                print("ahagaahhhhhh\(self.matchesarray)")
//                                            }
                                            
                                            self.loading.isHidden = true
                                            self.posts.removeAll()
                                            self.nextpage = 0
                                            if self.labelll == "Wall"{
                                                self.AddPost_TextView.text?.removeAll()
                                                self.viewDidLoad()
                                            }else{
                                                self.AddPost_TextView.text?.removeAll()
                                                self.viewDidLoad()
                                            }
                                            
                                          
                                        }
                                    })
        }
        
    }
    
    func textpostapi(){
        let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
            var parameter = ["familyCode": user?.familyCode, "text": AddPost_TextView.text!, "type": "0"]
            if fdate != 0{
         parameter = ["familyCode": user?.familyCode, "text": AddPost_TextView.text!, "type": "0", "fdate": String(self.fdate)]
            }
        AF.upload(multipartFormData: { multipartFormData in
                        for (key, value) in parameter {
                            if let temp = value {
                                           multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                            }
                        }
                       
                    }, to: "http://3.143.193.45:1337/api/createpost",method: .post , headers: header)
                        .responseJSON(completionHandler: { (response) in
                                    if let err = response.error{
                                        
                                        return
                                    }
                            
                                    let json = response.data
                                    if (json != nil)
                                    {
                                        
                                        let jsonObject = JSON(json!)
                                        self.loading.isHidden = true
                                        self.posts.removeAll()
                                        self.nextpage = 0
                                        if self.labelll == "Wall"{
                                            self.AddPost_TextView.text?.removeAll()
                                            self.viewDidLoad()
//                                            self.getposts(var: "http://3.143.193.45:1337/api/getpost", var: 0)
                                        }else{
                                            self.AddPost_TextView.text?.removeAll()
                                            self.viewDidLoad()
//                                            self.getposts(var: "http://3.143.193.45:1337/api/getnewsfeed", var: 0)
                                        }
                                        
                                      
                                    }
                                })
    }
    
    @IBAction func futurePostBtn(_ sender: Any) {
        RPicker.selectDate(title: "Select Date & Time", cancelText: "Cancel", datePickerMode: .dateAndTime, minDate: Date(), maxDate: Date().dateByAddingYears(10000), style: .Inline, didSelectDate: {[weak self] (selectedDate) in
            print(selectedDate)
            let since1970 = selectedDate.timeIntervalSince1970.rounded()
            print("aaaab\(since1970)")
            let inmilisec = since1970 * 1000
            print("aaaab\(inmilisec)")
            self?.fdate = Int(inmilisec)
        })
    }

    
    @IBAction func WallPostPhotoBtn(_ sender: Any) {
        self.uploadtype = "1"
        wishBtnAction(var: "Camera", var: "Gallery")
    }
    
    @IBAction func WallPostVideoBtn(_ sender: Any) {
        self.uploadtype = "2"
        videoBtnAction(var: "Camera", var: "Gallery")
    }
    func videoBtnAction(`var` str1:String, `var` str2:String) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: str1, style: .default, handler: { _ in
            self.RecordAction()
        }))
        
        alert.addAction(UIAlertAction(title: str2, style: .default, handler: { _ in
            self.openImgVideoPicker()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
      }
    
    //video
    func RecordAction() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.mediaTypes = [kUTTypeMovie as String]
            imagePicker.allowsEditing = true
             
            self.present(imagePicker, animated: true, completion: nil)
          } else {
            let alert = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
          }
        }
      //capture
      func imagePickerController(_ picker: UIImagePickerController,
                      didFinishPickingMediaWithInfo info: [String : Any]) {
          dismiss(animated: true, completion: nil)
          guard
            let mediaType = info[UIImagePickerController.InfoKey.mediaType.rawValue] as? String,
            mediaType == (kUTTypeMovie as String),
            let url = info[UIImagePickerController.InfoKey.mediaURL.rawValue] as? URL,
            UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path)
            else {
              return
          }
           
          // Handle a movie capture
          UISaveVideoAtPathToSavedPhotosAlbum(
            url.path,
            self,
            #selector(video(_:didFinishSavingWithError:contextInfo:)),
            nil)
        }
        @objc func video(_ videoPath: String, didFinishSavingWithError error: Error?, contextInfo info: AnyObject) {
          let title = (error == nil) ? "Success" : "Error"
          let message = (error == nil) ? "Video was saved" : "Video failed to save"
          let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
          present(alert, animated: true, completion: nil)
        }
      //videopick
      func openImgVideoPicker() {
          videoAndImageReview.sourceType = .savedPhotosAlbum
          videoAndImageReview.delegate = self
          videoAndImageReview.mediaTypes = ["public.movie"]
          present(videoAndImageReview, animated: true, completion: nil)
        }
        func videoAndImageReview(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            videoURL = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.mediaURL.rawValue)] as? URL
            guard let menuViewController = storyboard?.instantiateViewController(identifier: "PostStoryboardId") as? PostViewController else {
                  return
                }
            menuViewController.videoURL = self.videoURL
            menuViewController.selectedPostDescription = AddPost_TextView.text!
            menuViewController.type = "2"
            menuViewController.labelll = labelll
            menuViewController.fdate = self.fdate
            print("fdate is \(self.fdate)")
            menuViewController.user = self.user
            menuViewController.modalPresentationStyle = .overFullScreen
                present(menuViewController, animated: true)
          
          self.dismiss(animated: true, completion: nil)
        }
    
    func wishBtnAction(`var` str1:String, `var` str2:String) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: str1, style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: str2, style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        
        
        self.present(alert, animated: true, completion: nil)
        
      }
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
                //If you dont want to edit the photo then you can set allowsEditing to false
                imagePicker.allowsEditing = true
                imagePicker.delegate = self
            self.present(imagePicker, animated: true,completion: nil)
            }
            else{
                let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        //MARK: - Choose image from camera roll
        
        func openGallary(){
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            //If you dont want to edit the photo then you can set allowsEditing to false
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true,completion: nil)

        }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if uploadtype == "2"{
            videoURL = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.mediaURL.rawValue)] as? URL
            guard let menuViewController = storyboard?.instantiateViewController(identifier: "PostStoryboardId") as? PostViewController else {
                  return
                }
            menuViewController.videoURL = self.videoURL
                menuViewController.selectedPostDescription = AddPost_TextView.text!
                menuViewController.type = "2"
            menuViewController.labelll = labelll
        
                menuViewController.user = self.user
            menuViewController.modalPresentationStyle = .overFullScreen
               
                present(menuViewController, animated: true)
          
          self.dismiss(animated: true, completion: nil)
        }
        if uploadtype == "1"{
            
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            
//            self.imageUrl = (info[UIImagePickerController.InfoKey.imageURL] as! URL)
            selectedimage = editedImage
            //PostStoryboardId
            guard let menuViewController = storyboard?.instantiateViewController(identifier: "PostStoryboardId") as? PostViewController else {
                  return
                }
            
            
                //menuViewController.urlimage = imageUrl
                menuViewController.selectedimage = selectedimage
                menuViewController.selectedPostDescription = AddPost_TextView.text!
                menuViewController.type = "1"
                menuViewController.labelll = labelll
                menuViewController.user = self.user
                menuViewController.fdate = self.fdate
                print("fdate is \(self.fdate)")
                menuViewController.modalPresentationStyle = .overFullScreen
                present(menuViewController, animated: true)
        }
            
        }
    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.isNavigationBarHidden = false
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}

extension UIView {
 // OUTPUT 1
 func dropShadow2(scale: Bool = true) {
  layer.masksToBounds = false
  layer.shadowColor = UIColor.black.cgColor
  layer.shadowOpacity = 0.5
  layer.shadowOffset = CGSize(width: 0, height: 2.5)
  layer.shadowRadius = 2
 }
}
extension WallPostViewController : DataForCcell{
    func playVideo(videoUrl: String , caption : String) {
        
        playvideo(videoUrl: videoUrl, caption: caption)
        
    
    }
    
    func playvideo(videoUrl : String , caption : String) {
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "VideoPlayerViewController") as? VideoPlayerViewController else {return}
        menuViewController.urlString = String (videoUrl)
        menuViewController.captionTxtVideo = caption
        menuViewController.modalPresentationStyle = .overFullScreen
        self.present(menuViewController, animated: true, completion: nil)
//        let url: URL = URL(string: videoUrl)!
//        playerView = AVPlayer(url: url)
//        playerViewController.player = playerView
//        self.present(playerViewController, animated: true)
//        self.playerViewController.player?.play()
//
//        let videoURL = URL(string: videoUrl)!
//        let player = AVPlayer(url: videoURL)
//        player.rate = 1 //auto play
////
////        CGPoint superCenter = CGPointMake(CGRectGetMidX([superview bounds]), CGRectGetMidY([superview bounds]))
////
//        let playerFrame = CGRect(x: 0, y: view.frame.height/9, width: view.frame.width, height: view.frame.height/1.6)
//        let playerViewController = AVPlayerViewController()
//        playerViewController.player = player
//        playerViewController.view.frame = playerFrame
//
//        addChild(playerViewController)
//        view.addSubview(playerViewController.view)
//        playerViewController.didMove(toParent: self)
//
//        playerViewController.showsPlaybackControls = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fromWallToPost" {
            if let nextViewController = segue.destination as? PostViewController{
               
                nextViewController.selectedimage = selectedimage
                
                    nextViewController.selectedPostDescription = AddPost_TextView.text!
                    
                                nextViewController.type = "1"

            }
        }
        
    }
    func seguetocomments(possttId : String) {
        posttid = possttId
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "commentStoryboardID") as? CommentsViewController else {
              return
            }
        menuViewController.postId = posttid
        menuViewController.user = self.user
        menuViewController.labelll = labelll
        menuViewController.modalPresentationStyle = .overFullScreen
        self.present(menuViewController, animated: true, completion: nil)

    }
    
    func seguetolikers(possttId : String){
        posttid = possttId
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "likeStoryboardID") as? PostLikesViewController else {
              return
            }
        menuViewController.postId = posttid
        menuViewController.labelll = labelll
        menuViewController.modalPresentationStyle = .overFullScreen
        self.present(menuViewController, animated: true, completion: nil)
        
    }
    func buttonTap(possttId : String, imageSt : String, postText: String, postype: String){
        imagesttt = imageSt
        posttid = possttId
        postTexttt = postText
        postType = postype
        postOptionAction(var: "Edit", var: "Delete", pid: possttId)
  }
    func postOptionAction(`var` str1:String, `var` str2:String, pid: String) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Edit", style: UIAlertAction.Style.default) { (UIAlertAction) in
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "updatepostStoryboardID") as? PostUpdateViewController else {
                  return
                }
           
            menuViewController.posttid = self.posttid
            menuViewController.text = self.postTexttt
            menuViewController.selectedimage = self.imagesttt
        
            menuViewController.type = self.postType
            menuViewController.labelll = self.labelll
            menuViewController.modalPresentationStyle = .overFullScreen
            
             
             self.present(menuViewController, animated: true, completion: nil)

        })
        
        alert.addAction(UIAlertAction(title: "Delete", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.deletePost(posstid: pid)
        })
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
       
        
        self.present(alert, animated: true, completion: nil)
        
      }
    func imageviewer(imageStr: String , caption : String){
        zoomimage(IMage: imageStr, caption: caption)
    }
    
    func zoomimage(IMage: String, caption: String){
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "photoShow") as? detailedImg else {
              return
            }
        menuViewController.name = IMage
        menuViewController.captionTxt = caption
        menuViewController.albumname = "ursa1998"
        menuViewController.modalPresentationStyle = .overFullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        captionString = posts[indexPath.row].caption!
        
    }
    func deletePost(posstid: String){
               
                let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
                let comment = ApiCallDeletePost.Request(postid: posstid)
                        let requestService = SCService<ApiCallDeletePost.Request,ApiCallDeletePost.Response>()
                        requestService.request("http://3.143.193.45:1337/api/deletepost", method: .post, parameters: comment, encoder: JSONParameterEncoder(), header: header){
                            (result) in
        
                            switch result {
                            case .success(let response):
                                if response.status == true{
                                    
                                    self.loading.isHidden = true
                                    self.posts.removeAll()
                                    self.nextpage = 0
                                    self.viewDidLoad()
//                                if self.labelll == "Wall"{
//                                    self.viewDidLoad()
//                                    self.getposts(var: "http://3.143.193.45:1337/api/getpost", var: self.nextpage)
//                                }else{
//                                    self.viewDidLoad()
////                                    self.getposts(var: "http://3.143.193.45:1337/api/getnewsfeed", var: self.nextpage)
//
//                                }
                                }else{
                                    self.showPromptAlert("Sorry!", message: response.message);
                                }
                            case .failure(let error):
                               
                                print(error)
        
                            }
        
        
                        }
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
}
extension WallPostViewController : UIViewControllerTransitioningDelegate{
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = true
    return transition
  }
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = false
    return transition
  }
}

extension WallPostViewController : CloseSideMenu{
    func closeSideMenu(){
        dismiss(animated: true, completion: nil)
    }
}

extension UIViewController {
    func showPromptAlert(_ title:String?, message:String?, okTitle:String? = "OK" ) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: okTitle, style: .cancel, handler: nil)
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
//extension UITableViewCell {
//    func showPromptAlert(_ title:String?, message:String?, okTitle:String? = "OK" ) {
//
//        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        let okAction = UIAlertAction(title: okTitle, style: .cancel, handler: nil)
//        alertController.addAction(okAction)
//
//        self.present(alertController, animated: true, completion: nil)
//    }
//}
