//
//  CustomAlertViewController.swift
//  Family Pic Will
//
//  Created by Coder Crew on 6/2/21.
//

import UIKit
import SwiftyJSON
import Alamofire
class CustomAlertViewController: UIViewController {
    var UserId: String = ""
    var familyCode: String = ""
    var wishText: UITextField?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func WishAlertBtn(_ sender: UIButton) {
        
        let wishAlert = UIAlertController(title: "Send Wishes", message: nil, preferredStyle: .alert)
        wishAlert.addTextField { (textField : UITextField!) -> Void in
              textField.placeholder = "Send Wishes..."
            }
        wishAlert.addAction(UIAlertAction(title: "Wish", style: UIAlertAction.Style.default) { (UIAlertAction) in
            let firstTextField = wishAlert.textFields![0] as UITextField
            self.sendHandler(Wish: firstTextField.text ?? "Happy Birthday...", id: self.UserId, fcode: self.familyCode)

        })
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel, handler: nil) /*self.cancelBtn)*/

        wishAlert.addAction(cancelAction)
        self.present(wishAlert, animated: true)
        
       
        
    }
    
    func sendHandler(Wish: String, id: String, fcode: String){
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let wish = BirthdayWishApi.Request(text: Wish, familyCode: fcode, userid: id)
                let requestService = SCService<BirthdayWishApi.Request,BirthdayWishApi.Response>()
                requestService.request("http://3.143.193.45:1337/api/birthday", method: .post, parameters: wish, encoder: JSONParameterEncoder(), header: header){
                    (result) in

                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == false{
                        self.showPromptAlert("Sorry!", message: response.message);
                    }
                       // self.Relation.text = (self.wishText?.text)!

                        //self.performSegue(withIdentifier: "MainNav", sender: self)
                    case .failure(let error):
                        debugPrint(error)

                        print(error)

                    }
                }
    }
    
    func cancelBtn( alert: UIAlertAction) {
        
    }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


