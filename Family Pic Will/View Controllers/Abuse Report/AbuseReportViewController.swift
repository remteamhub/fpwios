//
//  AbuseReportViewController.swift
//  Family Pic Will
//
//  Created by Apple on 20/05/2021.
//

import Foundation
import UIKit
import Alamofire
import SDWebImage

class AbuseReportViewController: UIViewController {
    var membersDisplay : [User] = []
    var memberProfile : User?
    var user: Int?
    var member : [User] = []
    @IBOutlet weak var titlebarLabel: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var reportnumberLabel: UILabel!
    
    let transition = SlideInTransition()
    
    @IBAction func openSideMenuClicked(_ sender: Any) {
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "SideMenuStoryboardId") else {
              return
            }
            menuViewController.modalPresentationStyle = .overCurrentContext
            menuViewController.transitioningDelegate = self
            present(menuViewController, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        transition.delegate = self
        callAPi()

    }
    
    func callAPi(){
        
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallProfile.Request,ApiCallProfile.Response>()
        requestService.request("http://3.143.193.45:1337/api/profile", method: .post, parameters: nil, encoder: JSONParameterEncoder(), header: header){(result) in
            switch result{
            case .success(let response):
                
                if response.status == true{
                debugPrint(response)
                    self.user = response.data!.user.reportCount
                    self.reportnumberLabel.text = "\(String(describing: response.data!.user.reportCount ?? 0))"
    }
            case .failure(let error):
                debugPrint(error)
            }

}
    }

        }

extension AbuseReportViewController : UIViewControllerTransitioningDelegate{
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = true
    return transition
  }
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = false
    return transition
  }
}

extension AbuseReportViewController : CloseSideMenu {
    func closeSideMenu(){
        dismiss(animated: true, completion: nil)
    }
}

    

