//
//  SettingViewController.swift
//  FPW
//
//  Created by Apple on 03/05/2021.
//

import UIKit

class SettingViewController: UIViewController {
    //@IBOutlet weak var settingfamilies: UIStackView!
    @IBOutlet weak var titleBarLabel: UILabel!
    @IBOutlet weak var settingfamily: UIStackView!
    @IBOutlet weak var settingFamiliesLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var blockLebel: UILabel!
    @IBOutlet weak var contactUsLabel: UILabel!
    @IBOutlet weak var passwordScreen: UIStackView!
    @IBOutlet weak var contactUsView: UIStackView!
    var user:User?
    var member:User?
    let transition = SlideInTransition()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 38/255, green: 56/255, blue: 75/255,alpha: 1)
        transition.delegate = self
        titleBarLabel.font = UIFont(name: "Roboto-Regular", size: 20)
        settingFamiliesLabel.font = UIFont(name: "Roboto-Regular", size: 20)
        passwordLabel.font = UIFont(name: "Roboto-Regular", size: 20)
        blockLebel.font = UIFont(name: "Roboto-Regular", size: 20)
        contactUsLabel.font = UIFont(name: "Roboto-Regular", size: 20)

        if let data = UserDefaults.standard.data(forKey: "USER"){
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(User.self, from: data)
                print(user.self)
                self.user = user
            }catch{
                print("Error")
            }
        }

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.familiesTap))
        tapGesture.numberOfTapsRequired = 1
        settingfamily.addGestureRecognizer(tapGesture)
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.passwordTap))
        tapGesture2.numberOfTapsRequired = 1
        passwordScreen.addGestureRecognizer(tapGesture2)
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(blockTap))
        blockLebel.isUserInteractionEnabled = true
        blockLebel.addGestureRecognizer(gestureRecognizer)
        
        let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(self.contactTap))
        tapGesture4.numberOfTapsRequired = 1
        contactUsView.addGestureRecognizer(tapGesture4)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    @objc func familiesTap(){
        print("Clicked")
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "FamiliesStoryboardId") as? FamiliesViewController else {
            return
        }
        menuViewController.modalPresentationStyle = .fullScreen
        self.present(menuViewController, animated: true) {
            //self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func passwordTap(){
        //print("Clicked")
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "ChangePasswordStoryboardId") as? ChangePasswordViewController else {
            return
        }
        menuViewController.modalPresentationStyle = .fullScreen
        menuViewController.user = self.user
        self.present(menuViewController, animated: true) {
            //self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func blockTap(){
        print("Clicked")
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "BlockedMemberStoryboardId") as? ViewController else {
            return
        }
        menuViewController.modalPresentationStyle = .fullScreen
        menuViewController.member = self.member

        menuViewController.user = self.user
        self.present(menuViewController, animated: true) {
            //self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func contactTap(){
        //print("Clicked")
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "ContactUsStoryboardId") else {
            return
        }
        menuViewController.modalPresentationStyle = .fullScreen
        self.present(menuViewController, animated: true) {
            //self.dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func openSideMenuClicked(_ sender: Any) {
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "SideMenuStoryboardId") else {
              return
            }
            menuViewController.modalPresentationStyle = .overCurrentContext
            menuViewController.transitioningDelegate = self
            present(menuViewController, animated: true)
    }
    //    @IBAction func backClicked(_ sender: Any) {
//        guard let menuViewController = storyboard?.instantiateViewController(identifier: "SideMenuStoryboardId") else {
//              return
//            }
//            menuViewController.modalPresentationStyle = .overCurrentContext
//            menuViewController.transitioningDelegate = self
//            present(menuViewController, animated: true)
//    }
    

}
extension SettingViewController : UIViewControllerTransitioningDelegate{
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = true
    return transition
  }
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = false
    return transition
  }
}
extension SettingViewController : CloseSideMenu{
    func closeSideMenu(){
        dismiss(animated: true, completion: nil)
    }
}
