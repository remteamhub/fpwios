//
//  SubcriptionPlanViewController.swift
//  Family Pic Will
//
//  Created by Apple on 19/05/2021.
//

import UIKit
import PassKit
import StoreKit
import Alamofire
class SubscriptionPlanViewController: UIViewController,SKProductsRequestDelegate,SKPaymentTransactionObserver {
   
    
    
    
    @IBOutlet weak var futureMessageTextLabel1: UILabel!
    
    let transition = SlideInTransition()
    @IBOutlet weak var additionalFutureMessage1: UILabel!
    @IBOutlet weak var additionalFutureMessage2: UILabel!
    @IBOutlet weak var additionalFutureMessage3: UILabel!
    @IBOutlet weak var everyMonth1: UILabel!
    @IBOutlet weak var everyMonth2: UILabel!
    @IBOutlet weak var smallPlanbutton: UIButton!
    @IBOutlet weak var mediumPlanbutton: UIButton!
    @IBOutlet weak var largePlanbutton: UIButton!
    @IBOutlet weak var oneFuturebutton: UIButton!
    var index = -1
    var myProduct = [SKProduct]()
    
    
    @IBAction func openSideMenuClicked(_ sender: Any) {
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "SideMenuStoryboardId") else {
              return
            }
            menuViewController.modalPresentationStyle = .overCurrentContext
         //   menuViewController.transitioningDelegate = self
            present(menuViewController, animated: true)
    }
     
    
//    private var Paymentrequest : PKPaymentRequest = {
//        let request = PKPaymentRequest()
//        request.merchantIdentifier = "merchant.mustafashaheen.fpw"
//        request.supportedNetworks = [.masterCard, .quicPay, .visa]
//        request.supportedCountries = ["IN", "US"]
//        request.merchantCapabilities = .capability3DS
//        request.countryCode = "US"
//        request.currencyCode = "USD"
//        request.paymentSummaryItems = [PKPaymentSummaryItem(label: "Small Plan", amount: 13.99)]
//        //request.paymentSummaryItems = [PKPaymentSummaryItem(label: "Medium Plan", amount: 26.99)]
//        return request
//
//    }()
//
    override func viewDidLoad() {
        super.viewDidLoad()
        
     //   subscriptionApi()
        transition.delegate = self
//        // Do any additional setup after loading the view.
        futureMessageTextLabel1.font = UIFont(name: "Montserrat-Italic", size: 21)

        everyMonth1.font = UIFont(name: " Montserrat-Medium", size: 12)
        everyMonth2.font = UIFont(name: " Montserrat-Medium", size: 12)

        additionalFutureMessage1.font = UIFont(name: "Roboto-Thin", size: 15)
        additionalFutureMessage2.font = UIFont(name: "Roboto-Thin", size: 15)
        additionalFutureMessage3.font = UIFont(name: "Roboto-Thin", size: 15)
    }
    override func viewWillAppear(_ animated: Bool) {
        fetchProducts()
        smallPlanbutton.tag = 0
        mediumPlanbutton.tag = 1
        largePlanbutton.tag = 2
        oneFuturebutton.tag = 3
    }
    func fetchProducts() {

        let request = SKProductsRequest (productIdentifiers: ["com.fpw2.smallplan", "com.fpw2.mediumplan", "com.fpw2.largeplan", "com.fpw2.onefuturemessage"])
        request.delegate = self
        request.start()
}



    

    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        myProduct = response.products

   }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch  transaction.transactionState {
            case .purchasing :
                break
            case .purchased, .restored:
                subscriptionApi()
                SKPaymentQueue.default().finishTransaction(transaction)
                SKPaymentQueue.default().remove(self)
                break

            case .failed, .deferred:
                SKPaymentQueue.default().finishTransaction(transaction)
                SKPaymentQueue.default().remove(self)
                break

             default:
                SKPaymentQueue.default().finishTransaction(transaction)
                SKPaymentQueue.default().remove(self)
                break
            }
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    
//    @objc func tapForPay(_ sender: UITapGestureRecognizer? = nil) {
//        // handling code
//        fetchProducts()
//    }
//
//}
//

//extension SubscriptionPlanViewController: PKPaymentAuthorizationViewControllerDelegate{
//    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
//        controller.dismiss(animated: true, completion: nil)
//    }
//
//    func paymentAuthorizationViewController(controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, completion: ((PKPaymentAuthorizationStatus) -> Void)) {
//           completion(PKPaymentAuthorizationStatus.success)
//       }
//
//    func paymentAuthorizationController(_ controller: PKPaymentAuthorizationController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
//        completion(PKPaymentAuthorizationResult(status: .success, errors: nil))
//      }
//       // showPromptAlert("Success", message: "The Apple Pay Transaction was complete.")
    func subscriptionApi(){
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<SubscriptionApi.Request,SubscriptionApi.Response>()
        let parameters = SubscriptionApi.Request(plan: 1, payment: "asdfg", amount: "12.99")
        requestService.request("http://3.143.193.45:1337/api/purchaseplan", method: .post, parameters: parameters, encoder: JSONParameterEncoder(), header: header){(result) in
            switch result{
            case .success(let response):
                if response.status == true{
//                    self.showPromptAlert("Done", message: response.message)
                    let alert = UIAlertController(title: "Done" , message: "Thankyou for the purchase", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { action in
                        self.gottoProfile()
                    }))
                    self.present(alert, animated: true)
                }
                debugPrint(response)
                case .failure(let error):
                    debugPrint(error)
                }
            }
        
    }
    func gottoProfile(){
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "ProfileStoryboardId")  as? ProfileViewController else {
            return
            
        }
        
        menuViewController.modalPresentationStyle = .fullScreen
        self.present(menuViewController, animated: true,completion: nil)
        
    }
    
    @IBAction func oneFutureMessageTapped(_ sender: UIButton) {
        switch(sender.tag){
        case 0:
            index = 3
            break
        case 1:
            index = 1
            break
        case 2:
            index = 0
            break
        default:
            index = 2
            break
        }
        if(myProduct.count > 0){
        if SKPaymentQueue.canMakePayments(){
            let payment = SKPayment(product: myProduct[index])
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
        }
}
    }
    
}


extension SubscriptionPlanViewController : UIViewControllerTransitioningDelegate{
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = true
    return transition
  }
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = false
    return transition
  }
}
extension SubscriptionPlanViewController : CloseSideMenu{
    func closeSideMenu(){
        dismiss(animated: true, completion: nil)
        //transition.isPresenting = true
    }
}
