//
//  DeceasedViewController.swift
//  Family Pic Will
//
//  Created by Apple on 19/05/2021.
//

import UIKit
import Foundation
import Alamofire
import SDWebImage

class DeceasedViewController: UIViewController {
    
    var user : User?
    var display: [User?] = []

    @IBAction func backClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var deceasedTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        deceasedTableView.delegate = self
        deceasedTableView.dataSource = self
        deceasedTableView.separatorStyle = .none
        deceasedTableView.showsVerticalScrollIndicator = false
        Apicall()
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    
    
    func Apicall() {
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallGetBlockedMembers.Request, ApiCallGetBlockedMembers.Response>()
        requestService.request("http://3.143.193.45:1337/api/getdeceased", method: .post, parameters: nil, encoder: JSONParameterEncoder(), header: header){(result) in
          switch result{
          case .success(let response):
            if response.status == true{
            debugPrint(response)
              self.display = response.data
              print("Response Data")
              print(response.data)
              print("Display")
              print(self.display)
              self.deceasedTableView.reloadData()
      }
          case .failure(let error):
            debugPrint(error)
          }
        }
      }
}
extension DeceasedViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        display.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell",for: indexPath) as! DeceasedTableViewCell
        cell.deceasedName.text = display[indexPath.row]?.name
        cell.deceasedImage.layer.borderWidth = 1.0
        cell.deceasedImage.layer.masksToBounds = false
            cell.deceasedImage.layer.borderColor = UIColor.white.cgColor
            cell.deceasedImage.layer.cornerRadius = cell.deceasedImage.frame.size.width / 2
            cell.deceasedImage.clipsToBounds = true
        cell.deceasedImage.sd_setImage(with: URL(string: display[indexPath.row]?.profilepic ?? "userprofile.png" ),placeholderImage: UIImage(named: "userprofile.png"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
