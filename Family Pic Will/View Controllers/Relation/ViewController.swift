//
//  ViewController.swift
//  Family Pic Will
//
//  Created by Apple on 09/05/2021.
//

import Foundation
import UIKit
import Alamofire
import SDWebImage

class ViewController: UIViewController {
 
    
    var user: User?
    var member: User?
    var display: [User?] = []
    @IBOutlet weak var noblockedmember: UILabel!
    @IBOutlet weak var blockMemberTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        blockMemberTable.delegate = self
        blockMemberTable.dataSource = self
        blockMemberTable.separatorStyle = .none
        blockMemberTable.showsVerticalScrollIndicator = false
        Apicall()
         noblockedmember.isHidden = true
            
        
    }
    
    @IBAction func backBtn(_ sender: Any) {
        self.goBackToMessage()
    }
    
    func goBackToMessage(){
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "SettingStoryboardId") as? SettingViewController else {
              return
            }
        
        menuViewController.user = self.user
        menuViewController.modalPresentationStyle = .fullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    
    func Apicall()  {
        
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallGetBlockedMembers.Request,ApiCallGetBlockedMembers.Response>()
        requestService.request("http://3.143.193.45:1337/api/getblocked", method: .post, parameters: nil, encoder: JSONParameterEncoder(), header: header){(result) in
            switch result{
            case .success(let response):
                if response.status == true{
                debugPrint(response)
                    self.display = response.data
                    print("Response Data")
                    print(response.data)
                    print("Display")
                    print(self.display)
      
                    self.blockMemberTable.reloadData()

        
    }
    
            case .failure(let error):
                debugPrint(error)
            }
        }
    }
    
    func Apicallunblock(mid: String) {
        
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCallUnblockMember.Request(id: mid)
        let requestService = SCService<ApiCallUnblockMember.Request,ApiCallUnblockMember.Response>()
        requestService.request("http://3.143.193.45:1337/api/unblock", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){(result) in
            switch result{
            case .success(let response):
                if response.status == true{
                debugPrint(response)
//                    self.display = response.data
//                    print("Response Data")
//                    print(response.data)
//                    print("Display")
//                    print(self.display)
//
//                    self.blockMemberTable.reloadData()
//
                    self.Apicall()
        
    }
    
            case .failure(let error):
                debugPrint(error)
                //self.Apicall()
            }
        }

        
    }
    
}
extension ViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if display.count == 0 {
            noblockedmember.isHidden = false
            noblockedmember.text = "You currently have no Blocked members"
        } else {
            noblockedmember.isHidden = true
        }
        return display.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BlockedMemberTableViewCell
        cell.memberNameLabel.text = display[indexPath.row]?.name
//        cell.memberImage.layer.cornerRadius = cell.memberImage.frame.size.width / 6
//        cell.memberImage.clipsToBounds = true
        
        cell.memberImage.layer.borderWidth = 1.0
        cell.memberImage.layer.masksToBounds = false
        cell.memberImage.layer.borderColor = UIColor.white.cgColor
        cell.memberImage.layer.cornerRadius = cell.memberImage.frame.size.width / 2
        cell.memberImage.clipsToBounds = true
        cell.memberImage.sd_setImage(with: URL(string: display[indexPath.row]?.profilepic ?? "userprofile.png" ),placeholderImage: UIImage(named: "userprofile.png"))
        noblockedmember.isHidden = true
     

            return cell
    }
    
    func unblockMember(id: String){
        
        let wishAlert = UIAlertController(title: "Unblock User?", message: nil, preferredStyle: .alert)
//        wishAlert.addAction(UIAlertAction(title: "Unblock", style: UIAlertAction.Style.default, handler: nil))
            wishAlert.addAction(UIAlertAction(title: "Unblock", style: UIAlertAction.Style.default) { (UIAlertAction) in
                self.Apicallunblock(mid: id)
            })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        wishAlert.addAction(cancelAction)
        self.present(wishAlert, animated: true)
        
    
}

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Apicallunblock()
        //self.display[indexPath.row]?.id
        unblockMember(id: self.display[indexPath.row]?.id ?? "")
        tableView.deselectRow(at: indexPath, animated: true)

}
}

