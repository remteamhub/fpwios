//
//  EmailVeriVC.swift
//  Family Pic Will
//
//  Created by mac on 4/27/21.
//
import Alamofire
import UIKit

class EmailVeriVC: UIViewController {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var codeFeild: UITextField!
    @IBOutlet weak var verifyButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.indicator.isHidden = true
        verifyButton.layer.cornerRadius = 8.0
        verifyButton.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 21)
        codeFeild.attributedPlaceholder = NSAttributedString(string: "1234",attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        
        
        codeFeild.font = UIFont(name: "Helvetica", size: 30)
        codeFeild.keyboardType = .numberPad
        
        mailLabel.text = UserDefaults.standard.string(forKey: "Mail")
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    func makeAlert(AlertTilte: String, AlertMessage: String){
        let alert = UIAlertController(title: AlertTilte, message: AlertMessage, preferredStyle: UIAlertController.Style.alert)
        let Okbutton = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(Okbutton)
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func verifyClicked(_ sender: Any) {
        if codeFeild.text == "" {
            makeAlert(AlertTilte: "Code Missing", AlertMessage: "Enter the code sent")
        }
        else if codeFeild.text!.count >= 5 || codeFeild.text!.count <= 3 {
            makeAlert(AlertTilte: "Code Error", AlertMessage: "Code must be 4 digit")
        }
        else {
            self.indicator.isHidden = false
        let veri = VerifyModel.Request(code: codeFeild.text!)
        let Verifyfunc = SCService<VerifyModel.Request,VerifyModel.Response>()
      
        Verifyfunc.request("http://3.143.193.45:1337/api/verify", parameters: veri, header: ["Authorization" : "Bearer \(UserDefaults.standard.string(forKey: "token1")!)"]){(result) in
            switch result {
            case .success(let response):
                debugPrint(response)
                
                self.indicator.stopAnimating()
                if response.status == true{
                UserDefaults.standard.setValue(UserDefaults.standard.string(forKey: "token1"), forKey: "token")
                UserDefaults.standard.setValue(2, forKey: "loginStep")
                self.performSegue(withIdentifier: "EditProfileNav", sender: self)
                }else{
                    self.showPromptAlert("Sorry!", message: response.message);
                }
            case .failure(let error):
                debugPrint(error)
                self.makeAlert(AlertTilte: "Verification Error", AlertMessage: "Invalid code")
                self.indicator.stopAnimating()
            }
        }
        
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EditProfileNav"{
            let secondViewControler = segue.destination as! EditProfileViewController
            secondViewControler.fromVerificationScreen = true
        }
    }
    @IBAction func resend(_ sender: Any) {
        self.indicator.isHidden = false
        let resendfunc = SCService<ResendModel.Request,ResendModel.Response>()
        resendfunc.request("http://3.143.193.45:1337/api/resendcode", header: ["Authorization" : "Bearer \(UserDefaults.standard.string(forKey: "token1")!)"]){(result) in
            switch result {
            case .success(let response):
                debugPrint(response)
                self.indicator.stopAnimating()
                if response.status == true{
                self.makeAlert(AlertTilte: "Resend successful", AlertMessage: "")
                }else{
                    self.showPromptAlert("Sorry!", message: response.message);
                }
            case .failure(let error):
                debugPrint(error)
                self.indicator.stopAnimating()
                self.makeAlert(AlertTilte: "Resend Error", AlertMessage: "")
    }
}
    }
}
