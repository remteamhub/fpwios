//
//  LoginViewController.swift
//  Family Pic Will
//
//  Created by mac on 4/27/21.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController {
    var iconClick = true
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var loginFeild: UITextField!
    @IBOutlet weak var passwordFeild: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var forgetpassButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        indicator.isHidden = true
        loginLabel.font = UIFont(name: "Montserrat-Bold", size: 25)
        loginFeild.attributedPlaceholder = NSAttributedString(string: "Email Address",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        loginFeild.font = UIFont(name: "Montserrat-SemiBold", size: 13)
        passwordFeild.attributedPlaceholder = NSAttributedString(string: "Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        passwordFeild.font = UIFont(name: "Montserrat-SemiBold", size: 13)
        passwordFeild.isSecureTextEntry = true
        loginButton.layer.cornerRadius = 8.0
        loginButton.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 21)
        forgetpassButton.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 13)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    
    @IBAction func forgotpasswordClicked(_ sender: Any) {
        performSegue(withIdentifier: "ForgetPassNavigation", sender: self)
    }
    
    
    @IBAction func loginClicked(_ sender: Any) {
        if loginFeild.text == ""{
            self.makeAlert(AlertTilte: "Error", AlertMessage: "Email address missing")
        }
        else if loginFeild.text!.contains(" "){
            makeAlert(AlertTilte: "Email Error", AlertMessage: "Spaces are not allowed")
        }
        else if passwordFeild.text == ""{
            self.makeAlert(AlertTilte: "Error", AlertMessage: "Password missing")
        }
        else if passwordFeild.text!.contains(" "){
            makeAlert(AlertTilte: "Password Error", AlertMessage: "Spaces are not allowed")
        }
        else{
            self.indicator.isHidden = false
            let log = LoginModel.Request(email: loginFeild.text!, password: passwordFeild.text!, fcmtoken: UserDefaults.standard.string(forKey: "fcmtoken")!)//(email: loginFeild.text!, password: passwordFeild.text!, )
            let Loginfunc = SCService<LoginModel.Request,LoginModel.Response>()
            Loginfunc.request("http://3.143.193.45:1337/api/login", method: .post,parameters: log) { (result) in
                print(result)
                switch result {
                case .success(let response):
                    self.indicator.stopAnimating()
                    debugPrint(response)
                    let user1 : user = user(status: response.status, message: response.message, token: response.data.token, createdAt: response.data.user.createdAt, updatedAt: response.data.user.updatedAt, id: response.data.user.id, name: response.data.user.name, email: response.data.user.email, city: response.data.user.city, state: response.data.user.state, zip: response.data.user.zip, country: response.data.user.country, dob: response.data.user.dob, gender: response.data.user.gender, family: response.data.user.family, familyCode: response.data.user.familyCode, profilepic: response.data.user.profilepic, fcmToken: response.data.user.fcmToken, san: response.data.user.san, verify: response.data.user.verify, reportCount: response.data.user.reportCount)
                    UserDefaults.standard.setValue(response.data.user.id, forKey: "uid")
                    UserDefaults.standard.setValue(response.data.user.familyCode, forKey: "currentFamilyCode")
                    UserDefaults.standard.setValue(response.data.user.email, forKey: "mail")
                    UserDefaults.standard.setValue(response.data.token, forKey: "token")
                    UserDefaults.standard.setValue(response.data.user.city, forKey: "city")
                    UserDefaults.standard.setValue(response.data.user.state, forKey: "state")
                    UserDefaults.standard.setValue(response.data.user.family, forKey: "familyname")
                    UserDefaults.standard.setValue(response.data.user.profilepic, forKey: "profilepic")
                    UserDefaults.standard.setValue(response.data.user.reportCount, forKey: "reportcount")
                    print(user1)
                    do {
                    let encoder = JSONEncoder()
                    let data = try encoder.encode(user1)
                    UserDefaults.standard.setValue(data, forKey: "USER")
                        print(UserDefaults.standard.object(forKey: "USER")!)
                    } catch{
                        print("Error")
                    }
//                    self.send_token()
                    self.jump()
                    
//                    guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "ProfileStoryboardId")  as? ProfileViewController else {
//                        return
//                    }
//                    let presentingVC = self.presentingViewController
//                    menuViewController.modalPresentationStyle = .fullScreen
//                    self.dismiss(animated: true) {
//                        presentingVC!.present(menuViewController, animated: true,completion: nil)
//                    }
                case .failure( _):
                    
                    self.showPromptAlert("Sorry!", message: "Invalid Email/Password")
                    self.indicator.stopAnimating()
//                    let Loginfunc2 = SCService<LoginModel.Request,LoginModel.Response2>()
//                    Loginfunc2.request("http://3.143.193.45:1337/api/login", method: .post,parameters: log) { (result) in
//                        switch result {
//                        case .success(let response):
//                            self.indicator.stopAnimating()
//                            if response.status == true{
//                            debugPrint(response)
//                            self.makeAlert(AlertTilte: "Login faild", AlertMessage: "\(response.message)")
//                            }else{
//                                self.showPromptAlert("Sorry!", message: response.message);
//                            }
//                        case .failure(let error):
//                            self.indicator.stopAnimating()
//                            debugPrint(error)
//                            self.makeAlert(AlertTilte: "Login Error", AlertMessage: "No internet connection")
//                        }
//                    }
                }
            }
        }
    }
    func jump()
    {
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "ProfileStoryboardId")  as? ProfileViewController else {
            return
        }
        let presentingVC = self.presentingViewController
        menuViewController.modalPresentationStyle = .fullScreen
        self.dismiss(animated: true) {
            presentingVC!.present(menuViewController, animated: true,completion: nil)
        }
    }
    func makeAlert(AlertTilte: String, AlertMessage: String){
        let alert = UIAlertController(title: AlertTilte, message: AlertMessage, preferredStyle: UIAlertController.Style.alert)
        let Okbutton = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(Okbutton)
        self.present(alert, animated: true, completion: nil)
    }
//    func send_token() {
//
//        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
//
//        print(header)
//
//        let fcmtoken = UserDefaults.standard.string(forKey: "fcmtoken")!
//
//        print("asdfgh\(fcmtoken)")
//
//        let parameter = LoginPushApi.Request(token: "\(fcmtoken)")
//
//
//        let requestService = SCService<LoginPushApi.Request,LoginPushApi.Response>()
//
//        requestService.request("http://3.143.193.45:1337/api/fcmToken", method: .post, parameters: parameter, encoder: JSONParameterEncoder(), header: header){
//                    (result) in
//
//                    switch result {
//                    case .success(let response):
//                        debugPrint(response)
//                        print(response.status)
//                        print(response.message)
//                        self.jump()
//                        if response.status == false {
//                            self.showPromptAlert("Sorry!", message: response.message);
//                        }
//                    case .failure(let error):
//                        debugPrint(error)
//                        print(error)
//                       }}
//}
}
