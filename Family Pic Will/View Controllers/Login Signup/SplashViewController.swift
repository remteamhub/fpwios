//
//  ViewController.swift
//  Family Pic Will
//
//  Created by mac on 4/23/21.
//

import UIKit

class SplashViewController: UIViewController {
   
    @IBOutlet weak var LoginButton: UIButton!
    @IBOutlet weak var SignupButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LoginButton.layer.cornerRadius = 22.0
        LoginButton.titleLabel?.font = UIFont(name: "Montserrat-Regular", size: 13)
        LoginButton.titleLabel!.textColor = UIColor.white
        SignupButton.layer.cornerRadius = 22.0
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    override func viewDidAppear(_ animated: Bool) {
        print(UserDefaults.standard.string(forKey: "token"))
        print("abc")
        if UserDefaults.standard.string(forKey: "token") != nil  {
            guard let menuViewController = storyboard?.instantiateViewController(identifier: "ProfileStoryboardId") as? ProfileViewController else {
                  return
                }
            
           //menuViewController.labelll = "NewsFeed"
                menuViewController.modalPresentationStyle = .overFullScreen
            
             
             self.present(menuViewController, animated: true, completion: nil)
           // performSegue(withIdentifier: "checkCredential", sender: self)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    @IBAction func loginClicked(_ sender: Any) {
        performSegue(withIdentifier: "LoginNavigation", sender: self)
    }
    
    @IBAction func signupClicked(_ sender: Any) {
        performSegue(withIdentifier: "SignupNaviagtion", sender: self)
    }
    
}

