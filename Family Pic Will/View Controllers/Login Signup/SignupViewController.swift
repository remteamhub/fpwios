//
//  SignupViewController.swift
//  Family Pic Will
//
//  Created by mac on 4/27/21.
//

import UIKit

class SignupViewController: UIViewController {
    
    /// Variables used
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var signupLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var maleLabel: UILabel!
    @IBOutlet weak var femaleLabel: UILabel!
    @IBOutlet weak var checkLabel: UILabel!
    @IBOutlet weak var checkMale: UIButton!
    @IBOutlet weak var checkFemale: UIButton!
    @IBOutlet weak var firstnameFeild: UITextField!
    @IBOutlet weak var lastnameFeild: UITextField!
    @IBOutlet weak var emailaddressFeild: UITextField!
    @IBOutlet weak var passwordfeild: UITextField!
    @IBOutlet weak var confirmpasswordFeild: UITextField!
    @IBOutlet weak var dobFeild: UITextField!
    @IBOutlet weak var familycodeFeild: UITextField!
    @IBOutlet weak var createaccountButton: UIButton!
    @IBOutlet weak var familyLabelFeild: UITextField!
    @IBOutlet weak var termLabel1: UILabel!
    @IBOutlet weak var termLabel2: UILabel!
    @IBOutlet weak var TermButton: UIButton!
    @IBOutlet weak var termButton2: UIButton!
    @IBOutlet weak var termButton3: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    var name = ""
    var date : String?
    var flag = 2
    var check = 0
    let datePicker = UIDatePicker()
    
    /// viewDidLoad to render screen items
    override func viewDidLoad() {
        super.viewDidLoad()
        indicator.isHidden = true
        signupLabel.font = UIFont(name: "Montserrat-Bold", size: 25)
        genderLabel.font = UIFont(name: "Montserrat-SemiBold", size: 22)
        maleLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
        femaleLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
        checkLabel.font = UIFont(name: "Montserrat-SemiBold", size: 14)
        
        firstnameFeild.attributedPlaceholder = NSAttributedString(string: "First Name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        firstnameFeild.font = UIFont(name: "Montserrat-SemiBold", size: 13)
        
        lastnameFeild.attributedPlaceholder = NSAttributedString(string: "Last Name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        lastnameFeild.font = UIFont(name: "Montserrat-SemiBold", size: 13)
        
        emailaddressFeild.attributedPlaceholder = NSAttributedString(string: "Email Address",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        emailaddressFeild.font = UIFont(name: "Montserrat-SemiBold", size: 13)
        
        passwordfeild.attributedPlaceholder = NSAttributedString(string: "Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        passwordfeild.font = UIFont(name: "Montserrat-SemiBold", size: 13)
        passwordfeild.isSecureTextEntry = true
        
        confirmpasswordFeild.attributedPlaceholder = NSAttributedString(string: "Confirm password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        confirmpasswordFeild.font = UIFont(name: "Montserrat-SemiBold", size: 13)
        confirmpasswordFeild.isSecureTextEntry = true
        
        dobFeild.attributedPlaceholder = NSAttributedString(string: "Date of Birth",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        dobFeild.font = UIFont(name: "Montserrat-SemiBold", size: 13)
        
        familycodeFeild.attributedPlaceholder = NSAttributedString(string: "Family code",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        familycodeFeild.font = UIFont(name: "Montserrat-SemiBold", size: 13)
        familycodeFeild.isHidden = true
        
        familyLabelFeild.attributedPlaceholder = NSAttributedString(string: "Family Label",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        familyLabelFeild.font = UIFont(name: "Montserrat-SemiBold", size: 13)
        
        createaccountButton.layer.cornerRadius = 8.0
        createaccountButton.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 21)
        
        termLabel1.font = UIFont(name: "Montserrat-Bold", size: 9)
        termLabel2.font = UIFont(name: "Montserrat-Bold", size: 9)
        TermButton.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 13)
        termButton2.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 13)
        termButton3.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 13)
        /// for date input label.
        createDatePicker()
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    /// info button to show family code info
    @IBAction func infoClicked(_ sender: Any) {
        self.makeAlert(AlertTilte: "Info", AlertMessage: "A family code is a code you will recieve automatically after registering or when joining an existing group. you will need to provide a code to join that particular group. A family label is a title or nickname you will create for any particular family group you belong to.")
    }
    /// checks to switch from male to female and vice versa
    @IBAction func maleCheck(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            checkFemale.isSelected = true
            flag = 0
        }
        else {
            sender.isSelected = true
            checkFemale.isSelected = false
            flag = 1
        }
    }
    
    @IBAction func femaleCheck(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            checkMale.isSelected = true
            flag = 1
            
        }
        else {
            sender.isSelected = true
            checkMale.isSelected = false
            flag = 0
        }
    }
    /// check for family code and family label
    @IBAction func checkClicked(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            familyLabelFeild.isHidden = false
            
            familycodeFeild.isHidden = true
            familycodeFeild.text = ""
            check = 0
            
        }
        else {
            sender.isSelected = true
            familyLabelFeild.isHidden = true
            familyLabelFeild.text = ""
            familycodeFeild.isHidden = false
            
            check = 1
        }
    }
    /// generating alert generic function
    func makeAlert(AlertTilte: String, AlertMessage: String){
        let alert = UIAlertController(title: AlertTilte, message: AlertMessage, preferredStyle: UIAlertController.Style.alert)
        let Okbutton = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(Okbutton)
        self.present(alert, animated: true, completion: nil)
    }
    /// Date label toolbar
    func createToolBar() -> UIToolbar {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donepressed))
        toolbar.setItems([doneBtn], animated: true)
        return toolbar
    }
    /// Date input keyboard
    func createDatePicker(){
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = .date
        dobFeild.inputView = datePicker
        dobFeild.inputAccessoryView = createToolBar()
    }
    /// Date toolbar action button functionality
    @objc func donepressed(){
        let currentdate = Date()
        let dateFormattor1 = DateFormatter()
        dateFormattor1.dateStyle = .medium
        dateFormattor1.timeStyle = .none
        let dateFormattor = DateFormatter()
        dateFormattor.dateStyle = .medium
        dateFormattor.timeStyle = .none
        if datePicker.date > currentdate{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            let year: String = dateFormatter.string(from: currentdate)
            
            dateFormatter.dateFormat = "MM"
            let month: String = dateFormatter.string(from: currentdate)
            dateFormatter.dateFormat = "MMMM"
            let monthName: String = dateFormatter.string(from: currentdate)
            
            
            dateFormatter.dateFormat = "dd"
            let day: String = dateFormatter.string(from: currentdate)
            
            date = "\(day)-\(month)-\(year)"
            dobFeild.text = "\(day) \(monthName) \(year)"
            //self.dobFeild.text = "\(currentdate)"
            //self.dobFeild.text = dateFormattor.string(from: currentdate)
            view.endEditing(true)
        }
        else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            let year: String = dateFormatter.string(from: self.datePicker.date)
            dateFormatter.dateFormat = "MM"
            let month: String = dateFormatter.string(from: self.datePicker.date)
            dateFormatter.dateFormat = "MMMM"
            let monthName: String = dateFormatter.string(from: self.datePicker.date)
            dateFormatter.dateFormat = "dd"
            let day: String = dateFormatter.string(from: self.datePicker.date)
            date = "\(day)-\(month)-\(year)"
            
            dobFeild.text = "\(day) \(monthName) \(year)"
            //self.dobFeild.text = "\(datePicker.date)"
            //self.dobFeild.text = dateFormattor.string(from: datePicker.date)
            view.endEditing(true)
        }
    }
    /// check for valid date format
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    /// check for valid password format
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: password)
    }
    
    /// check for password length
    func isPasswordLength(_ password : String) -> Bool{
        return password.count > 7 && password.count < 21 ? true : false
    }
    
    @IBAction func createAccountClicked(_ sender: Any) {
        /// checks for signup page
        if firstnameFeild.text == "" {
            makeAlert(AlertTilte: "First Name Missing", AlertMessage: "Enter first name")
        }
        else if firstnameFeild.text!.count <= 2 {
            makeAlert(AlertTilte: "First Name Error", AlertMessage: "First name is too small")
        }
        else if firstnameFeild.text!.contains(" "){
            makeAlert(AlertTilte: "First Name Error", AlertMessage: "Spaces are not allowed")
        }
        else if lastnameFeild.text == "" {
            makeAlert(AlertTilte: "Last Name Missing", AlertMessage: "Enter last name")
        }
        else if lastnameFeild.text!.count <= 2 {
            makeAlert(AlertTilte: "Last Nmae Error", AlertMessage: "Last name is too small")
        }
        else if lastnameFeild.text!.contains(" "){
            makeAlert(AlertTilte: "Last Name Error", AlertMessage: "Spaces are not allowed")
        }
        else if emailaddressFeild.text == "" {
            makeAlert(AlertTilte: "Email Address Missing", AlertMessage: "Enter email address")
        }
        else if emailaddressFeild.text!.contains(" "){
            makeAlert(AlertTilte: "Email Error", AlertMessage: "Spaces are not allowed")
        }
        else if !isValidEmail(testStr: emailaddressFeild.text!) {
            makeAlert(AlertTilte: "Email Error", AlertMessage: "Invalid format")
        }
        else if passwordfeild.text == "" {
            makeAlert(AlertTilte: "Password Missing", AlertMessage: "Enter password")
        }
        else if passwordfeild.text!.contains(" "){
            makeAlert(AlertTilte: "Email Error", AlertMessage: "Spaces are not allowed")
        }
        else if !isPasswordLength(passwordfeild.text!){
            makeAlert(AlertTilte: "Password Error", AlertMessage: "Password must be 8 to 20 characters")
        }
        else if !isPasswordValid(passwordfeild.text!) {
            makeAlert(AlertTilte: "Password Error", AlertMessage: "Password should be a combination of one upper case character, one lowercase character, one special character !,@,#,$,%,&,*,? and one number")
        }
        else if confirmpasswordFeild.text == "" {
            makeAlert(AlertTilte: "Confirm Password Missing", AlertMessage: "Enter confirmed password")
        }
        else if confirmpasswordFeild.text != passwordfeild.text {
            makeAlert(AlertTilte: "Password Mismatch", AlertMessage: "Confirm password does not match password")
        }
        else if dobFeild.text == "" {
            makeAlert(AlertTilte: "Date Of Birth Missing", AlertMessage: "Select date of birth")
        }
        else if flag == 2 {
            makeAlert(AlertTilte: "Gender Error", AlertMessage: "Select a gender")
        }
        else if check == 0 && familyLabelFeild.text == "" {
            makeAlert(AlertTilte: "Family Label Missing", AlertMessage: "Enter family label")
        }
        else if check == 1 && familycodeFeild.text == "" {
            makeAlert(AlertTilte: "Family Code Missing", AlertMessage: "Enter family code")
        }
        /// Api calling for sign up
        else {
            name = "\(firstnameFeild.text!) \(lastnameFeild.text!)"
            print(name)
            indicator.isHidden = false
            let Signupfunc = SCService<SignupModel.Request,SignupModel.Response>()
            
            Signupfunc.request("http://3.143.193.45:1337/api/signup", method: .post,parameters: SignupModel.Request(email: emailaddressFeild.text!, name: self.name, password: passwordfeild.text!, zip: "", state: "", country: "", gender: flag, family: self.familyLabelFeild.text! , familyCode: self.familycodeFeild.text!, dob: dobFeild.text!, city: "")) { (result) in
                
                switch result {
                
                case .success(let response):
                    self.indicator.stopAnimating()
                    debugPrint(response)
                    if response.status == true{
                        var user1 : user = user(status: response.status, message: response.message, token: response.data.token, createdAt: response.data.user.createdAt, updatedAt: response.data.user.updatedAt, id: response.data.user.id, name: response.data.user.name, email: response.data.user.email, city: response.data.user.city, state: response.data.user.state, zip: response.data.user.zip, country: response.data.user.country, dob: response.data.user.dob, gender: response.data.user.gender, family: response.data.user.family, familyCode: response.data.user.familyCode, profilepic: response.data.user.profilepic, fcmToken: response.data.user.fcmToken, san: response.data.user.san, verify: response.data.user.verify, reportCount: response.data.user.reportCount)
                        UserDefaults.standard.setValue(response.data.user.id, forKey: "uid")
//                        UserDefaults.standard.setValue(response.data.user.familyCode, forKey: "currentFamilyCode")

                        //UserDefaults.standard.setValue(response.data.user.id, forKey: "uid")
                        //UserDefaults.standard.setValue(response.data.user.familyCode, forKey: "currentFamilyCode")
                        
                    print(user1)
                    do {
                    let encoder = JSONEncoder()
                    let data = try encoder.encode(user1)
                    UserDefaults.standard.setValue(data, forKey: "USER")
                        print(UserDefaults.standard.object(forKey: "USER"))
                    } catch{
                        print("Error")
                    }
                    UserDefaults.standard.setValue(self.emailaddressFeild.text!, forKey: "Mail")
                    UserDefaults.standard.setValue(response.data.token, forKey: "token1")
                        UserDefaults.standard.setValue(response.data.token, forKey: "token")
                        UserDefaults.standard.setValue(1, forKey: "fcount")
                    UserDefaults.standard.setValue(1, forKey: "loginStep")
                   // UserDefaults.standard.set(self.familycodeFeild.text!, forKey: "currentFamilyCode")
                    UserDefaults.standard.set(response.data.user.familyCode, forKey: "currentFamilyCode")
                    self.performSegue(withIdentifier: "EverificationNav", sender: self)
                    }else{
                        self.showPromptAlert("Sorry!", message: response.message);
                    }
                case .failure(_):
                    
                    if UserDefaults.standard.integer(forKey: "loginStep") == 1 {
                        self.performSegue(withIdentifier: "EverificationNav", sender: self)
                    }
                    
                    let Signupfunc2 = SCService<SignupModel.Request,SignupModel.Response2>()
                    Signupfunc2.request("http://3.143.193.45:1337/api/signup", method: .post,parameters: SignupModel.Request(email: self.emailaddressFeild.text!, name: self.name, password: self.passwordfeild.text!, zip: "", state: "", country: "", gender: self.flag, family:self.familyLabelFeild.text!, familyCode: self.familycodeFeild.text! , dob: self.dobFeild.text!, city: "")) { (result) in
                        switch result {
                        
                        case .success(let response):
                            self.indicator.stopAnimating()
                            self.makeAlert(AlertTilte: "Sign up Failed", AlertMessage: "\(response.message)")
                        
                        case .failure(let error):
                            self.indicator.stopAnimating()
                            debugPrint(error)
                            self.makeAlert(AlertTilte: "Signup Error", AlertMessage: "No internet connection.")
                            
                        }
                    }
                }
            }
        }
    }
}
