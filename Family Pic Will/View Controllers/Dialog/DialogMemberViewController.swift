//
//  DialogMemberViewController.swift
//  FPW
//
//  Created by Apple on 30/04/2021.
//

import UIKit
import Alamofire
class DialogMemberViewController: UIViewController {
    var delegate :RefereshProfile?
    var wishText: UITextField?
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var Relation: UILabel!
    @IBOutlet weak var dialogSendMessageLabel: UILabel!
    @IBOutlet weak var dialogReportMember: UILabel!
    @IBOutlet weak var dialogMarkAsDeceased: UILabel!
    @IBOutlet weak var changeRelationship: UILabel!
    @IBOutlet weak var dialogBlockMember: UILabel!
    @IBOutlet weak var dialogPhotoVideo: UILabel!
    @IBOutlet weak var dialogViewWall: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    var member : User?
    var user : User?
    var memberid = ""
    var memberImage = ""
    var memberName = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        dialogSendMessageLabel.font = UIFont(name: "SFProText-Regular", size: 14)
        dialogReportMember.font = UIFont(name: "SFProText-Regular", size: 14)
        dialogMarkAsDeceased.font = UIFont(name: "SFProText-Regular", size: 14)
        changeRelationship.font = UIFont(name: "SFProText-Regular", size: 14)
        dialogBlockMember.font = UIFont(name: "SFProText-Regular", size: 14)
        dialogPhotoVideo.font = UIFont(name: "SFProText-Regular", size: 14)
        dialogViewWall.font = UIFont(name: "SFProText-Regular", size: 14)
        Name.text = "\( member?.name ?? "" ) \( member?.relation ?? "" )"
        memberid = member?.id ?? ""
        memberName = "\( member?.name ?? "" )"
        memberImage = "\( member?.profilepic ?? "" )"
        indicator.isHidden = true
        indicator.startAnimating()
        
        //Dialog box for report member option
        var gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(reportMember))
        dialogReportMember.isUserInteractionEnabled = true
        dialogReportMember.addGestureRecognizer(gestureRecognizer)
        
        //Dialog box for mark as deceased option
        gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(markAsDeceased))
        dialogMarkAsDeceased.isUserInteractionEnabled = true
        dialogMarkAsDeceased.addGestureRecognizer(gestureRecognizer)
        
        //Dialog box for change relationship option
        gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(changeRelation))
        changeRelationship.isUserInteractionEnabled = true
        changeRelationship.addGestureRecognizer(gestureRecognizer)
        print(member!)
        self.userImage.sd_setImage(with: URL(string: self.member!.profilepic), placeholderImage: UIImage(named: "userprofile.png"))
        getMemberUser()
        var gestureTap1 = UITapGestureRecognizer(target: self, action: #selector(changeRelation))
        gestureTap1.numberOfTapsRequired = 1
        changeRelationship.addGestureRecognizer(gestureTap1)
        
        //Dialog box for report member option
        gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(blockMember))
        dialogBlockMember.isUserInteractionEnabled = true
        dialogBlockMember.addGestureRecognizer(gestureRecognizer)
        
        //Dialog box for photo/video option
            gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(photoVideo))
            dialogPhotoVideo.isUserInteractionEnabled = true
            dialogPhotoVideo.addGestureRecognizer(gestureRecognizer)
            //Dialog box for vieew wall option
            gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewWall))
            dialogViewWall.isUserInteractionEnabled = true
            dialogViewWall.addGestureRecognizer(gestureRecognizer)
        
        gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SendMessage))
        dialogSendMessageLabel.isUserInteractionEnabled = true
        dialogSendMessageLabel.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func SendMessage(){
        
        guard let menuuViewController = self.storyboard?.instantiateViewController(identifier: "Chatstoryboardid") as?
                ChatViewController else{
            return
        }
        let presentingVC = self.presentingViewController
        menuuViewController.modalPresentationStyle = .fullScreen
        var smember = [self.member!]
        menuuViewController.member = smember
        menuuViewController.receiver = self.member!.id
        menuuViewController.membername = memberName
        menuuViewController.memberimage = memberImage
        
        dismiss(animated: true){
            presentingVC?.present(menuuViewController, animated: true, completion: nil)
        }

        
    }
    
    
    // function for the report member dialog box
    @objc func reportMember(){
        
        let wishAlert = UIAlertController(title: "Report Member", message: nil, preferredStyle: .alert)
        wishAlert.addAction(UIAlertAction(title: "Report", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.reportmember()
        })
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        
        wishAlert.addAction(cancelAction)
        self.present(wishAlert, animated: true)
        
        //wishAlert.addTextField(configurationHandler: wishText)
        
    }

    // function for the mark as deceased dialog box
    @objc func markAsDeceased(){
        
        let wishAlert = UIAlertController(title: "Deceased", message: nil, preferredStyle: .alert)
        wishAlert.addAction(UIAlertAction(title: "Deceased", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.deceasedmember()
        })
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        
        wishAlert.addAction(cancelAction)
        self.present(wishAlert, animated: true)
        
        //wishAlert.addTextField(configurationHandler: wishText)
        
    }
    
    // function for the change relation dialog box
    @objc func changeRelation(){
        
        let wishAlert = UIAlertController(title: "Relationship", message: nil, preferredStyle: .alert)
        wishAlert.addAction(UIAlertAction(title: "Add", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.sendHandler()
        })
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        
        wishAlert.addAction(cancelAction)
        self.present(wishAlert, animated: true)
        
        wishAlert.addTextField(configurationHandler: wishText)
        
    }
    
    // function for the block member dialog box
    @objc func blockMember(){
        
        let wishAlert = UIAlertController(title: "Block", message: nil, preferredStyle: .alert)
        wishAlert.addAction(UIAlertAction(title: "Block", style: UIAlertAction.Style.default) { (UIAlertAction) in
            self.blockmember()
        })
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        
        wishAlert.addAction(cancelAction)
        self.present(wishAlert, animated: true)
        
        //wishAlert.addTextField(configurationHandler: wishText)
        
    }
    
    // function for the photo video
      @objc func photoVideo() {
        print("tepped")
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "UserMediaStoryboardId")
        as? UserMediaViewController else {
            return
        }
        let presentingVC = self.presentingViewController
        menuViewController.modalPresentationStyle = .fullScreen
        menuViewController.user = self.user
        menuViewController.member = self.member
        dismiss(animated: true) {
            presentingVC!.present(menuViewController, animated: true,completion: nil)
        }
      }
    
    // function for the view wall
      @objc func viewWall(){
        print("tapped")
        guard let menuuViewController = self.storyboard?.instantiateViewController(identifier: "BelongingStoryboardId") as?
                BelongingViewController else{
            return
        }
        let presentingVC = self.presentingViewController
        menuuViewController.user = self.user
        menuuViewController.member = self.member
        dismiss(animated: true){
            presentingVC?.present(menuuViewController, animated: true, completion: nil)
        }
        
      }
        
        
      
        
    
    func wishText( wishtextfield: UITextField){
        wishText = wishtextfield
        wishText?.placeholder = "add relation"
        
    }
    
    func sendHandler(){
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCallChangeRelation.Request(email: self.member!.email, relation: (wishText?.text)!)
                let requestService = SCService<ApiCallChangeRelation.Request,ApiCallChangeRelation.Response>()
                requestService.request("http://3.143.193.45:1337/api/changerelation", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
        
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        self.Relation.text = (self.wishText?.text)!
                        
                        //self.performSegue(withIdentifier: "MainNav", sender: self)
                    case .failure(let error):
                        debugPrint(error)
                   
                        print(error)
        
                    }
        
        
                }
    }
    
    func reportmember(){
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCallChangeRelation.Request(id: self.member!.id)
                let requestService = SCService<ApiCallChangeRelation.Request,ApiCallChangeRelation.Response>()
                requestService.request("http://3.143.193.45:1337/api/reportuser", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
        
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                    case .failure(let error):
                        debugPrint(error)
                   
                        print(error)
        
                    }
        
        
                }
    }
    func deceasedmember(){
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCallChangeRelation.Request(id: self.member!.id)
                let requestService = SCService<ApiCallChangeRelation.Request,ApiCallChangeRelation.Response>()
                requestService.request("http://3.143.193.45:1337/api/markdeceased", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
        
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                    case .failure(let error):
                        debugPrint(error)
                   
                        print(error)
        
                    }
        
        
                }
    }
    func blockmember(){
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCallChangeRelation.Request(id: self.member!.id)
                let requestService = SCService<ApiCallChangeRelation.Request,ApiCallChangeRelation.Response>()
                requestService.request("http://3.143.193.45:1337/api/block", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
        
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                    case .failure(let error):
                        debugPrint(error)
                   
                        print(error)
        
                    }
        
        
                }
    }
    func cancelBtn( alert: UIAlertAction) {
        
    }
    
    func makeAlert2(AlertTilte: String, AlertMessage: String){
        let alert = UIAlertController(title: AlertTilte, message: AlertMessage, preferredStyle: UIAlertController.Style.alert)
        
        let noButton = UIAlertAction(title: "No", style: UIAlertAction.Style.default)
        let yesButton = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (UIAlertAction) in
            
        }
        alert.addAction(noButton)
        alert.addAction(yesButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func getMemberUser(){
        indicator.isHidden = false
        self.indicator.startAnimating()
        
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallGetProfileById.Request,ApiCallGetProfileById.Response>()
        let param = ApiCallGetProfileById.Request(id: member!.id, page: 0)
        requestService.request("http://3.143.193.45:1337/api/getprofilebyid2", method: .post, parameters: param, encoder: JSONParameterEncoder(), header: header){(result) in
            switch result{
            case .success(let response):
                self.indicator.isHidden = true
                self.member?.createdAt = response.data!.createdAt
                self.member?.updatedAt = response.data!.updatedAt
                self.member?.email = response.data!.email
                self.member?.dob  = response.data!.dob
                self.Relation.text = ( response.data!.relation )
                self.member?.family = response.data!.family
                self.member?.familyCode = response.data!.familyCode
                self.member?.fcmToken = response.data!.fcmToken
                self.member?.gender = response.data!.gender
                self.member?.id = response.data!.id
                self.member?.zip = response.data!.zip
                self.member!.name = response.data!.name
                self.member?.profilepic = response.data!.profilepic
                self.member?.state = response.data!.state
                self.member?.san = response.data!.san
                self.member?.verify = response.data!.verify
                self.member?.country = response.data!.country
                self.member?.city = response.data!.city
                debugPrint(response)
                
                self.userImage.sd_setImage(with: URL(string: self.member!.profilepic), placeholderImage: UIImage(named: "userprofile.png"))
                self.Name.text = self.member?.name
                
            case .failure(let error):
                debugPrint(error)
                self.indicator.isHidden = true
            }
            
        }
    }
    @IBAction func cancelButton(_ sender: Any) {
        delegate?.refereshProfile()
        dismiss(animated: true, completion: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }

}
