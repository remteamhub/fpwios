//
//  DialogUnjoinFamilyViewController.swift
//  FPW
//
//  Created by Apple on 05/05/2021.
//

import UIKit
import Alamofire
protocol RelodeFamilyList : AnyObject {
    func refereshFamily()
}
class DialogUnjoinFamilyViewController: UIViewController {
    public var delegate : RelodeFamilyList?
    public var family :Family?
    @IBOutlet weak var familyCodeDialogTextView: UITextField!
    @IBAction func cancelClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    @IBAction func unjoinFamilyDialogClicked(_ sender: Any) {
        let parameter = ApiCallRemoveFamily.Request(familyCode: familyCodeDialogTextView.text!)
        if familyCodeDialogTextView.text == UserDefaults.standard.string(forKey: "currentFamilyCode") {
            self.makeAlert(AlertTilte: "Error", AlertMessage: "You cannot join the current family.")
        }
        else {
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallRemoveFamily.Request,ApiCallRemoveFamily.Response>()
        requestService.request("http://3.143.193.45:1337/api/removefamily", method: .post, parameters: parameter, encoder: JSONParameterEncoder(), header: header){(result) in
            switch result{
            case .success(let response):
                self.delegate?.refereshFamily()
                debugPrint(response)
                self.dismiss(animated: true, completion: nil)
            case .failure(let error):
                debugPrint(error)
            }
        }
        }
    }
            
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        familyCodeDialogTextView.text = family?.familyCode
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func makeAlert(AlertTilte: String, AlertMessage: String){
        let alert = UIAlertController(title: AlertTilte, message: AlertMessage, preferredStyle: UIAlertController.Style.alert)
        let Okbutton = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(Okbutton)
        self.present(alert, animated: true, completion: nil)
    }
}
