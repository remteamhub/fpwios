//
//  DialogJoinFamilyViewController.swift
//  FPW
//
//  Created by Apple on 04/05/2021.
//

import UIKit
import Alamofire
class DialogJoinFamilyViewController: UIViewController {
    //public var delegate : RelodeFamilyList?
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var familyCodeTextview: UITextField!
    @IBAction func joinFamilyClicked(_ sender: Any) {
        if familyCodeTextview.text == "" {
            makeAlert(AlertTilte: "Error", AlertMessage: "No family code found")
        }
        indicator.isHidden = false
        self.indicator.startAnimating()
        let parameter = ApiCallAddFamilyByCode.Request(familyCode: familyCodeTextview.text!)
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallAddFamilyByCode.Request,ApiCallAddFamilyByCode.Response>()
        requestService.request("http://3.143.193.45:1337/api/addfamilybycode", method: .post, parameters: parameter, encoder: JSONParameterEncoder(), header: header){(result) in
            switch result{
            case .success(let response):
                debugPrint(response)
                self.indicator.isHidden = true
                
                //if response
               // UserDefaults.standard.set(response.data.familyCode, forKey: "currentFamilyCode")
               // self.switchh(FamilyCode: UserDefaults.standard.string(forKey: "currentFamilyCode")!)
                self.dismiss(animated: true, completion: nil)
                //self.delegate?.refereshFamily()
            case .failure(let error):
                self.indicator.isHidden = true
                self.makeAlert(AlertTilte: "Error", AlertMessage: "Invalid family code.")
                debugPrint(error)
            }
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    @IBAction func cancelClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        indicator.isHidden = true
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func switchh(FamilyCode: String) {
        let param = ApiCallChangeFmaily.Request(familyCode: FamilyCode)
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallChangeFmaily.Request,ApiCallChangeFmaily.Response>()
        requestService.request("http://3.143.193.45:1337/api/changefamily", method: .post, parameters: param, encoder: JSONParameterEncoder(), header: header){(result) in
            switch result{
            case .success(let response):
                debugPrint(response)
                UserDefaults.standard.set(response.data.familyCode, forKey: "currentFamilyCode")
                guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "ProfileStoryboardId") else {
                        return
                      }
                      let presentingVC = self.presentingViewController
                      menuViewController.modalPresentationStyle = .fullScreen
                      self.dismiss(animated: true) {
                        presentingVC!.present(menuViewController, animated: true) {
                          //self.dismiss(animated: true, completion: nil)
                        }
                      }
            case .failure(let error):
                debugPrint(error)
            }
    }

}
    func makeAlert(AlertTilte: String, AlertMessage: String){
        let alert = UIAlertController(title: AlertTilte, message: AlertMessage, preferredStyle: UIAlertController.Style.alert)
        let Okbutton = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(Okbutton)
        self.present(alert, animated: true, completion: nil)
    }
}
