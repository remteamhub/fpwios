//
//  ForgetPasswordVC.swift
//  Family Pic Will
//
//  Created by mac on 4/27/21.
//

import UIKit

class ForgetPasswordVC: UIViewController {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var emailFeild: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    override func viewDidLoad() {
        self.indicator.isHidden = true
        super.viewDidLoad()
        emailFeild.attributedPlaceholder = NSAttributedString(string: "Email Address",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        emailFeild.font = UIFont(name: "Montserrat-SemiBold", size: 13)
        
        sendButton.layer.cornerRadius = 8.0
        sendButton.titleLabel?.font = UIFont(name: "Montserrat-SemiBold", size: 21)
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    @IBAction func sendClicked(_ sender: Any) {
        self.indicator.isHidden = false
        if emailFeild.text == ""{
            makeAlert(AlertTilte: "Email Missing", AlertMessage: "Enter your email")
        }
        
        else {
            let FP = ForgetPassModel.Request(email: emailFeild.text!)
            let FPfunc = SCService<ForgetPassModel.Request,ForgetPassModel.Response>()
            FPfunc.request("http://3.143.193.45:1337/api/forgetpassword", method: .post,parameters: FP) { (result) in
                switch result {
                case .success(let response):
                    self.indicator.stopAnimating()
                    if response.status == true{
                    debugPrint(response)
                    self.makeAlert(AlertTilte: "Code Sent", AlertMessage: "")
                    //self.performSegue(withIdentifier: "MainNav", sender: self)
                    }else{
                        self.showPromptAlert("Sorry!", message: response.message);
                    }
                case .failure(let error):
                    self.indicator.stopAnimating()
                    debugPrint(error)
                    self.makeAlert(AlertTilte: "Error", AlertMessage: "Invalid Email")
                    
                }
            }
        }
    }
   
    
    
    
    
    func makeAlert(AlertTilte: String, AlertMessage: String){
        let alert = UIAlertController(title: AlertTilte, message: AlertMessage, preferredStyle: UIAlertController.Style.alert)
        let Okbutton = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(Okbutton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
}
