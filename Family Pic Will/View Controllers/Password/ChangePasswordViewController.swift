//
//  ChangePasswordViewController.swift
//  FPW
//
//  Created by Apple on 06/05/2021.
//

import UIKit
import Alamofire
class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var titleBarlabel: UILabel!
    var iconClick = false
    @IBOutlet weak var retypeNewPasswordTextfield: UITextField!
    @IBOutlet weak var newPasswordTextfield: UITextField!
    @IBOutlet weak var changePasswordTextfield: UITextField!
    @IBOutlet weak var newPasswordLabel: UILabel!
    
    @IBOutlet weak var curentPasswordLabel: UILabel!
    var user:User?
    @IBOutlet weak var newTypePasswordLabel: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBAction func showPasswordClicked(_ sender: Any) {
        if(iconClick == true) {
             changePasswordTextfield.isSecureTextEntry = false
               } else {
                 changePasswordTextfield.isSecureTextEntry = true
               }
               iconClick = !iconClick
    }
    @IBAction func showNewPasswordClicked(_ sender: Any) {
        if(iconClick == true) {
              newPasswordTextfield.isSecureTextEntry = false
                } else {
                  newPasswordTextfield.isSecureTextEntry = true
                }
                iconClick = !iconClick
    }
    @IBAction func showRetypePasswordClicked(_ sender: Any) {
        if(iconClick == true) {
              retypeNewPasswordTextfield.isSecureTextEntry = false
                } else {
                  retypeNewPasswordTextfield.isSecureTextEntry = true
                }
                iconClick = !iconClick
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    @IBAction func updateClicked(_ sender: Any) {
        if changePasswordTextfield.text == "" {
              self.makeAlert(AlertTilte: "Previous Password Missing", AlertMessage: "Enter last password")
            }
            if newPasswordTextfield.text == "" {
              self.makeAlert(AlertTilte: "New Password Missing", AlertMessage: "Enter new password")
            }
            else if newPasswordTextfield.text!.contains(" "){
              makeAlert(AlertTilte: "New Password Error", AlertMessage: "Spaces are not allowed")
            }
            else if !isPasswordLength(newPasswordTextfield.text!){
              makeAlert(AlertTilte: "New Password Error", AlertMessage: "Password must be 8 to 20 characters")
            }
            else if !isPasswordValid(newPasswordTextfield.text!) {
              makeAlert(AlertTilte: "Password Error", AlertMessage: "Password should be a combination of one upper case character, one lowercase character, one special character !,@,#,$,%,&,*,? and one number")
            }
            else if newPasswordTextfield.text != retypeNewPasswordTextfield.text {
              makeAlert(AlertTilte: "Password Mismatch Error", AlertMessage: "New password and confirm password mismatched")
            }
            else {
                indicator.isHidden = false
                self.indicator.startAnimating()
        let param = ApiCallUpdatePassword.Request(oldpassword: changePasswordTextfield.text!, newpassword: newPasswordTextfield.text!)
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallUpdatePassword.Request,ApiCallUpdatePassword.Response>()
        requestService.request("http://3.143.193.45:1337/api/updatepassword", method: .post, parameters: param, encoder: JSONParameterEncoder(), header: header){(result) in
            switch result{
            case .success(let response):
                debugPrint(response)
                self.indicator.isHidden = true
                if response.status == true{
                
                self.makeAlert(AlertTilte: "Status", AlertMessage: response.message)
                //let alertdialg = UIC
                //self.familyList = response.data
                }else{
                    self.showPromptAlert("Sorry!", message: response.message);
                }
                //self.familyListTableview.reloadData()
            case .failure(let error):
                debugPrint(error)
                self.indicator.isHidden = true
            }
        }
        }
    }
    @IBAction func canceClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 38/255, green: 56/255, blue: 75/255,alpha: 1)
        indicator.isHidden = true
        titleBarlabel.font = UIFont(name: "Roboto-Regular", size: 20)
        curentPasswordLabel.font = UIFont(name: "Roboto-Regular", size: 15)
        newPasswordLabel.font = UIFont(name: "Roboto-Regular", size: 15)
        retypeNewPasswordTextfield.font = UIFont(name: "Roboto-Regular", size: 15)
        newPasswordTextfield.font = UIFont(name: "Open Sans Regular", size: 14)
        retypeNewPasswordTextfield.font = UIFont(name: "Open Sans Regular", size: 14)
        changePasswordTextfield.font = UIFont(name: "Open Sans Regular", size: 14)
        // Do any additional setup after loading the view.
    }
    
    func makeAlert(AlertTilte: String, AlertMessage: String){
        let alert = UIAlertController(title: AlertTilte, message: AlertMessage, preferredStyle: UIAlertController.Style.alert)
        let Okbutton = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(Okbutton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: password)
    }
    
    /// check for password length
    func isPasswordLength(_ password : String) -> Bool{
        return password.count > 7 && password.count < 21 ? true : false
    }

}
