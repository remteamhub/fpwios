//
//  ViewController.swift
//  Bdaynotif
//
//  Created by Coder Crew on 4/29/21.
//

import UIKit
import SwiftyJSON
import Alamofire
class BirthDayViewController: UIViewController, UITableViewDelegate,UITableViewDataSource, UINavigationControllerDelegate{
    var wishText: UITextField?
    var member: User?
    var user: User?
    var display: [User?] = []
    var notifications: [notification?] = []
    let transition = SlideInTransition()
    @IBOutlet weak var Bdaytableview: UITableView!
    var bdays: [Bday] = [Bday(userimage: "puck", username: " Moeez Ahmad Raza qadri padri"),Bday(userimage: "puck",  username: " Pucks")]
    override func viewDidLoad() {
        super.viewDidLoad()
        getNotification()
        Bdaytableview.dataSource = self
        Bdaytableview.delegate = self
        transition.delegate = self
        
    }
    
    
    
    @IBAction func openSideMenuClicked(_ sender: Any) {
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "SideMenuStoryboardId") else {
              return
            }
        menuViewController.modalPresentationStyle = .overCurrentContext
        menuViewController.transitioningDelegate = self
        present(menuViewController, animated: true)
            
    }
    
    func getNotification(){
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
      
                let requestService = SCService<NotificationApi.Request,NotificationApi.Response>()
                requestService.request("http://3.143.193.45:1337/api/getnotifications", method: .post, parameters: nil, encoder: JSONParameterEncoder(), header: header){
                    (result) in

                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        //self.member = response.data
                        self.notifications = response.data
                        print(response.status)
                        print(response.message)
                        //print(response.data.text)
                        self.Bdaytableview.reloadData()
                        if response.status == false{
                        self.showPromptAlert("Sorry!", message: response.message);
                    }
                       
                    case .failure(let error):
                        debugPrint(error)

                        print(error)

                    }
                }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bdaytableview.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for:  indexPath) as! BdayTableViewCell
        cell.nameLable.text = notifications[indexPath.row]?.name
        cell.Bdaycellimage.layer.borderWidth = 1.0
        cell.Bdaycellimage.layer.masksToBounds = false
            cell.Bdaycellimage.layer.borderColor = UIColor.white.cgColor
            cell.Bdaycellimage.layer.cornerRadius = cell.Bdaycellimage.frame.size.width / 2
            cell.Bdaycellimage.clipsToBounds = true
        cell.Bdaycellimage.sd_setImage(with: URL(string: notifications[indexPath.row]!.pic),placeholderImage: UIImage(named: "userprofile.png"))
        
        cell.Bdaycelllabel.text = notifications[indexPath.row]?.text
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if(notifications[indexPath.row]?.verb == "Deceased" || notifications[indexPath.row]?.verb == "Reported" )
        {
            let id = notifications[indexPath.row]?.post_id
            deletenotification(postid: notifications[indexPath.row]?.id ?? "",id: id ?? "")
            getNotification()
        }
        else{
            let id = notifications[indexPath.row]?.post_id
        deletenotification(postid: notifications[indexPath.row]?.id ?? "",id: id ?? "")
        
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "NewNotificationStroryboardId")
        as? NewNotificationViewController else {
            return
        }

        menuViewController.postid = id ?? ""

        let presentingVC = self.presentingViewController
        menuViewController.modalPresentationStyle = .fullScreen
        self.dismiss(animated: true) {
            presentingVC!.present(menuViewController, animated: true,completion: nil)
        }
        }
        
    }
    
}

extension BirthDayViewController : BirthdayDataForcell{
    func buttonTap(UserId: String, familyCode: String) {
        
        let wishAlert = UIAlertController(title: "Send Wishes", message: nil, preferredStyle: .alert)
        wishAlert.addTextField { (textField : UITextField!) -> Void in
              textField.placeholder = "Send Wishes..."
            }
        wishAlert.addAction(UIAlertAction(title: "Wish", style: UIAlertAction.Style.default) { (UIAlertAction) in
            let firstTextField = wishAlert.textFields![0] as UITextField
            self.sendHandler(Wish: firstTextField.text ?? "Happy Birthday...", id: UserId, fcode: familyCode)

        })
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel, handler: nil)

        wishAlert.addAction(cancelAction)
        self.present(wishAlert, animated: true)
    }
    
    func sendHandler(Wish: String, id: String, fcode: String){
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let wish = BirthdayWishApi.Request(text: Wish, familyCode: fcode, userid: id)
                let requestService = SCService<BirthdayWishApi.Request,BirthdayWishApi.Response>()
                requestService.request("http://3.143.193.45:1337/api/birthday", method: .post, parameters: wish, encoder: JSONParameterEncoder(), header: header){
                    (result) in

                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                      
                        if response.status == false{
                        self.showPromptAlert("Sorry!", message: response.message);
                    }
                      
                    case .failure(let error):
                        debugPrint(error)

                        print(error)

                    }
                }
    }
    func deletenotification(postid: String,id: String){
        
        
        
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = NotificationApi.deleteRequest(id: postid)
                let requestService = SCService<NotificationApi.deleteRequest,NotificationApi.deleteResponse>()
                requestService.request("http://3.143.193.45:1337/api/delnotification", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
                   
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == true{
                        }else
                        {
                          
                        }
                        
                    case .failure(let error):
                        debugPrint(error)
                        
                        print(error)
        
                    }
        
        
                }
        
    }
    
    
  
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
}
extension BirthDayViewController : UIViewControllerTransitioningDelegate{
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = true
    return transition
  }
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = false
    return transition
  }
}
extension BirthDayViewController : CloseSideMenu{
    func closeSideMenu(){
        dismiss(animated: true, completion: nil)
    }
}
