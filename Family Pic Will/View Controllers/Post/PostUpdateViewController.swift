//
//  PostUpdateViewController.swift
//  familywithwall
//
//  Created by Apple on 20/05/2021.
//

import UIKit
import AVKit
import MobileCoreServices
import AVFoundation
import SwiftyJSON
import Alamofire
class PostUpdateViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate,UITextViewDelegate {
    var posttid: String = ""
    var type: String = ""
    var check: Bool = true
    var uploadtype: String = ""
    var text: String = ""
    var labelll: String = ""
    var checkVideo: Bool = false
    var selectedimage: String = ""
    var imagePicker = UIImagePickerController()
    var videoURL : URL?
    var empty : URL?
    var playerViewController = AVPlayerViewController()
      var playerView = AVPlayer()
    var user: User?
    var videoAndImageReview = UIImagePickerController()
    
    @IBOutlet weak var deleteimgOutlet: UIButton!
    @IBOutlet weak var postBtnOutlet: UIButton!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var VideoBtnOutlet: UIButton!
    @IBOutlet weak var UserName: UILabel!
    @IBOutlet weak var postDescriptionTextField: UITextView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var PostImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        self.loading.isHidden = true
        self.loading.startAnimating()
        postDescriptionTextField.text = text
        getuser()
        userImage.layer.cornerRadius = userImage.frame.size.width / 2
        userImage.clipsToBounds = true
      
        placeimage_video()
      

        // Do any additional setup after loading the view.
        postDescriptionTextField.delegate = self
        
        // Do any additional setup after loading the view.
    }
    func placeimage_video(){
        if type == "1"{
            if selectedimage != ""{
                self.deleteimgOutlet.isHidden = false
        let url = URL(string: selectedimage)
        let data = try? Data(contentsOf: url!)

        if let imageData = data {
            print("2")
            PostImage.image = UIImage(data: imageData)
            self.check = true
        }
            }else{
                self.PostImage.image = UIImage(named: "")
                self.deleteimgOutlet.isHidden = true
                self.check = false
            }
        }
        if type == "2"{
            self.deleteimgOutlet.isHidden = false
            self.VideoBtnOutlet.isHidden = false
            if selectedimage != ""{
            vidInIt(videoUrl : selectedimage)
                self.check = true
            }else{
                self.VideoBtnOutlet.isHidden = true
                self.PostImage.image = UIImage(named: "")
                self.deleteimgOutlet.isHidden = true
                self.check = false
            }
        }
    }
    func getuser(){
        if let data = UserDefaults.standard.data(forKey: "USER"){
            do {
                let decoder = JSONDecoder()
                let user = try decoder.decode(User.self, from: data)
                print(user.self)
                self.UserName.text = user.name
                self.user = user
               
                self.userImage.sd_setImage(with: URL(string: user.profilepic ), placeholderImage: UIImage(named: "userprofile.png"))
            }catch{
                print("Error")
            }
        }
    }
    @objc func tabelViewTapped() {
        self.postDescriptionTextField.endEditing(true)
    }
    
    
    
    
    func textViewDidBeginEditing (_ textView: UITextView) {
       
        
            if postDescriptionTextField.text.isEmpty || postDescriptionTextField.text == "What's on your mind?" {
                postDescriptionTextField.text = nil
                postDescriptionTextField.textColor = .black // YOUR PREFERED COLOR HERE
            }
        
        
        }
        func textViewDidEndEditing (_ textView: UITextView) {
            

            if postDescriptionTextField.text.isEmpty {
                postDescriptionTextField.textColor = UIColor.gray // YOUR PREFERED PLACEHOLDER COLOR HERE
                postDescriptionTextField.text =  "What's on your mind?"
            }
        }
    @IBAction func gobackBtn(_ sender: Any) {
        self.goback()
    }
    func goback(){
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "WallPostStoryboardId") as? WallPostViewController else {
              return
            }
        
        menuViewController.labelll = labelll
            menuViewController.modalPresentationStyle = .overFullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
    }
    
    @IBAction func PlayVideo(_ sender: Any) {
        if checkVideo == true {
            playvideo(videoUrl : "\(videoURL)")
        }else{
        playvideo(videoUrl : selectedimage)
        }
    }
    
    @IBAction func PostBtn(_ sender: Any) {
        if check == false{
            print("calling post")
            self.loading.isHidden = false
            self.postBtnOutlet.isHidden = true
            updatePostByText()
            
        }else{
            self.loading.isHidden = false
            self.postBtnOutlet.isHidden = true
            updatePost()
        }
        
        
        
        
    }
    
    @IBAction func deleteImageBtn(_ sender: Any) {
        self.selectedimage = ""
        placeimage_video()
        
        
    }
    func updatePost(){
       
        let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        
        let parameter = ["postid": posttid, "type": type, "text": postDescriptionTextField.text!]
        let imageData = (PostImage.image?.jpegData(compressionQuality: 0.8))!
        print("q")
        print(imageData as Any)
        
        
                    AF.upload(multipartFormData: { multipartFormData in
                        for (key, value) in parameter {
                            if let temp = value as? String {
                                           multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                                       }
                           
                            print("parameter")
                           
                        }
                        
                            print("checked")
                        if self.type == "1"{
                            multipartFormData.append(imageData, withName: "image" , fileName: "file.png", mimeType: "image/png")
                        }
                        if self.type == "2"{
                            multipartFormData.append(self.videoURL! as URL, withName: "video", fileName: "video.mp4", mimeType: "video/mp4")
                        }
                                
                    }, to: "http://3.143.193.45:1337/api/editpost",method: .post , headers: header)
                        .responseJSON(completionHandler: { (response) in
                                    print(parameter)
                                    print(response)
                                    
                                    if let err = response.error{
                                        print(err)
                                        self.postBtnOutlet.isHidden = false
                                        return
                                    }
                                    print("Succesfully uploaded")
                                    
                                    let json = response.data
                                    print("1")
                            print(json)
                                    if (json != nil)
                                    {
                                        let jsonObject = JSON(json!)
                                        print(jsonObject)
                                        self.loading.isHidden = true
                                        self.goback()
                                    }
                                })
    }
    func updatePostByText(){
        
        let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        
        let parameter = ["postid": posttid, "type": "0", "text": postDescriptionTextField.text!]
        
        
        
                    AF.upload(multipartFormData: { multipartFormData in
                        for (key, value) in parameter {
                            if let temp = value as? String {
                                           multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                                       }
                           
                            print("parameter")
                           
                        }
                        
                            print("checked")
                                
                    }, to: "http://3.143.193.45:1337/api/editpost",method: .post , headers: header)
                        .responseJSON(completionHandler: { (response) in
                                    print(parameter)
                                    print(response)
                                    
                                    if let err = response.error{
                                        print(err)
                                        self.postBtnOutlet.isHidden = false
                                        return
                                    }
                                    print("Succesfully uploaded")
                                    
                                    let json = response.data
                                    print("1")
                            print(json)
                                    if (json != nil)
                                    {
                                        let jsonObject = JSON(json!)
                                        print(jsonObject)
                                        self.loading.isHidden = true
                                        self.goback()
                                    }
                                })
            }
    
    
    @IBAction func VideoBtn(_ sender: Any) {
        self.uploadtype = "2"
        
        videoBtnAction(var: "Camera", var: "Gallery")
    }
    @IBAction func PhotoBtn(_ sender: Any) {
        self.uploadtype = "1"
        wishBtnAction(var: "Camera", var: "Gallery")
    }
    
    func videoBtnAction(`var` str1:String, `var` str2:String) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: str1, style: .default, handler: { _ in
            self.RecordAction()
        }))
        
        alert.addAction(UIAlertAction(title: str2, style: .default, handler: { _ in
            self.openImgVideoPicker()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        
        
        self.present(alert, animated: true, completion: nil)
        
      }
    
    
    
    
    
    
    
    //video
    func RecordAction() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            print("Camera Available")
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.mediaTypes = [kUTTypeMovie as String]
            imagePicker.allowsEditing = true
             
            self.present(imagePicker, animated: true, completion: nil)
          } else {
            let alert = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
          }
        }
      //capture
      func imagePickerController(_ picker: UIImagePickerController,
                      didFinishPickingMediaWithInfo info: [String : Any]) {
          dismiss(animated: true, completion: nil)
          guard
            let mediaType = info[UIImagePickerController.InfoKey.mediaType.rawValue] as? String,
            mediaType == (kUTTypeMovie as String),
            let url = info[UIImagePickerController.InfoKey.mediaURL.rawValue] as? URL,
            UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path)
            else {
              return
          }
           
          // Handle a movie capture
          UISaveVideoAtPathToSavedPhotosAlbum(
            url.path,
            self,
            #selector(video(_:didFinishSavingWithError:contextInfo:)),
            nil)
        }
        @objc func video(_ videoPath: String, didFinishSavingWithError error: Error?, contextInfo info: AnyObject) {
          let title = (error == nil) ? "Success" : "Error"
          let message = (error == nil) ? "Video was saved" : "Video failed to save"
          let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
          alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
          present(alert, animated: true, completion: nil)
        }
      ///
      //videopick
      func openImgVideoPicker() {
        print("11")
          videoAndImageReview.sourceType = .savedPhotosAlbum
          videoAndImageReview.delegate = self
          videoAndImageReview.mediaTypes = ["public.movie"]
          present(videoAndImageReview, animated: true, completion: nil)
        }

    
    
    
    func wishBtnAction(`var` str1:String, `var` str2:String) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: str1, style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: str2, style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
       
        
        self.present(alert, animated: true, completion: nil)
        
      }
    
    

    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
                //If you dont want to edit the photo then you can set allowsEditing to false
                imagePicker.allowsEditing = true
                imagePicker.delegate = self
            self.present(imagePicker, animated: true,completion: nil)
            }
            else{
                let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        //MARK: - Choose image from camera roll
        
        func openGallary(){
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            //If you dont want to edit the photo then you can set allowsEditing to false
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            self.present(imagePicker, animated: true,completion: nil)

        }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if uploadtype == "2"{
            print("22")
            videoURL = (info[.mediaURL] as! URL)
            print("selectedimage")
            print(selectedimage)
            self.checkVideo = true
            
            do {
                print("33")
                let asset = AVURLAsset(url: videoURL! as! URL , options: nil)
                let imgGenerator = AVAssetImageGenerator(asset: asset)
                imgGenerator.appliesPreferredTrackTransform = true
                let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
                let thumbnail = UIImage(cgImage: cgImage)
                self.PostImage.image = thumbnail
                self.deleteimgOutlet.isHidden = false
                self.deleteimgOutlet.isUserInteractionEnabled = true
                self.VideoBtnOutlet.isHidden = false
                self.check = true
                self.type = "2"
            } catch let error {
                print("*** Error generating thumbnail: \(error.localizedDescription)")
            }
            self.dismiss(animated: true, completion: nil)
        }
        if uploadtype == "1"{
            print("456")
            picker.dismiss(animated: true, completion: nil)
            self.check = true
            self.type = "1"
            if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
                PostImage.image = editedImage
                self.deleteimgOutlet.isHidden = false
                self.VideoBtnOutlet.isHidden = true
                self.deleteimgOutlet.isUserInteractionEnabled = true
                
                }
        }
        
    }
    

    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.isNavigationBarHidden = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func playvideo(videoUrl : String) {
        let url: URL = URL(string: videoUrl)!
        playerView = AVPlayer(url: url)
        playerViewController.player = playerView
        self.present(playerViewController, animated: true)
        self.playerViewController.player?.play()
    }
    
    func vidInIt(videoUrl : String) {
          let url = URL(string: videoUrl)
        self.getThumbnailImageFromVideoUrl(url: url!){ (thumbnailImage) in
          self.PostImage.image = thumbnailImage
        }
      }
      //MARK -thumbnail
      func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
          let asset = AVAsset(url: url) //2
          let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
          avAssetImageGenerator.appliesPreferredTrackTransform = true //4
          let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
          do {
            let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
            let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
            DispatchQueue.main.async { //8
              completion(thumbNailImage) //9
            }
          } catch {
            print(error.localizedDescription) //10
            DispatchQueue.main.async {
              completion(nil) //11
            }
          }
        }
      }

}
