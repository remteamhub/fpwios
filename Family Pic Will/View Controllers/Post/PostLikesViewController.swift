//
//  PostLikesViewController.swift
//  familywithwall
//
//  Created by Apple on 29/04/2021.
//

import UIKit
import SwiftyJSON
import Alamofire

class PostLikesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var postId : String = ""
    var member : User?
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var PostlikesTabelView: UITableView!
    
    
    var labelll: String = ""
    var likes: [Likers] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        PostlikesTabelView.delegate = self
        PostlikesTabelView.dataSource = self
        self.loading.isHidden = false
        self.loading.startAnimating()
        getposts()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func likeBackBtn(_ sender: Any) {
        
        if labelll == "Notification" {
            
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "NewNotificationStroryboardId") as? NewNotificationViewController else {
                  return
                }
           
            
            menuViewController.postid = postId
            //menuViewController.user = self.user
            menuViewController.labelll = labelll
            menuViewController.modalPresentationStyle = .fullScreen
            
             
             self.present(menuViewController, animated: true, completion: nil)
            
        }
        
        else if labelll == "backtoMembersProfile"{
            gobacktoProfile()
        }else{
            gobacktoAlbum()
            
        }

    }
    func gobacktoProfile(){
        print("Clicked")
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "BelongingStoryboardId")  as? BelongingViewController else {
            return
        }
        
       
        menuViewController.member = member
        menuViewController.modalPresentationStyle = .fullScreen
        self.present(menuViewController, animated: true,completion: nil)
        print("Clicked")
    }
    func gobacktoAlbum(){
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "WallPostStoryboardId") as? WallPostViewController else {
              return
            }
        
        menuViewController.labelll = labelll
            menuViewController.modalPresentationStyle = .overFullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
    }
    
    func getposts(){
        
        
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let like = ApiCallLikers.Request(pid: postId)
                let requestService = SCService<ApiCallLikers.Request,ApiCallLikers.Response>()
                requestService.request("http://3.143.193.45:1337/api/getlikers", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
        
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == true{
                        print(response.data)
                        
                            self.likes = response.data!
                        self.PostlikesTabelView.reloadData()
                        }else{
                            self.showPromptAlert("Sorry!", message: response.message);
                        }
                        self.loading.isHidden = true
                        //self.performSegue(withIdentifier: "MainNav", sender: self)
                    case .failure(let error):
                        debugPrint(error)
                        
                        print(error)
        
                    }
        
        
                }
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(likes.count)
        return likes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = PostlikesTabelView.dequeueReusableCell(withIdentifier: "PostLikesCell", for: indexPath) as! PostLikesViewCell
        if likes[indexPath.row].likerprofilepic != ""{
        let url = URL(string: likes[indexPath.row].likerprofilepic)
        let data = try? Data(contentsOf: url!)

        if let imageData = data {
            cell.UserImage.image = UIImage(data: imageData)
        }
        }
        
        cell.UserName.text = likes[indexPath.row].likername
        return cell
    }


    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
}
