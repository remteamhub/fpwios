//
//  PostViewController.swift
//  familywithwall
//
//  Created by Apple on 05/05/2021.
//

import UIKit
import SwiftyJSON
import Alamofire
import AVKit
import AVFoundation
//import IQKeyboardManagerSwift
class PostViewController: UIViewController,UITextViewDelegate {
    var selectedimage: UIImage!
    var matchesarray : [Any] = []
    
    var type: String = ""
    var selectedPostDescription:String!
    var user : User?
    var labelll: String = ""
    var videoURL : URL?
    var fdate: Int = 0
    var captionString : String = "default"
    var urlimage : URL?
    
    @IBOutlet weak var postCaptionLabel: UITextField!
    
    var playerViewController = AVPlayerViewController()
    var playerView = AVPlayer()
    @IBOutlet weak var postimage: UIImageView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var VideoplayOutlet: UIButton!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var userimage: UIImageView!
    
    @IBOutlet weak var postBtnOutlet: UIButton!
    @IBOutlet weak var postInputTextField: UITextView!
    
    @IBOutlet weak var backClicked: UIButton!
    
    @IBAction func backClicked1(_ sender: Any) {
        goback()
    }
    override func viewDidLoad() {
        self.loading.isHidden = false
        self.loading.startAnimating()
        super.viewDidLoad()
        print(type)
        print(videoURL)
        userimage.layer.cornerRadius = userimage.frame.size.width / 2
        userimage.clipsToBounds = true
        if type == "1"{
            self.VideoplayOutlet.isHidden = true
      postimage.image = selectedimage
            self.loading.isHidden = true
        }
        if type == "2"{
            self.VideoplayOutlet.isHidden = false
            vidInIt(videoUrl : videoURL!)
        }
        if selectedPostDescription != "What's on your mind?"{
        postInputTextField.text = selectedPostDescription
        }
        username.text = user?.name
    
       
        self.userimage.sd_setImage(with: URL(string: self.user!.profilepic ), placeholderImage: UIImage(named: "user2.png"))
        postInputTextField.delegate = self
      
        // Do any additional setup after loading the view.
    }
    
    @IBAction func videoplayBtn(_ sender: Any) {
        playvideo(videoUrl : videoURL!)
        
    }
    func playvideo(videoUrl : URL) {
        //let url: URL = URL(string: videoUrl)!
        playerView = AVPlayer(url: videoUrl)
        playerViewController.player = playerView
        self.present(playerViewController, animated: true)
        self.playerViewController.player?.play()
    }
    func vidInIt(videoUrl : URL) {
         // let url = URL(string: videoUrl)
        self.getThumbnailImageFromVideoUrl(url: videoUrl){ (thumbnailImage) in
          self.postimage.image = thumbnailImage
        }
      }
      //MARK -thumbnail
      func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
          let asset = AVAsset(url: url) //2
          let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
          avAssetImageGenerator.appliesPreferredTrackTransform = true //4
          let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
          do {
            let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
            let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
            DispatchQueue.main.async { //8
              completion(thumbNailImage) //9
                self.loading.isHidden = true
            }
          } catch {
            print(error.localizedDescription) //10
            DispatchQueue.main.async {
              completion(nil) //11
            }
          }
        }
      }
    @objc func tabelViewTapped() {
        self.postInputTextField.endEditing(true)
    }
    
    @IBAction func backButtonTapped(_ sender: UIBarButtonItem) {
        self.goback()
    }
    
    func goback(){
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "WallPostStoryboardId") as? WallPostViewController else {
              return
            }
        
        menuViewController.labelll = labelll
            menuViewController.modalPresentationStyle = .overFullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
    }
    
    func textViewDidBeginEditing (_ textView: UITextView) {
       
        
            if postInputTextField.text.isEmpty || postInputTextField.text == "Say something about this post..." {
                postInputTextField.text = nil
                postInputTextField.textColor = .black // YOUR PREFERED COLOR HERE
            }
        
        
        }
        func textViewDidEndEditing (_ textView: UITextView) {
            

            if postInputTextField.text.isEmpty {
                postInputTextField.textColor = UIColor.gray // YOUR PREFERED PLACEHOLDER COLOR HERE
                postInputTextField.text =  "Say something about this post..."
            }
        }
    func checkcaption() {
        
        if(postCaptionLabel.text != "")
        {
        
        
            let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
            let parameter = ["models": "text","api_user": "406594658", "api_secret": "7x3uRdHtFyaNg4HHd5C7", "lang": "en", "mode": "standard", "text": postCaptionLabel.text!] as [String : Any]
            
            AF.upload(multipartFormData: { multipartFormData in
                            for (key, value) in parameter {
                                if let temp = value as? String {
                                    multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                                }
                            }
                           
                        }, to: "https://api.sightengine.com/1.0/text/check.json",method: .post , headers: header)
                            .responseJSON(completionHandler: { (response) in
                                
                                        if let err = response.error{
                                            
                                            return
                                        }
                                print(response)
                                
                                        let json = response.data
                                
                                        if (json != nil)
                                        {
                                            
                                            let jsonObject = JSON(json!)
                                            print(jsonObject)
                                            let jsonObjectprofanity = jsonObject["profanity"]
                                            print(jsonObjectprofanity)
                                            let jsonObjectmatches = jsonObjectprofanity["matches"]
                                            print(jsonObjectmatches)
                                            
                                        
                                            self.matchesarray = jsonObjectmatches.arrayObject! as [Any]
                                            print(self.matchesarray)
                                            if self.matchesarray.count>0 {
                                                self.showPromptAlert("Caption Alert", message: "Selected Caption is against our privacy policies", okTitle: "ok")
                                                self.loading.isHidden = true
                                                self.goback()
                                                
                                            } else if self.type == "1" {
                                                self.checktext()
                                           // self.checkimage()
                                               // self.callapi()
                                            } else if self.type == "2"{
                                                self.checktext()
                                            }
                                            
                                          
                                        }
                                    })
        }
        else if self.type == "1" {
            self.checktext()
        } else if self.type == "2"{
            self.checktext()
        }
}
    
    func checktext(){
        
        if(postInputTextField.text != "")
        {
        
        let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let parameter = ["models": "text","api_user": "406594658", "api_secret": "7x3uRdHtFyaNg4HHd5C7", "lang": "en", "mode": "standard", "text": postInputTextField.text!] as [String : Any]
        
        AF.upload(multipartFormData: { multipartFormData in
                        for (key, value) in parameter {
                            if let temp = value as? String {
                                multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                            }
                        }
                       
                    }, to: "https://api.sightengine.com/1.0/text/check.json",method: .post , headers: header)
                        .responseJSON(completionHandler: { (response) in
                            
                                    if let err = response.error{
                                        
                                        return
                                    }
                            print(response)
                            
                                    let json = response.data
                            
                                    if (json != nil)
                                    {
                                        
                             let jsonObject = JSON(json!)
                                    print(jsonObject)
                             let jsonObjectprofanity = jsonObject["profanity"]
                                    print(jsonObjectprofanity)
                             let jsonObjectmatches = jsonObjectprofanity["matches"]
                                     print(jsonObjectmatches)
                                        
                                    
                               self.matchesarray = jsonObjectmatches.arrayObject! as [Any]
                                 print(self.matchesarray)
                                 if self.matchesarray.count>0 {
                                self.showPromptAlert("Text Alert", message: "Selected Text is against our privacy policies", okTitle: "ok")
                                            self.loading.isHidden = true
                                            self.goback()
                                            
                                        } else if self.type == "1" {
                                            self.checkimage()
                                      
                                        } else if self.type == "2"{
                                            self.checkvideo()
                                        }

                                        
                                      
                                    }
                                })
        }
        else if self.type == "1" {
            self.checkimage()
      
        } else if self.type == "2"{
            self.checkvideo()
        }
        else {
            
        }

    
        
    }
    
    @IBAction func postBtn(_ sender: Any) {
        
    
        captionString = postCaptionLabel.text!
            
            print("ADDING CAPTION WHEN CREATING POST ADDING CAPTION WHEN CREATING POST ADDING CAPTION WHEN CREATING POST ADDING CAPTION WHEN CREATING POST ADDING CAPTION WHEN CREATING POST ================== \(captionString)")
        
        self.loading.isHidden = false
        self.postBtnOutlet.isHidden = true
       
        if self.type == "1" {
            self.checkcaption()
       // self.checkimage()
           // self.callapi()
        }
        if self.type == "2"{
            self.checkcaption()
        }
            
}
    func checkimage(){
        
        let imageData = (postimage.image?.jpegData(compressionQuality: 0.8))!

    let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
    let parameter = ["models": "nudity,wad","api_user": "406594658", "api_secret": "7x3uRdHtFyaNg4HHd5C7"] as [String : Any]
    
    AF.upload(multipartFormData: { multipartFormData in
        for (key, value) in parameter {
            if let temp = value as? String {
                           multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                       }
           
            print("parameter")
           
        }
        
            print("checked")
        if self.type == "1"{
            multipartFormData.append(imageData, withName: "media" , fileName: "file.png", mimeType: "image/png")
        }
                
    }, to: "https://api.sightengine.com/1.0/check.json",method: .post , headers: header)
        .responseJSON(completionHandler: { (response) in
                    print(parameter)
                    print(response)
                    
                    if let err = response.error{
                        print(err)
                        self.postBtnOutlet.isHidden = false
                        return
                    }
                    print("Succesfully uploaded")
                    
            let json = response.data
                    print("1")
            print(json)
            response
            
                    if (json != nil)
                    {
                        let jsonObject = JSON(json!)
                        print(jsonObject)
                        let jsonObjectNudity = jsonObject["nudity"]
                        print(jsonObjectNudity)
                        let raw = jsonObjectNudity["raw"]
                        print(raw)
                        let partial = jsonObjectNudity["partial"]
                        print(partial)
                        let safe = jsonObjectNudity["safe"]
                        print(safe)
                        
                        if raw > safe {
                            
                            self.showPromptAlert("Image Alert", message: "Selected image is against our privacy policies", okTitle: "ok")
                           
                            
                        } else if self.type == "1"{
                            self.callapi()
                        }
                        
                        
                        
                        self.loading.isHidden = true
                        self.goback()
                    }
                })
    
    }
    
    func checkvideo(){
        
      //  let imageData = (postimage.image?.jpegData(compressionQuality: 0.8))!

    let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
    let parameter = ["models": "nudity,wad","api_user": "406594658", "api_secret": "7x3uRdHtFyaNg4HHd5C7"] as [String : Any]
    
    AF.upload(multipartFormData: { multipartFormData in
        for (key, value) in parameter {
            if let temp = value as? String {
                           multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                       }
           
            print("parameter")
           
        }
        
            print("checked")
        if self.type == "2"{
            multipartFormData.append(self.videoURL! as URL, withName: "media", fileName: "video.mp4", mimeType: "video/mp4")
        }
                
    }, to: "https://api.sightengine.com/1.0/video/check-sync.json",method: .post , headers: header)
        .responseJSON(completionHandler: { (response) in
                    print(parameter)
                    print(response)
                    
                               if let err = response.error{
                                print(err)
                                self.postBtnOutlet.isHidden = false
                                 return
                    }
                                   print("Succesfully uploaded")
                    
                                     let json = response.data
                                     print("1")
                                    print(json)
                                     response
            
                    if (json != nil)
                    {
                        let jsonObject = JSON(json!)
                        print("jsonObject")
                        print(jsonObject)
                        let jsonObjectData = jsonObject["data"]
                        print("jsonObjectData")
                        print(jsonObjectData)
                        let jsonObjectframes = jsonObjectData["frames"]
                        print(jsonObjectframes)
                        let jsonObjectNudity = jsonObjectframes[0]["nudity"]
                        
                        var x:Bool = true
                        for var i in (0..<jsonObjectframes.count)
                        {
                            if( jsonObjectframes[i]["nudity"]["raw"] > jsonObjectframes[i]["nudity"]["safe"] )
                            {
                                x = false
                                
                                 self.showPromptAlert("Video Alert", message: "Selected Video is against our privacy policies", okTitle: "ok")
                                break

                            }
                            
                        }
                        if ( x == true )
                        {
                            self.callapi()
                        }

                        print(jsonObjectframes)
                        self.loading.isHidden = true
                        self.goback()
                    }
                })
    
    }

    
    
    
    
    
    func callapi(){
        let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
    if postInputTextField.text ==  "Say something about this post..."{
        postInputTextField.text = ""
    }
    var parameter = ["familyCode": user?.familyCode ?? "", "text": postInputTextField.text! ?? "" , "type": type , "caption" : captionString ?? "", "fdate" : String(self.fdate) ?? ""]
    if(self.fdate == 0)
    {
        parameter = ["familyCode": user?.familyCode ?? "", "text": postInputTextField.text! ?? "", "type": type, "caption" : captionString ?? ""]
    }
    let imageData = (postimage.image?.jpegData(compressionQuality: 0.8))!
    print("q")
    print(imageData as Any)
    
    
                AF.upload(multipartFormData: { multipartFormData in
                    for (key, value) in parameter {
                        if let temp = value as? String {
                                       multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                                   }
                       
                        print("parameter")
                       
                    }
                    
                        print("checked")
                    if self.type == "1"{
                        multipartFormData.append(imageData, withName: "image" , fileName: "file.png", mimeType: "image/png")
                    }
                    if self.type == "2"{
                        multipartFormData.append(self.videoURL! as URL, withName: "video", fileName: "video.mp4", mimeType: "video/mp4")
                    }
                            
                }, to: "http://3.143.193.45:1337/api/createpost",method: .post , headers: header)
                    .responseJSON(completionHandler: { (response) in
                                print(parameter)
                                print(response)
                                
                                if let err = response.error{
                                    print(err)
                                    self.postBtnOutlet.isHidden = false
                                    return
                                }
                                print("Succesfully uploaded")
                                
                                let json = response.data
                                print("1")
                        print(json)
                                if (json != nil)
                                {
                                    let jsonObject = JSON(json!)
                                    print(jsonObject)
                                    self.loading.isHidden = true
                                    self.goback()
                                }
                            })
       

}
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
}
