//
//  CommentsViewController.swift
//  familywithwall
//
//  Created by Apple on 03/05/2021.
//

import UIKit
import SwiftyJSON
import Alamofire
//import IQKeyboardManagerSwift
class CommentsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    var labelll: String = ""
    var user : User?
    var member : User?
    var postId : String = ""
    //var posterId: [String] = ["60938a616095720b1fb7f7c6","azhar@gmail.com"]
    @IBOutlet weak var CommentTabelView: UITableView!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var commentInputTextField: TextFieldWithPadding!
    
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var CommentUserImage: UIImageView!
    @IBOutlet weak var CommentTextField: UITextField!
    
    @IBOutlet weak var commentView: UIView!
    
    
    @IBAction func backClicked(_ sender: Any) {
        
        if labelll == "Notification" {
            
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "NewNotificationStroryboardId") as? NewNotificationViewController else {
                  return
                }
           
            
            menuViewController.postid = postId
            menuViewController.user = self.user
            menuViewController.labelll = labelll
            menuViewController.modalPresentationStyle = .fullScreen
            
             
             self.present(menuViewController, animated: true, completion: nil)
            
        }
        
        else if labelll == "backtoMembersProfile"{
            gobacktoProfile()
        }else{
            gobacktoAlbum()
            
        }
    }
    func gobacktoProfile(){
        print("Clicked")
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "BelongingStoryboardId")  as? BelongingViewController else {
            return
        }
        
       
        menuViewController.member = member
        menuViewController.modalPresentationStyle = .fullScreen
        self.present(menuViewController, animated: true,completion: nil)
        print("Clicked")
    }
    func gobacktoAlbum(){
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "WallPostStoryboardId") as? WallPostViewController else {
              return
            }
        
        menuViewController.labelll = labelll
            menuViewController.modalPresentationStyle = .overFullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
    }
    var comments: [Comments] = []
    //    CommentsObject(username: "Wind Ranger", comment: "Nice Pic", userimage: "windranger"),CommentsObject(username: "Drow Ranger", comment: "Cute", userimage: "drowranger")
    override func viewWillDisappear(_ animated: Bool) {
        //IQKeyboardManager.shared.enable = true
      //IQKeyboardManager.shared.shouldResignOnTouchOutside = true
   }

    override func viewDidLoad() {
        super.viewDidLoad()
        //IQKeyboardManager.shared.enable = false
      //IQKeyboardManager.shared.shouldResignOnTouchOutside = false

        CommentTabelView.delegate = self
        CommentTabelView.dataSource = self
        self.loading.isHidden = false
        self.loading.startAnimating()
        commentView.layer.cornerRadius = commentView.frame.size.height/2
        commentView.layer.borderColor = UIColor.lightGray.cgColor
        commentView.layer.borderWidth = 1.0
        commentView.layer.masksToBounds = true
      
        getComments()
        
        

        CommentUserImage.layer.cornerRadius = CommentUserImage.frame.size.width / 2
        CommentUserImage.clipsToBounds = true
      
        
        
    }

    
    @IBAction func ADDcommentBtn(_ sender: Any) {
        self.loading.isHidden = false
        addComment()
        CommentTextField.resignFirstResponder()
    }
    
    func addComment(){
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        

        let like = ApiCallAddComment.Request(comment: CommentTextField.text!, postid: postId)
                let requestService = SCService<ApiCallAddComment.Request,ApiCallAddComment.Response>()
                requestService.request("http://3.143.193.45:1337/api/comment", method: .post, parameters: like, encoder: JSONParameterEncoder(), header: header){
                    (result) in
        
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        if response.status == true{
                        self.CommentTextField.text?.removeAll()
                        self.getComments()
                        }else{
                            self.showPromptAlert("Sorry!", message: response.message);
                        }
                        self.loading.isHidden = true
                        //self.performSegue(withIdentifier: "MainNav", sender: self)
                    case .failure(let error):
                        debugPrint(error)
                       
                        print(error)
        
                    }
        
        
                }
    }
    
    
    

    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    func getComments(){
        self.loading.isHidden = false
     
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        
        let comment = ApiCallGetComments.Request(pid: postId)
                let requestService = SCService<ApiCallGetComments.Request,ApiCallGetComments.Response>()
                requestService.request("http://3.143.193.45:1337/api/getcomments", method: .post, parameters: comment, encoder: JSONParameterEncoder(), header: header){
                    (result) in
                    
                    switch result {
                    case .success(let response):
                        debugPrint(response)
                        print(response.status)
                        print(response.message)
                        print(response.data)
                        if response.status == true{
                            self.comments = response.data!
                        self.CommentTabelView.reloadData()
                        }else{
                            self.showPromptAlert("Sorry!", message: response.message);
                        }
                        self.loading.isHidden = true
                        //self.performSegue(withIdentifier: "MainNav", sender: self)
                    case .failure(let error):
                        debugPrint(error)
                       
                        print(error)
        
                    }
        
        
                }
    }
    
    
    
    
    
    
    
    func setupUI() {
       let imgSearch = UIImageView();

        imgSearch.image = UIImage(named: "Send button");
        
       // set frame on image before adding it to the uitextfield
       
       imgSearch.frame = CGRect(x: CommentTextField.frame.size.width - 40 , y: 5, width: 22, height: 22)
        
        CommentTextField.rightView = imgSearch
    
        CommentTextField.rightViewMode = .always
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = CommentTabelView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentsCell
        cell.commentid = comments[indexPath.row].id
        cell.checkOptionBtn(PosterId: user!.id, CommenterId: comments[indexPath.row].commenterid)
        cell.delegate = self
        let attributsBold = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 12, weight: .regular)]
            let attributsNormal = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18, weight: .regular)]
        let attributedString = NSMutableAttributedString(string: comments[indexPath.row].commentername, attributes:attributsNormal)
        let boldStringPart = NSMutableAttributedString(string:  " \(comments[indexPath.row].comment)", attributes:attributsBold)
            attributedString.append(boldStringPart)
        
        if comments[indexPath.row].commenterprofilepic != ""{
            
            let url = URL(string: comments[indexPath.row].commenterprofilepic)
            let data = try? Data(contentsOf: url!)

            if let imageData = data {
                cell.userimage.image = UIImage(data: imageData)
            }
            
        }
     
        
        
        
        cell.commentLabel.attributedText = attributedString
        return cell
    }


}




class TextFieldWithPadding: UITextField {
    var textPadding = UIEdgeInsets(
        top: 0,
        left: 5,
        bottom: 0,
        right: 30
    )

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.textRect(forBounds: bounds)
        return rect.inset(by: textPadding)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.editingRect(forBounds: bounds)
        return rect.inset(by: textPadding)
    }
}
extension CommentsViewController : DataForCell2{
    func buttonTap(commentId : String){
    print("\(commentId)")
        postOptionAction( var: "Delete",cid: commentId)
  }
    func postOptionAction( `var` str2:String, cid: String) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        alert.addAction(UIAlertAction(title: "Delete", style: UIAlertAction.Style.default) { (UIAlertAction) in
            print(cid)
            self.loading.isHidden = false
            self.deleteComment(commentid: cid)
        })
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        
        
        self.present(alert, animated: true, completion: nil)
        
      }
    func deleteComment(commentid: String){
                //let header = ["authorization":"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwYTRhYjEzNWIwZjFmNTlmNzUzYjVlOSIsImlhdCI6MTYyMTQwNDQzNSwiZXhwIjoxNjIzOTk2NDM1fQ.Y3gw3jQ8Ll48eoYraNKZNU4zQv3Nqcizoixvm2gHLg0"]
                let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
                let comment = ApiCallDeleteComment.Request(cid: commentid)
                        let requestService = SCService<ApiCallDeleteComment.Request,ApiCallDeleteComment.Response>()
                        requestService.request("http://3.143.193.45:1337/api/deletecomment", method: .post, parameters: comment, encoder: JSONParameterEncoder(), header: header){
                            (result) in
        
                            switch result {
                            case .success(let response):
                                debugPrint(response)
                                print(response.status)
                                print(response.message)
                                if response.status == true{
                                self.getComments()
                                }else{
                                    self.showPromptAlert("Sorry!", message: response.message);
                                }
                                self.loading.isHidden = false
                                //self.performSegue(withIdentifier: "MainNav", sender: self)
                            case .failure(let error):
                                debugPrint(error)
                               
                                print(error)
        
                            }
        
        
                        }
    }
}
