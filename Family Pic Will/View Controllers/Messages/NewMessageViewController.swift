//
//  NewMessageViewController.swift
//  Family Pic Will
//
//  Created by Apple on 20/05/2021.
//

import UIKit
import Firebase
import Alamofire
import SDWebImage


class NewMessageViewController: UIViewController,UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate {
    
    
    var members: [MessageModel] = []
    var member: [User] = []
    var user: User?
    var membersDisplay: [User] = []
    var memberProfile: User?
    var display: [User?] = []
    var filteredMemberName : [User] = []
    var a = 2
    var name = ""
    var image = ""
    
    
    @IBOutlet weak var NewGroupButton: UIButton!
    @IBOutlet weak var titleBarLabel: UILabel!
    @IBAction func backTap(_ sender: Any) {
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "MessageStoryboardId") as? MessageViewController else {
            return
        }
        let presentingVC = self.presentingViewController
        menuViewController.modalPresentationStyle = .fullScreen
        menuViewController.user = self.user
        
        dismiss(animated: true) {
            presentingVC!.present(menuViewController, animated: true,completion: nil)
        }    }
    
    @IBOutlet weak var Searchbar: UISearchBar!
    @IBOutlet weak var suggestedLabel: UILabel!
    @IBOutlet weak var suggestedUserTableView: UITableView!
    @IBOutlet weak var viewCreateANewGroup: UIView!
    @IBOutlet weak var createANewGroupLabel: UILabel!
    @IBAction func NewGroupButton(_ sender: Any) {
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "NewGroupStoryboardId")
        as? NewGroupViewController else {
            return
        }
        let presentingVC = self.presentingViewController
        menuViewController.modalPresentationStyle = .fullScreen
        menuViewController.user = self.user
        menuViewController.a = self.a
        menuViewController.member = self.member
        dismiss(animated: true) {
            presentingVC!.present(menuViewController, animated: true,completion: nil)
        }

        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        Searchbar.delegate = self
        familyMemberApi()
        titleBarLabel.font = UIFont(name: "Roboto-Regular", size: 20)
        //TotextField.font = UIFont(name: "Roboto-Light", size: 17)
        
        createANewGroupLabel.font = UIFont(name: "Roboto-Medium", size: 17)
        viewCreateANewGroup.layer.borderWidth = 1
        viewCreateANewGroup.layer.borderColor = UIColor(red: 235/255, green: 235/255, blue: 235/255,alpha: 1).cgColor
        suggestedUserTableView.dataSource = self
        suggestedUserTableView.delegate = self
        suggestedUserTableView.separatorStyle = .none
        suggestedUserTableView.showsVerticalScrollIndicator = false
        
        suggestedLabel.font = UIFont(name: "Montserrat-SemiBold", size: 17)
        // Do any additional setup after loading the view.
        fetchUser()
    }
    func fetchUser(){
        FirebaseDatabase.Database.database().reference().child("Friends").observe(.childAdded) { (snapshot) in
            
            if let dictionry = snapshot.value as? [String: AnyObject] {
                let member = self.members
                self.members.append(contentsOf: member)
                self.suggestedUserTableView.reloadData()
        
            }
            
            print(snapshot)
        }
        
    }
    func familyMemberApi(){
    let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallProfile.Request,ApiCallProfile.Response>()
        requestService.request("http://3.143.193.45:1337/api/profile", method: .post, parameters: nil, encoder: JSONParameterEncoder(), header: header){(result) in
          switch result{
          case .success(let response):
            if response.status == true{
            debugPrint(response)
              self.user = response.data?.user
              self.member = response.data!.members
                let x = self.member.count - 1
              for index in 0...x
              {
                if(self.member[index].blocked == 0 && self.member[index].deceased == 0 )
                {
                  self.membersDisplay.append(self.member[index])
                    self.filteredMemberName.append(self.member[index])
                    
                }
              }
              self.member = self.membersDisplay
//                self.image = self.memberProfile?.profilepic ?? ""
//                print("-=-=-=--=\(self.image)")
//                self.name = self.memberProfile?.profilepic ?? ""
//                print("-=-=-=--=\(self.name)")
              self.suggestedUserTableView.reloadData()

            do {
              let encoder = JSONEncoder()
              let data = try encoder.encode(response.data!.user)
              UserDefaults.standard.setValue(data, forKey: "USER")
            } catch{
              print("Error")
            }

            }else{
              self.showPromptAlert("Sorry!", message: response.message);
            }
          case .failure(let error):
            debugPrint(error)
          }
        }
      }

    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredMemberName = []
        
        if searchText == ""{
            filteredMemberName = member
        }
        else {
            for names in member {
                if names.name.lowercased().contains(searchText.lowercased()){
                    filteredMemberName.append(names)
                }
            }
        }
        self.suggestedUserTableView.reloadData()
        
    }

 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("aaaaaaabb\(member.count)")
        //return member.count
        return filteredMemberName.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SuggestedUserTableViewCell
        cell.userName.text = filteredMemberName[indexPath.row].name
        cell.userImage.sd_setImage(with: URL(string: filteredMemberName[indexPath.row].profilepic ), placeholderImage: UIImage(named: "userprofile.png"))
//        self.image = member[indexPath.row].profilepic
//        print("-=-=-=--=\(image)")
//        self.name = filteredMemberName[indexPath.row].name
//        print("-=-=-=--=\(name)")
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "Chatstoryboardid") as? ChatViewController else {
            return
        }
        let presentingVC = self.presentingViewController
        menuViewController.modalPresentationStyle = .fullScreen
        menuViewController.type = "1"
        menuViewController.receiver = member[indexPath.row].id
        let smember = [member[indexPath.row]]
        menuViewController.member = smember
        menuViewController.membername = member[indexPath.row].name
        print("-=-=-=-=-\(self.name)")
        menuViewController.memberimage = member[indexPath.row].profilepic
        print("-=-=-=-=-\(self.image)")
        dismiss(animated: true) {
            presentingVC!.present(menuViewController, animated: true,completion: nil)
        }

    }
}
    
    

