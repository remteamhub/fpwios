//
//  NewGroupViewController.swift
//  Family Pic Will
//
//  Created by mac on 9/9/21.
//

import UIKit
import Alamofire

class NewGroupViewController: UIViewController, UITextFieldDelegate,UISearchBarDelegate {
    var user: User?
    var member: [User] = []
    var sendMembers: [User] = []
    var membersDisplay: [User] = []
    var ids = [String]()
    var filteredMemberName : [User] = []
    var a=0

    
    
    private var allCells = Set<NewGroupTableViewCell>()

    
    @IBOutlet weak var Searchbar: UISearchBar!
    @IBOutlet weak var NextButton: UIButton!
    @IBAction func backbutton(_ sender: Any) {
        if a == 2{
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "NewMessageStoryboardId") as? NewMessageViewController else {
                return
            }
            menuViewController.user = self.user
            
            menuViewController.modalPresentationStyle = .fullScreen
            self.present(menuViewController, animated: true, completion: nil)
        } else if a == 1{
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "MessageStoryboardId") as? MessageViewController else {
            return
        }
        menuViewController.user = self.user
        
        menuViewController.modalPresentationStyle = .fullScreen
        self.present(menuViewController, animated: true, completion: nil)
        }
    }
    @IBAction func NextButton(_ sender: Any) {
        
        
        print("COUNT")
        
        print(self.allCells.count)
        print(self.sendMembers.count)
        print(self.ids)
        if a  == 1 {
            
            guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "MessageStoryboardId") as? MessageViewController else {
                return
            }
            menuViewController.ids = self.ids
            menuViewController.modalPresentationStyle = .fullScreen
            self.present(menuViewController, animated: true, completion: nil)
            
            
        } else{
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "CreateGroupStoryboardId") as? CreateGroupViewController else {
            return
        }
        menuViewController.sendMembers = self.sendMembers
        menuViewController.ids = self.ids
            menuViewController.a = self.a
        menuViewController.modalPresentationStyle = .fullScreen
        self.present(menuViewController, animated: true, completion: nil)
        }
        
    }
    @IBOutlet weak var TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        TableView.delegate = self
        TableView.dataSource = self
        TableView.allowsMultipleSelection = true
        TableView.allowsMultipleSelectionDuringEditing = true
        Searchbar.delegate = self
        familyMemberApi()
        self.filteredMemberName = member
       

        // Do any additional setup after loading the view.
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredMemberName = []
        
        if searchText == ""{
            filteredMemberName = member
        }
        else {
            for names in member {
                if names.name.lowercased().contains(searchText.lowercased()){
                    filteredMemberName.append(names)
                }
            }
        }
        self.TableView.reloadData()
        
    }
    
    
}
extension NewGroupViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        print("asdf\(filteredMemberName)")
        return self.filteredMemberName.count
    
    }
    
    
    func familyMemberApi(){
    let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallProfile.Request,ApiCallProfile.Response>()
        requestService.request("http://3.143.193.45:1337/api/profile", method: .post, parameters: nil, encoder: JSONParameterEncoder(), header: header){(result) in
          switch result{
          case .success(let response):
            if response.status == true{
            debugPrint(response)
              self.user = response.data?.user
              self.member = response.data!.members
                let x = self.member.count - 1
              for index in 0...x
              {
                if(self.member[index].blocked == 0 && self.member[index].deceased == 0 )
                {
                  self.membersDisplay.append(self.member[index])
                    self.filteredMemberName.append(self.member[index])
                }
              }
              self.member = self.membersDisplay
            self.TableView.reloadData()

            do {
              let encoder = JSONEncoder()
              let data = try encoder.encode(response.data!.user)
              UserDefaults.standard.setValue(data, forKey: "USER")
            } catch{
              print("Error")
            }

            }else{
              self.showPromptAlert("Sorry!", message: response.message);
            }
          case .failure(let error):
            debugPrint(error)
          }
        }
      }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
         let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewGroupTableViewCell
        cell.memberName.text = filteredMemberName[indexPath.row].name
       
        cell.memberImage.sd_setImage(with: URL(string: filteredMemberName[indexPath.row].profilepic ), placeholderImage: UIImage(named: "userprofile.png"))
        cell.member = filteredMemberName[indexPath.row]
        cell.radio.isHidden = true
        cell.check.isHidden = true
        
        
       
        cell.returnValue = { value in
                    if( value == true)
                    {
                    
                        tableView.cellForRow(at: indexPath)?.setHighlighted(true, animated: true)
                        self.sendMembers.append(self.member[indexPath.row])
                        self.ids.append(self.member[indexPath.row].id)

                    }
                    else{
                        if(self.ids.contains(self.member[indexPath.row].id))
                        {
                      
                            let index = self.ids.index(of: self.member[indexPath.row].id)

                            self.sendMembers.remove(at: index ?? -1)
                            self.ids.remove(at: index ?? -1)

                        }
                    }

                    print(self.member[indexPath.row].name)
                    print(value)
                }
        return cell
    }
}
