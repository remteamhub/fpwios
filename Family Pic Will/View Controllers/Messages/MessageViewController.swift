//
//  MessageViewController.swift
//  Family Pic Will
//
//  Created by Apple on 20/05/2021.
//

import UIKit
import FirebaseDatabase
import Firebase

class MessageViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate  {
    var x=0
    var a=1
    var ids = [String]()
    var user: User?
    var type : String = ""
    var message : [MessageModel] = []
    var delegate : RefereshProfile?
    let transition = SlideInTransition()
    var member: [User] = []
    var chat = [chatModel]()
    var mess = [String:Any]()
    var memberImage: String = ""
    var memberName: String = ""
    var currenttime = Int64(Date().timeIntervalSince1970 * 1000)
   // var ref:FIRDatabaseReference?
    @IBOutlet weak var messagetableview: UITableView!
    @IBOutlet weak var titleBarLabel: UILabel!
    @IBAction func NewMessageClicked(_ sender: Any) {
        print("Maa ki kuss")
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "NewMessageStoryboardId")  as? NewMessageViewController else {
            return
        }
        menuViewController.member = self.member
        menuViewController.name = self.memberName
        menuViewController.image = self.memberImage
        menuViewController.modalPresentationStyle = .fullScreen
        self.present(menuViewController, animated: true,completion: nil)
    }
    func messages(){
         FirebaseDatabase.Database.database().reference().child("Friends").child(UserDefaults.standard.string(forKey: "uid") ?? "").observe(DataEventType.value, with: { snapshot in
            //get list id's
            if snapshot.exists(){
                if let result = snapshot.children.allObjects as? [DataSnapshot]{
                    for child in result{
                        let y = chatModel(idReceiver: "", idSender: "", isFutrue: true, status: "", text: "", timestamp: 123213123123123123, type: "")
                        let value = snapshot.value as? [String: Any]
                        let x=MessageModel(email: value?["email"] as? String ?? "", id: value?["email"] as? String ?? "", avata: value?["email"] as? String ?? "", message: y)
                        print(x)
                        self.message.append(x)
                        
                    }
                }
            }
          })
    }
    
  
    
    @IBAction func openSideMenuClicked(_ sender: Any) {
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "SideMenuStoryboardId") else {
              return
            }
            menuViewController.modalPresentationStyle = .overCurrentContext
            menuViewController.transitioningDelegate = self
            present(menuViewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchUser()
      //  FirebaseApp.configure()
        
        
        
        messagetableview.delegate = self
        messagetableview.showsVerticalScrollIndicator = false
        titleBarLabel.font = UIFont(name: "Roboto-Regular", size: 20)
        // Do any additional setup after loading the view.
        transition.delegate = self
    }
    
    func fetchUser(){
        
        let use = UserDefaults.standard.string(forKey: "uid")
      
        FirebaseDatabase.Database.database().reference().child("Friends").child(use ?? "").observe(.childAdded) { (snapshot) in
            self.ids.append(snapshot.key)
  
            var dic = [String:Any]()
            for child in snapshot.children {
                let snap = child as! DataSnapshot
                     let key = snap.key
                     let value = snap.value
                dic[snap.key] = snap.value
                     print("key = \(key)  value = \(value!)")
            
            }
            self.mess[snapshot.key]=dic
            
                self.messagetableview.reloadData()
                
        }
        
    }
    
}

extension MessageViewController : UIViewControllerTransitioningDelegate{
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = true
    return transition
  }
    
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = false
    return transition
  }
    
}

extension MessageViewController : CloseSideMenu{
    func closeSideMenu(){
        dismiss(animated: true, completion: nil)
    }
}

extension MessageViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ids.count
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
         let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! MessageMemberTableViewCell
        

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dropDownAction))
        cell.dropDown.isUserInteractionEnabled = true
        cell.dropDown.addGestureRecognizer(gestureRecognizer)
        
        let dic = self.mess[ids[indexPath.row]] as! NSDictionary
        let message = dic["message"] as! NSDictionary
        print(message["text"] as! String)
//        if (self.currenttime >= message["timestamp"] as! Int64 ) {
        cell.messageLabel.text = ""
        cell.nameLabel.text = dic["name"] as? String
        cell.memberImage.sd_setImage(with: URL(string: dic["avata"] as! String ), placeholderImage: UIImage(named: "userprofile.png"))
            self.memberName = dic["name"] as! String
            self.memberImage =  dic["avata"] as! String
//        }
        let keyExists = dic["type"] != nil
        if( keyExists == true)
        {
            let type = dic["type"] as! String
            if( type == "0"){
                if(dic["id"] as? String !=  UserDefaults.standard.string(forKey: "uid"))
            {
                    cell.memberImage.image = UIImage(named: "group3")
                cell.dropdown.isHidden = true
            }
                
            }
            if( type == "1")
            {
                cell.memberImage.image = UIImage(named: "group3")
                cell.dropdown.isHidden = true
            }
        }
        else
        {
            cell.dropdown.isHidden = true
        }

        return cell
    }
    @objc func dropDownAction(){
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "NewGroupStoryboardId")  as? NewGroupViewController else {
            return
        }
        menuViewController.member = self.member
        menuViewController.user = self.user
        menuViewController.a = self.a
        menuViewController.modalPresentationStyle = .fullScreen
        self.present(menuViewController, animated: true,completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
        tableView.deselectRow(at: indexPath, animated: true)
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "Chatstoryboardid") as? ChatViewController else {
              return
            }
        
        let dic = self.mess[ids[indexPath.row]] as! NSDictionary
        let keyExists = dic["type"] != nil
        if(keyExists == true)
        {
          menuViewController.type = dic["type"] as! String
            self.memberName = dic["name"] as! String
            self.memberImage = dic["avata"] as! String
            menuViewController.membername = self.memberName
            menuViewController.memberimage = self.memberImage
            menuViewController.isGroup = "isGroup"
        }
        else
        {
            menuViewController.type = "1"
        }
            menuViewController.receiver = self.ids[indexPath.row]
        self.memberName = dic["name"] as! String
        self.memberImage = dic["avata"] as! String
        menuViewController.membername = self.memberName
        menuViewController.memberimage = self.memberImage
        menuViewController.modalPresentationStyle = .fullScreen
         self.present(menuViewController, animated: true, completion: nil)

    }
        
}
extension MessageViewController : DataForCell {
    func buttonTap(title: Int) {
        print("\(title)")
        //selectedMember = title
    }
}
