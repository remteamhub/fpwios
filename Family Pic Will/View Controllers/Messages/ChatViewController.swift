//
//  ChatViewController.swift
//  Family Pic Will
//
//  Created by mac on 8/11/21.
//

import UIKit
import Firebase
import RealmSwift
import AVKit
import AVFoundation
import FirebaseStorage
import MobileCoreServices
import UniformTypeIdentifiers
import SwiftyJSON
import Alamofire

class ChatViewController: UIViewController, UITextViewDelegate, UIImagePickerControllerDelegate & UINavigationControllerDelegate  {
    var isGroup = ""
    var membername = ""
    var user : User?
    var chatList = [chatModel]()
    var members = [MessageModel]()
    var member: [User] = []
    var receiver: String = ""
    var mess = [String:Any]()
    var ids = [String]()
    var type: String = ""
    var memberimage = ""
    var indexes = [Int:String]()
    var counter = 0//group or single chat
    var uploadtype: String = ""
    var imagePicker = UIImagePickerController()
    //var documentPicker = UIDocumentPickerController()
    var fileUrl: URL?
    var imageUrl: String = ""
    var videoUrl: String = ""
    var pdfUrl: String = ""
    var fdate = Int64(Date().timeIntervalSince1970 * 1000)
    var isfuture = false
    var albumname = "fromchat"
    var currenttime = Int64(Date().timeIntervalSince1970 * 1000)
    let cellSpacingHeight: CGFloat = 2
    var urls: URL?
    var uploadfileurl: String = ""
    var uploadfiletype: String = ""
    var myimage: String = ""
    var myname: String = ""
    var myemail: String = ""
    var imagedata: UIImageView!
    var matchesarray : [Any] = []
    
    @IBOutlet weak var loadingSign: UIActivityIndicatorView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var attachmentBtn: UIButton!
    @IBOutlet weak var callenderBtn: UIButton!
    @IBOutlet weak var ifBlockedLabel: UILabel!
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var memberImage: UIImageView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var selectedimageView: UIImageView!
    @IBOutlet weak var ChatTextView: UITextView! = {
        var  ChatTextView = UITextView()
        ChatTextView.translatesAutoresizingMaskIntoConstraints = false
        return ChatTextView
        }()
    @IBOutlet weak var memberName: UILabel!
    
//    override func viewWillAppear(_ animated: Bool) {
//        self.ChatTextView.text = nil
//    }
//    @IBOutlet weak var playbutton: UIButton!
    
  override func viewDidLoad() {
       super.viewDidLoad()
//      profileApi()
      self.ChatTextView.text = ""
      //self.chatTableView.reloadData()
      selectedimageView.isHidden = true
      cancelButton.isHidden = true
    self.loadingSign.isHidden = false
    self.loadingSign.startAnimating()
      self.memberImage.sd_setImage(with: URL(string: memberimage ), placeholderImage: UIImage(named: "userprofile.png"))
      memberImage.layer.cornerRadius = memberImage.frame.height / 2
      memberImage.clipsToBounds = true
      self.memberName.text = self.membername
      self.chatTableView.setContentOffset(CGPoint(x: 0, y: CGFloat.greatestFiniteMagnitude), animated: false)
      //self.loadingSign.isHidden = true
      ifBlockedLabel.isHidden = true
      //canMessageApi()
      ChatTextView.delegate = self
      chatTableView.delegate = self
      chatTableView.dataSource = self
      chatTableView.showsVerticalScrollIndicator = false
      getmessages()
//      var use = UserDefaults.standard.string(forKey: "uid") ?? ""
//
//      var currentChat = ""
//      if(self.type == "0"){
//      currentChat = "\(self.receiver)"
//      }
//      else
//      {
//          currentChat = "\(use)_\(self.receiver)"
//      }
//
//      print("CURRENT CHAT")
//      print(currentChat)
//      FirebaseDatabase.Database.database().reference().child("Messages").child(currentChat).observe(.childAdded) { (snapshot) in
////            self.loadingSign.isHidden = false
////            self.loadingSign.startAnimating()
//          self.ids.append(snapshot.key)
//          print(snapshot.key)
//          print("salmansohail\(snapshot.children)")
//          var dic = [String:Any]()
//          for child in snapshot.children {
//              let snap = child as! DataSnapshot
//              print("Snap is \(snap)")
//              let key = snap.key
//              print("Key is \(key)")
//              let value = snap.value
//              print("value is \(value)")
//              dic[snap.key] = snap.value
//
//              print("key = \(key)  value = \(value!)")
//          }
////            self.loadingSign.stopAnimating()
////            self.loadingSign.isHidden = true
//          self.currenttime = Int64(Date().timeIntervalSince1970 * 1000)
//          if(self.currenttime >= dic["timestamp"] as! Int64 ){
//              self.mess[snapshot.key] = dic
//              print("mess is \(self.mess[snapshot.key])")
//              self.indexes[self.counter] = snapshot.key
//              self.counter = self.counter + 1
//              print(self.mess)
//              self.chatTableView.reloadData()
//              self.scrollToBottom()
//
//          }
//
//      }
//      self.loadingSign.stopAnimating()
//      self.loadingSign.isHidden = true
//      print("MESSSSSS")
//      print(self.mess)

    canMessageApi()
      if isGroup == "isGroup"{
          callenderBtn.isHidden = true
      }
      self.chatTableView.register(UINib(nibName: "ReciverTableViewCell", bundle: nil), forCellReuseIdentifier: "ReciverTableViewCell")
      self.chatTableView.register(UINib(nibName: "TableViewCell", bundle: nil),forCellReuseIdentifier: "TableViewCell")
    }
     
    
//    @IBAction func playbtnreceivertableviewcell(_ sender: Any) {
//        cellVideoTapped()
//
//    }
//    @IBAction func playbtntableviewcell(_ sender: Any) {
//        cellVideoTapped()
//    }
    
    @IBAction func calenderButton(_ sender: Any) {
        if UserDefaults.standard.value(forKey: "fcount") as! Int > 0{
        RPicker.selectDate(title: "Select Date & Time", cancelText: "Cancel", datePickerMode: .dateAndTime, minDate: Date(), maxDate: Date().dateByAddingYears(10000), style: .Inline, didSelectDate: {[weak self] (selectedDate) in
            let since1970 = selectedDate.timeIntervalSince1970.rounded()
                  print("aaaab\(since1970)")
                  let inmilisec = since1970 * 1000
                  print("aaaab\(inmilisec)")
                  self?.fdate = Int64(inmilisec)
            self?.isfuture = true
            })
        }else{
            self.showPromptAlert("Sorry", message: "You do not have any future messages left")
        }
    }
    
    
    func upload(url: URL?)
    {
        let storage = Storage.storage()
        var fileName = self.urls?.lastPathComponent
        var fileType = self.urls?.pathExtension
        fileType = "mp4"
        let metadata = StorageMetadata()
        if(self.uploadfiletype == "video")
        {metadata.contentType = "video/mp4"
            fileType = "mp4"
        }
        else if(self.uploadfiletype == "image")
        {metadata.contentType = "image/jpeg"}
        else
        {metadata.contentType = "application/pdf"
            fileType = "pdf"
        }

        //convert video url to data
        
      
       //FirebaseStorage.storage()
       let currentDate = Date()
       let since1970 = currentDate.timeIntervalSince1970
       let currentTime = Int(since1970 * 1000)
       let child = "uploads/\(currentTime).\(fileType!)"
       let storageRef = storage.reference().child(child)
        if let videoData = NSData(contentsOf: self.urls!) as Data? {
            //use 'putData' instead
            self.loadingSign.isHidden = false
            self.loadingSign.startAnimating()
            
            let uploadTask = storageRef.putData(videoData, metadata: metadata)
            uploadTask.observe(.success) { snapshot in
              // Upload completed successfully
                storageRef.downloadURL { (url, error) in
                    if let error = error  {
                              print("Error on getting download url: \(error.localizedDescription)")
                              return
                            }
                            print("Download url of \(child) is \(url!.absoluteString)")
                    self.uploadfileurl = url!.absoluteString
                    self.loadingSign.isHidden = true
                    self.handleSend1()
                          }
                print("success")
                
            }
           
            uploadTask.observe(.failure) { snapshot in
              if let error = snapshot.error as? NSError {
                switch (StorageErrorCode(rawValue: error.code)!) {
                case .objectNotFound:
                  // File doesn't exist
                    print("file not found")
                  break
                case .unauthorized:
                    print("unauthorized")
                  // User doesn't have permission to access file
                  break
                case .cancelled:
                    print("cancled")
                  // User canceled the upload
                  break

                /* ... */

                case .unknown:
                    print("unknown")
                  // Unknown error occurred, inspect the server response
                  break
                default:
                    print("lol")
                  // A separate error occurred. This is a good place to retry the upload.
                  break
                }
              }
            }
        }else {
            do{
                       let imgName = UUID().uuidString
                                               let documentDirectory = NSTemporaryDirectory()
                                               let localPath = documentDirectory.appending(imgName)
                let documentData = try Data(contentsOf: self.urls!, options: .dataReadingMapped)
           
                       let data = documentData as NSData
                       //.jpegData(compressionQuality: 0.75)! as NSData
                                      data.write(toFile: localPath, atomically: true)
                       let photoURL = URL.init(fileURLWithPath: localPath)
                                           self.urls = photoURL
                
                
                   }catch let error {
                               print(error)
                   }

        }

    }
    
    @IBAction func AttachmentButton(_ sender: Any) {
        self.uploadtype = "1"
        wishBtnAction(var: "Camera", var: "Gallery", var: "Files")
    }
    
    func wishBtnAction(`var` str1:String, `var` str2:String, `var` str3:String) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: str1, style: .default, handler: { _ in
          self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: str2, style: .default, handler: { _ in
          self.openGallary()
        }))
        alert.addAction(UIAlertAction(title: str3, style: .default, handler: { _ in
          self.openFiles()
        }))

        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
       }
        func openCamera(){
          if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
              //If you dont want to edit the photo then you can set allowsEditing to false
              imagePicker.mediaTypes = ["public.image","public.movie"]
              
              imagePicker.allowsEditing = false
              imagePicker.delegate = self
            self.present(imagePicker, animated: true,completion: nil)
            }
            else{
              let alert = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
              alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
              self.present(alert, animated: true, completion: nil)
            }
          }
        //MARK: - Choose image from camera roll
        func openGallary(){
          imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
          //If you dont want to edit the photo then you can set allowsEditing to false
            imagePicker.mediaTypes = ["public.image","public.movie"]
          imagePicker.allowsEditing = true
          imagePicker.delegate = self
          self.present(imagePicker, animated: true,completion: nil)
        }
    
    //MARK: - Open Files
    func openFiles(){
        
        
        let types = [kUTTypePDF]
            let importMenu = UIDocumentPickerViewController(documentTypes: types as [String], in: .import)
            importMenu.delegate = self
            importMenu.modalPresentationStyle = .formSheet
            present(importMenu, animated: true, completion: nil)
    }
    

    //MARK: - Image Picker
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.urls = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.mediaURL.rawValue)] as? URL
      
        var fileName = self.urls?.lastPathComponent
        var fileType = self.urls?.pathExtension
        
        if self.urls != nil{
        self.getThumbnailImageFromVideoUrl(url: self.urls!){ (thumbnailImage) in
              self.selectedimageView.image = thumbnailImage
              self.selectedimageView.isHidden = false
            self.cancelButton.isHidden = false
            }
        }
        self.uploadfiletype = "video"
        if(fileType == nil)
        {
            //imagedata.image = (info[UIImagePickerController.InfoKey.editedImage] as? UIImage)!
            self.urls = info[UIImagePickerController.InfoKey.imageURL] as? URL
            fileType = self.urls?.pathExtension
            self.uploadfiletype = "image"
            let selectedimage = info[UIImagePickerController.InfoKey.imageURL]
            selectedimageView.isHidden = false
            cancelButton.isHidden = false
            selectedimageView.sd_setImage(with: selectedimage as? URL, completed: nil)
            print(info)
            
            if(self.urls == nil)
            {
                var selectedImage: UIImage!
                if (picker.sourceType == UIImagePickerController.SourceType.camera) {

                        let imgName = UUID().uuidString
                        let documentDirectory = NSTemporaryDirectory()
                        let localPath = documentDirectory.appending(imgName)
                    selectedImage = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as! UIImage
                    print(selectedImage)
                    selectedImage = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)] as! UIImage
                    print(info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.originalImage.rawValue)])
                     //    selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
                
                    var data = selectedImage.jpegData(compressionQuality: 0.75)! as NSData
                    print("=============\(data)")
                    data.write(toFile: localPath, atomically: true)
                        let photoURL = URL.init(fileURLWithPath: localPath)
                    self.urls = photoURL
                    print(photoURL)
                    selectedimageView.sd_setImage(with: photoURL as? URL, completed: nil)
                    }
            }
        }
            
//        upload(url: self.urls)
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func cancelButton(_ sender: Any) {
        self.selectedimageView.sd_setImage(with: URL(string: ""))
        self.selectedimageView.isHidden = true
        self.cancelButton.isHidden = true
    }
    
    //MARK: - Send button
    
    @IBAction func sendButton(_ sender: Any) {
        if (ChatTextView.text == "") && (selectedimageView.image == UIImage(named: "")){

            showPromptAlert("Enter text", message: "Please enter text ")
            
            if  (   self.uploadfiletype == "image"   )
            {
                checkimage()
            }
            else if ( self.uploadfiletype == "video")
            {
                checkvideo()
            }
            else
            {
                
            }
            
        }else if (ChatTextView.text != "")   {
            checktext()
            
        }else if (selectedimageView != nil) {
            if  (   self.uploadfiletype == "image"   )
            {
                checkimage()
            }
            else if ( self.uploadfiletype == "video")
            {
                checkvideo()
            }
            else
            {
                
            }        }
        
        self.selectedimageView.sd_setImage(with: URL(string: ""))
        self.selectedimageView.isHidden = true
        self.cancelButton.isHidden = true
    }
    
    //MARK: - Back Button
    
    @IBAction func backBtn(_ sender: Any) {
        self.goBackToMessage()
        
    
    }
    //MARK: - Check Video
    
    
    func checkvideo(){
        
        

    let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
    let parameter = ["models": "nudity,wad","api_user": "406594658", "api_secret": "7x3uRdHtFyaNg4HHd5C7"] as [String : Any]
    
    AF.upload(multipartFormData: { multipartFormData in
        for (key, value) in parameter {
            if let temp = value as? String {
                           multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                       }
           
            print("parameter")
           
        }
        
            print("checked")
        if self.uploadfiletype == "video"{
            multipartFormData.append(self.urls! as URL, withName: "media", fileName: "video.mp4", mimeType: "video/mp4")
        }
                
    }, to: "https://api.sightengine.com/1.0/video/check-sync.json",method: .post , headers: header)
        .responseJSON(completionHandler: { (response) in
                    print(parameter)
                    print(response)
                    
                               if let err = response.error{
                                print(err)
                                   
                                 return
                    }
                                   print("Succesfully uploaded")
                    
                                     let json = response.data
                                     print("1")
                                    print(json)
                                     response
            
                    if (json != nil)
                    {
                        let jsonObject = JSON(json!)
                        print("jsonObject")
                        print(jsonObject)
                        let jsonObjectData = jsonObject["data"]
                        print("jsonObjectData")
                        print(jsonObjectData)
                        let jsonObjectframes = jsonObjectData["frames"]
                        print(jsonObjectframes)
                        let jsonObjectNudity = jsonObjectframes[0]["nudity"]
                        
                        
                        var x:Bool = true
                        
                        for var i in (0..<jsonObjectframes.count)
                        {
                            if( jsonObjectframes[i]["nudity"]["raw"] > jsonObjectframes[i]["nudity"]["safe"] )
                            {
                                x = false
                                
                                self.showPromptAlert("Video Alert", message: "Selected Video is against our privacy policies", okTitle: "ok")
                                break
                            }
                            
                        }
                        
                        if ( x == true ){
                            self.handleSend()
                        }
                        
                    }
                })
    
    }
    
    
    
    //MARK: - Check Image
    
    func checkimage(){
        
        
        let imageData = (selectedimageView.image?.jpegData(compressionQuality: 0.8))!

    let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
    let parameter = ["models": "nudity,wad","api_user": "406594658", "api_secret": "7x3uRdHtFyaNg4HHd5C7"] as [String : Any]
    
    AF.upload(multipartFormData: { multipartFormData in
        for (key, value) in parameter {
            if let temp = value as? String {
                           multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                       }
           
            print("parameter")
           
        }
        
            print("checked")
        if self.uploadfiletype == "image"{
            multipartFormData.append(imageData, withName: "media" , fileName: "file.png", mimeType: "image/png")
        }
                
    }, to: "https://api.sightengine.com/1.0/check.json",method: .post , headers: header)
        .responseJSON(completionHandler: { (response) in
                    print(parameter)
                    print(response)
                    
                    if let err = response.error{
                        print(err)
                        //self.postBtnOutlet.isHidden = false
                        return
                    }
                    print("Succesfully uploaded")
                    
            let json = response.data
                    print("1")
            print(json)
            response
            
                    if (json != nil)
                    {
                        let jsonObject = JSON(json!)
                        print(jsonObject)
                        let jsonObjectNudity = jsonObject["nudity"]
                        print(jsonObjectNudity)
                        let raw = jsonObjectNudity["raw"]
                        print(raw)
                        let partial = jsonObjectNudity["partial"]
                        print(partial)
                        let safe = jsonObjectNudity["safe"]
                        print(safe)
                        
                        if raw > safe {
                            self.showPromptAlert("Image Alert", message: "Selected images is against our privacy policies", okTitle: "ok")
                            
                        } else {
                            self.handleSend()
                            self.ChatTextView.text = ""
                        }
                        
                        
                    }
                })
    
    }
    
    
    
    
    
    
    func checktext(){
        let header : HTTPHeaders = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let parameter = ["models": "text","api_user": "406594658", "api_secret": "7x3uRdHtFyaNg4HHd5C7", "lang": "en", "mode": "standard", "text": ChatTextView.text!] as [String : Any]
        
        AF.upload(multipartFormData: { multipartFormData in
                        for (key, value) in parameter {
                            if let temp = value as? String {
                                multipartFormData.append(temp.data(using: .utf8)!, withName: key)
                            }
                        }
                       
                    }, to: "https://api.sightengine.com/1.0/text/check.json",method: .post , headers: header)
                        .responseJSON(completionHandler: { (response) in
                            
                                    if let err = response.error{
                                        
                                        return
                                    }
                            print(response)
                            
                                    let json = response.data
                            
                                    if (json != nil)
                                    {
                                        
                                        let jsonObject = JSON(json!)
                                        print(jsonObject)
                                        let jsonObjectprofanity = jsonObject["profanity"]
                                        print(jsonObjectprofanity)
                                        let jsonObjectmatches = jsonObjectprofanity["matches"]
                                        print(jsonObjectmatches)
                                        
                                    
                                        self.matchesarray = jsonObjectmatches.arrayObject! as [Any]
                                        print(self.matchesarray)
                                        if self.matchesarray.count>0 {
                                            self.showPromptAlert("Text Alert", message: "Selected Text is against our privacy policies", okTitle: "ok")
                                        } else {
                                            self.handleSend()
                                            self.ChatTextView.text = ""
                                        }
                                      
                                    }
                                })

    }
    
    
    //MARK: - CanMessageApi
    
    func canMessageApi(){
        
        
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let parameters = CanMessage.Request (user_id: receiver)
            let requestService = SCService<CanMessage.Request,CanMessage.Response>()
            requestService.request("http://3.143.193.45:1337/api/canmessage", method: .post, parameters: parameters, encoder: JSONParameterEncoder(), header: header){(result) in
              switch result{
              case .success(let response):
                if response.status == true{
                debugPrint(response)
                    if response.data.canmessage == false{
                        self.ifBlockedLabel.isHidden = false
                        self.ChatTextView.isHidden = true
                        self.sendBtn.isHidden = true
                        self.callenderBtn.isHidden = true
                        self.attachmentBtn.isHidden = true
                        self.ifBlockedLabel.text = response.message
                
                    }
                do {
                  let encoder = JSONEncoder()
                    let data = try encoder.encode(response.data.canmessage)
                  //UserDefaults.standard.setValue(data, forKey: "USER")
                  print(UserDefaults.standard.object(forKey: "USER"))
                } catch{
                  print("Error")
                }

                }else{
                  self.showPromptAlert("Sorry!", message: response.message);
                }
              case .failure(let error):
                debugPrint(error)
              }
            }
        
    }
    
    
    //MARK: - Get Messages
    
    func getmessages()
    {
        var use = UserDefaults.standard.string(forKey: "uid") ?? ""

        var currentChat = ""
        if(self.type == "0"){
        currentChat = "\(self.receiver)"
        }
        else
        {
            currentChat = "\(use)_\(self.receiver)"
        }

        print("CURRENT CHAT")
        print(currentChat)
        FirebaseDatabase.Database.database().reference().child("Messages").child(currentChat).observe(.childAdded) { (snapshot) in
//            self.loadingSign.isHidden = false
//            self.loadingSign.startAnimating()
            self.ids.append(snapshot.key)
            print(snapshot.key)
            print("salmansohail\(snapshot.children)")
            var dic = [String:Any]()
            for child in snapshot.children {
                let snap = child as! DataSnapshot
                print("Snap is \(snap)")
                let key = snap.key
                print("Key is \(key)")
                let value = snap.value
                print("value is \(value)")
                dic[snap.key] = snap.value

                print("key = \(key)  value = \(value!)")
            }
//            self.chatTableView.reloadData()
//            self.scrollToBottom()
            
//            self.loadingSign.stopAnimating()
//            self.loadingSign.isHidden = true
            self.currenttime = Int64(Date().timeIntervalSince1970 * 1000)
            if(self.currenttime >= dic["timestamp"] as! Int64 ){
                self.mess[snapshot.key] = dic
                print("mess is \(self.mess[snapshot.key])")
                self.indexes[self.counter] = snapshot.key
                self.counter = self.counter + 1
                print(self.mess)
                self.chatTableView.reloadData()
                self.scrollToBottom()
            }
            
        }
        
        self.loadingSign.stopAnimating()
        self.loadingSign.isHidden = true
        print("MESSSSSS")
        print(self.mess)
        
    }
    
    //MARK: - ScrollToBottom
    
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.indexes.count-1, section: 0)
            self.chatTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)

        }
//        let indexPath = NSIndexPath(row: indexes.count-1, section: 0)
//        chatTableView.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: true)
    }
    
    //MARK: - GoBackToMessage
    
    func goBackToMessage(){

            guard let menuViewController = storyboard?.instantiateViewController(identifier: "MessageStoryboardId") as? MessageViewController else {
              return
            }
        
//        menuViewController.type = type
        menuViewController.modalPresentationStyle = .fullScreen
        
         
         self.present(menuViewController, animated: true, completion: nil)
    
    }
    
    //MARK: - HandleSend
    func handleSend() {
        if(self.uploadfiletype != "")
        {
            upload(url: self.urls)
        }
        else
        {
            self.handleSend1()
        }
    }
    func handleSend1() {

        
        let use = UserDefaults.standard.string(forKey: "uid") ?? ""
        let currentChat = "\(use)_\(self.receiver)"
        let currentChat1 = "\(self.receiver)_\(use)"
        print("CHAT TYPE")
        print(self.type)
        var message = ChatTextView.text!
        var type = "text"
        if(self.uploadfiletype != "")
        {
           // upload(url: self.urls)
            message = self.uploadfileurl
            type = self.uploadfiletype
        }
    
   //     self.chatTableView.reloadData()
        if(self.type == "0")
        {
            
            let groupchat = self.receiver
            let refg = FirebaseDatabase.Database.database().reference().child("Messages")
                .child(groupchat)
            let friendmessage = FirebaseDatabase.Database.database().reference().child("Friends").child(use).child(groupchat).child("message")
                
            let childRefg = refg.childByAutoId()
            
            let values = ["text": message, "idReceiver": self.receiver , "idSender": use, "isFuture": false, "status": "1", "timestamp": self.fdate, "type" : type ] as [String : Any]
            let lastmessage = ["text": message, "idReceiver": self.receiver , "idSender": use, "isFuture": false, "status": "1", "timestamp": self.fdate, "type" : type ] as [String : Any]
            
            friendmessage.updateChildValues(lastmessage)
            childRefg.updateChildValues(values)
        }
        else{
            print(message)
            print(type)
        let ref = FirebaseDatabase.Database.database().reference().child("Messages")
            .child(currentChat)
        let ref1 = FirebaseDatabase.Database.database().reference().child("Messages")
            .child(currentChat1)
            
            var friendmessage = FirebaseDatabase.Database.database().reference().child("Friends").child(use).child(self.receiver).child("message")
            var friendmessage1 = FirebaseDatabase.Database.database().reference().child("Friends").child(self.receiver).child(use).child("message")
            if(self.member.count > 0)
            {
                friendmessage = FirebaseDatabase.Database.database().reference().child("Friends").child(use).child(self.receiver)
                friendmessage1 = FirebaseDatabase.Database.database().reference().child("Friends").child(self.receiver).child(use)
                
            }
        let childRef = ref.childByAutoId()
        let childRef1 = ref1.childByAutoId()
            let values = ["text": message, "idReceiver": self.receiver , "idSender": use, "isFuture": self.isfuture, "status": "1", "timestamp": self.fdate , "type" : type ] as [String : Any]
            let lastmessage = ["text": message, "idReceiver": self.receiver , "idSender": use, "isFuture": self.isfuture, "status": "1", "timestamp": self.fdate , "type" : type ] as [String : Any]
            
                  if (self.isfuture == true){
                      
                      decreaseFuture()
                  }
         //   else{
                print(self.member.count)
                if(self.member.count > 0)
                {
                    let complete = ["avata": self.member[0].profilepic, "email": self.member[0].email, "id": self.member[0].id ,"message": lastmessage, "name": self.member[0].name] as [String : Any]
                    if let data = UserDefaults.standard.data(forKey: "USER"){
                        do {
                            let decoder = JSONDecoder()
                            let user = try decoder.decode(User.self, from: data)
                            print(user.self)
                          
                            self.myimage = user.profilepic
                            self.myemail = user.email
                            self.myname = user.name
                        }catch{
                            print("Error")
                        }
                    }
                    let complete1 = ["avata": self.myimage, "email": self.myemail, "id": use ,"message": lastmessage, "name": self.myname] as [String : Any]
                        friendmessage.updateChildValues(complete)
                        friendmessage1.updateChildValues(complete1)
                    
                }
                else{
                friendmessage.updateChildValues(lastmessage)
                friendmessage1.updateChildValues(lastmessage)
                }
       //     }
        childRef.updateChildValues(values)
        childRef1.updateChildValues(values)
        
        }
        self.uploadfileurl = ""
        self.uploadfiletype = ""
        self.isfuture = false
        self.fdate = Int64(Date().timeIntervalSince1970 * 1000)
       
    }
    

    
    
    //MARK: - DecreaseFuture
    
    func decreaseFuture(){
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        //let parameters = CanMessage.Request (user_id: receiver)
          let requestService = SCService<DecreaseFutureMsg.Request,DecreaseFutureMsg.Response>()
          requestService.request("http://3.143.193.45:1337/api/decreasecount", method: .post, parameters: nil, encoder: JSONParameterEncoder(), header: header){(result) in
           switch result{
           case .success(let response):
            if response.status == true{
            debugPrint(response)
                UserDefaults.standard.setValue(response.data?.user.fcount, forKey: "fcount")
                UserDefaults.standard.setValue(response.data?.user, forKey: "USER")
            }else{
             self.showPromptAlert("Sorry!", message: response.message);
            }
           case .failure(let error):
            debugPrint(error)
           }
          }
      }
    func profileApi(){
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallProfile.Request,ApiCallProfile.Response>()
        requestService.request("http://3.143.193.45:1337/api/profile", method: .post, parameters: nil, encoder: JSONParameterEncoder(), header: header){(result) in
            switch result{
            case .success(let response):
                if response.status == true{
                    UserDefaults.standard.setValue(response.data?.user.fcount, forKey: "fcount")
                    UserDefaults.standard.setValue(response.data?.user, forKey: "USER")
                }
                debugPrint(response)
                case .failure(let error):
                    debugPrint(error)
                }
            }
        
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return true
    }
    @IBAction func playbtnreceivertableviewcell(_ sender: Any) {
        cellVideoTapped()
        
    }
    
    @IBAction func playbtnactiontableviewcell(_ sender: Any) {
        cellVideoTapped()
    }
}
extension ChatViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return indexes.count
        
}

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dic = self.mess[indexes[indexPath.row] ?? ""] as! NSDictionary
        let message = dic["text"]
        let sender = dic["idSender"] as! String
        let use = UserDefaults.standard.string(forKey: "uid") ?? ""
        let currenttime = Int64(Date().timeIntervalSince1970 * 1000)
        
        if (use != sender){
            let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
            cell.chatImage.isHidden = true
            cell.chatLabel.isHidden = true
            cell.playbutton.isHidden = true
            if(dic["type"] as! String == "image")
            {
                func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                    return 200
                  }
          //      let url = URL(string: message as! String)
                imageUrl = message as! String
         //           let data = try? Data(contentsOf: url!)
           //         if let imageData = data {
                        cell.chatImage.image = UIImage(named: "media_phto" )
                        cell.chatImage.contentMode = .scaleAspectFill
                        cell.chatImage.load(urlString: message as! String)
//                        cell.chatImage.image = UIImage(data: imageData)
                        cell.chatImage.isUserInteractionEnabled = true
                        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(cellImageTapped))
                        cell.chatImage.isUserInteractionEnabled = true
                        cell.chatImage.addGestureRecognizer(gestureRecognizer)
                        
            //        }
                cell.chatImage.isHidden = false
                cell.chatLabel.isHidden = true
                cell.playbutton.isHidden = true
            }
           else if(dic["type"] as! String == "video")
            {
               func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                   return 200
                   }
                let url = URL(string: message as! String)
            videoUrl = message as! String
               cell.chatImage.image = UIImage(named: "media_video" )
               cell.chatImage.contentMode = .scaleAspectFill
              self.getThumbnailImageFromVideoUrl(url: url!){ (thumbnailImage) in
                  cell.playbutton.isHidden = false
                  cell.chatImage.image = thumbnailImage
//                  cell.videoUrl = self.videoUrl
//                  cell.receiver = self.receiver
//                  cell.albumname = self.albumname
//                  let gestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(self.cellVideoTapped))
//                  cell.playbutton.isUserInteractionEnabled = true
//                  cell.playbutton.addGestureRecognizer(gestureRecognizer1)
                  let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.cellVideoTapped))
                  cell.chatImage.isUserInteractionEnabled = true
                  cell.chatImage.addGestureRecognizer(gestureRecognizer)
                    }
            cell.chatImage.isHidden = false
            cell.chatLabel.isHidden = true
            
                
            }
           else if(dic["type"] as! String == "pdf")
            {
               func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                   return 100
                }
      //         let url = URL(string: message as! String)
               pdfUrl = message as! String
               print("adadadada\(pdfUrl)")
        //           let data = try? Data(contentsOf: url!)
               cell.chatImage.isUserInteractionEnabled = true
               let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(cellPDFTapped))
               cell.chatImage.addGestureRecognizer(gestureRecognizer)
               cell.chatImage.image = UIImage(named: "pdf.png")
               cell.chatImage.contentMode = .scaleAspectFit
               cell.chatImage.isHidden = false
               cell.chatLabel.isHidden = true
               cell.playbutton.isHidden = true
                
            }
            else{
                func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                    return 100
                    }
                cell.chatLabel.text = message as? String
                cell.chatImage.isHidden = true
                cell.chatLabel.isHidden = false
                cell.playbutton.isHidden = true
                cell.setNeedsUpdateConstraints()
            }
//            let indexPath = IndexPath(row: indexes.count-1, section: 0)
//            chatTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            return cell
        }
        
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReciverTableViewCell", for: indexPath) as! ReciverTableViewCell
            
            
            cell.ChatImage.isHidden = true
            cell.ChatReciverLabel.isHidden = true
            cell.playbutton.isHidden = true
            if(dic["type"] as! String == "image")
            {
                func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                    return 200
                    }
            //    let url = URL(string: message as! String)
                imageUrl = message as! String
                
                   // let data = try? Data(contentsOf: url!)
                      
                //    if let imageData = data {
                cell.ChatImage.image = UIImage(named: "media_phto" )
                cell.ChatImage.contentMode = .scaleAspectFill
                        cell.ChatImage.load(urlString: message as! String)
//                        cell.ChatImage.image = UIImage(data: imageData)
                        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(cellImageTapped))
                        cell.ChatImage.isUserInteractionEnabled = true
                        cell.ChatImage.addGestureRecognizer(gestureRecognizer)
                        //cell.chatLabel.isHidden = true
                  //  }
                cell.ChatImage.isHidden = false
                cell.ChatReciverLabel.isHidden = true
                cell.playbutton.isHidden = true
            }
           else if(dic["type"] as! String == "video")
            {
               func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                   return 200
                   }
                let url = URL(string: message as! String)
//               let stringurls = url as! String
               videoUrl = message as! String
               cell.ChatImage.image = UIImage(named: "media_video" )
               cell.ChatImage.contentMode = .scaleAspectFill
              self.getThumbnailImageFromVideoUrl(url: url!){ (thumbnailImage) in
//                  cell.ChatImage.load(urlString: url as! String)
                  
                  cell.ChatImage.image = thumbnailImage
                
                    }

               cell.ChatImage.isHidden = false
               cell.ChatReciverLabel.isHidden = true
               cell.playbutton.isHidden = false
//               cell.videoUrl = self.videoUrl
//               cell.receiver = self.receiver
//               cell.albumname = self.albumname
//               let gestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(self.cellVideoTapped))
//               cell.playbutton.isUserInteractionEnabled = true
//               cell.playbutton.addGestureRecognizer(gestureRecognizer1)
               let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.cellVideoTapped))
               cell.ChatImage.isUserInteractionEnabled = true
               cell.ChatImage.addGestureRecognizer(gestureRecognizer)
                
            }
           else if(dic["type"] as! String == "pdf")
            {
               func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                   return 100
                   }
                
             //  let url = URL(string: message as! String)
               pdfUrl = message as! String
             //  let data = try? Data(contentsOf: url!)
       //        if data != nil {
               
            cell.ChatImage.image = UIImage(named: "pdf.png")
            cell.ChatImage.contentMode = .scaleAspectFit
            cell.ChatImage.isHidden = false
            cell.playbutton.isHidden = true
            cell.ChatReciverLabel.isHidden = true
            cell.ChatImage.isUserInteractionEnabled = true
            var gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(cellPDFTapped))
            cell.ChatImage.addGestureRecognizer(gestureRecognizer)
         //      }
            }
            else{
                func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                    return 100
                    }
                cell.ChatReciverLabel.text = message as? String
                cell.ChatImage.isHidden = true
                cell.ChatReciverLabel.isHidden = false
                cell.playbutton.isHidden = true
                cell.setNeedsUpdateConstraints()
            }
            
            return cell
    }
  }
    
    
    
    
    //MARK: - CellPDFTapped
    
    @objc func cellPDFTapped(){
        print("adadadada\(pdfUrl)")

        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "PDFStoryboardId") as? PDFViewController else {
            return

        }
        let presentingVC = self.presentingViewController
        menuViewController.modalPresentationStyle = .overCurrentContext
        menuViewController.albumname = albumname
        menuViewController.urlString = pdfUrl
        menuViewController.receiver = receiver
        menuViewController.membername = self.membername
        menuViewController.memberimage = self.memberimage
        dismiss(animated: false) {
            presentingVC!.present(menuViewController, animated: false,completion: nil)
        }
        
        
    }
    
    //MARK: - CellVideoTapped
    
    @objc func cellVideoTapped(){
        
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "VideoPlayerViewController") as? VideoPlayerViewController else {
            return
        }
        let presentingVC = self.presentingViewController
        menuViewController.modalPresentationStyle = .overCurrentContext
        menuViewController.albumname = albumname
        menuViewController.urlString = videoUrl
        menuViewController.receiver = receiver
        menuViewController.membername = self.membername
        menuViewController.memberimage = self.memberimage
        dismiss(animated: false) {
            presentingVC!.present(menuViewController, animated: false,completion: nil)
        }
        
    }
    
    //MARK: - CellImageTapped
   @objc func cellImageTapped(){
    
    guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "photoShow") as? detailedImg else {
        return
    }
    let presentingVC = self.presentingViewController
       menuViewController.modalPresentationStyle = .overCurrentContext
       menuViewController.albumname = albumname
       menuViewController.name = imageUrl
       menuViewController.membername = self.membername
       menuViewController.memberimage = self.memberimage
       menuViewController.receiver = receiver
         dismiss(animated: false) {
        presentingVC!.present(menuViewController, animated: false,completion: nil)
    }
    
   }
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
      DispatchQueue.global().async { //1
        let asset = AVAsset(url: url) //2
        let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
        avAssetImageGenerator.appliesPreferredTrackTransform = true //4
        let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
        do {
          let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
          let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
          DispatchQueue.main.async { //8
            completion(thumbNailImage) //9
          }
        } catch {
          print(error.localizedDescription) //10
          DispatchQueue.main.async {
            completion(nil) //11
          }
        }
      }
    }
    
}
//extension ChatViewController : DataForCccell{
//    func playVideo(videoUrl: String, receiver: String, albumname: String) {
//    }
//}
extension Date {
    func dateString(_ format: String = "MMM-dd-YYYY, hh:mm a") -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    return dateFormatter.string(from: self)
  }
  func dateByAddingYears(_ dYears: Int) -> Date {
    var dateComponents = DateComponents()
    dateComponents.year = dYears
    return Calendar.current.date(byAdding: dateComponents, to: self)!
  }
}


extension ChatViewController: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
       // viewModel.attachDocuments(at: urls)
        print(urls)
        self.urls = urls[0]
        self.uploadfiletype = "pdf"
            controller.dismiss(animated: true)
        self.selectedimageView.image = UIImage(named: "pdf")
        self.selectedimageView.isHidden = false
        self.cancelButton.isHidden = false
    }
//        guard controller.documentPickerMode == .open, let url = urls.first, url.startAccessingSecurityScopedResource()
//        else { return }
//              defer {
//                  DispatchQueue.main.async {
//                      url.stopAccessingSecurityScopedResource()
//                  }
//                   }
//        do{
//            let imgName = UUID().uuidString
//                                    let documentDirectory = NSTemporaryDirectory()
//                                    let localPath = documentDirectory.appending(imgName)
//        let documentData = try Data(contentsOf: urls, options: .dataReadingMapped)
//
//            let data = documentData as NSData
//            //.jpegData(compressionQuality: 0.75)! as NSData
//                           data.write(toFile: localPath, atomically: true)
//            let photoURL = URL.init(fileURLWithPath: localPath)
//                                self.urls = photoURL
//        }catch let error {
//                    print(error)
//        }
//
//        self.uploadfiletype = "pdf"
//            controller.dismiss(animated: true)
//        upload(url: self.urls)
//    }

     func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
