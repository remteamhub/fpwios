//
//  CreateGroupViewController.swift
//  Family Pic Will
//
//  Created by mac on 9/10/21.
//

import UIKit
import FirebaseDatabase
import Firebase
class CreateGroupViewController: UIViewController {
    var a = 0
    var sendMembers: [User] = []
    var ids: [String] = []
    var user: User?
    @IBOutlet weak var groupNameText: UITextField!
    @IBOutlet weak var GroupMemberTableView: UITableView!
    @IBOutlet weak var CreateButton: UIButton!
    @IBOutlet weak var MembersLabel: UILabel!
    @IBAction func backButton(_ sender: Any) {
        
        guard let menuviewcontroller = self.storyboard?.instantiateViewController(identifier: "NewGroupStoryboardId") as? NewGroupViewController else {
            return
        }
        menuviewcontroller.a = self.a
        menuviewcontroller.user = self.user
        menuviewcontroller.modalPresentationStyle = .fullScreen
        self.present(menuviewcontroller, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(self.sendMembers)
        GroupMemberTableView.delegate = self
        GroupMemberTableView.dataSource = self
        MembersLabel.text = "\(self.sendMembers.count)"

        // Do any additional setup after loading the view.
    }
    @IBAction func CreateButton(_ sender: Any) {
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "MessageStoryboardId") as? MessageViewController else {
            return
        }
        if groupNameText.text != nil{
        var uid =  UserDefaults.standard.string(forKey: "uid") ?? ""
        
        let refg = FirebaseDatabase.Database.database().reference().child("Friends")
            .child(uid)
        let string = self.ids.joined(separator: ",")
        
        
        let childRefg = refg.child( groupNameText.text!)
            

        let message = ["idReceiver": "0" , "idSender": "0", "isFuture": false, "status": "1", "timestamp": 0, "text" : "",  ] as [String : Any]
        
        
        let values = ["avata": "", "email": "mianahmed115@gmail.com" ,"memberIds": string,"id":"60a5329908590f7229c23881","name": groupNameText.text,"type": "0", "message": message  ] as [String : Any]
        
        childRefg.updateChildValues(values)
        
        menuViewController.modalPresentationStyle = .fullScreen
        self.present(menuViewController, animated: true, completion: nil)

        }else{
            showPromptAlert("Missing group name", message: "Please enter a name for this group")
            
        }
    }
}
extension CreateGroupViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sendMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CreateGroupTableViewCell
        
        cell.MemberName.text = self.sendMembers[indexPath.row].name
        return cell
        
    }
    
}

