//
//  AboutUsViewController.swift
//  Family Pic Will
//
//  Created by mac on 8/4/21.
//

import UIKit
import AVFoundation
import AVKit
//import WebKit
//import MediaPlayer

class AboutUsViewController: UIViewController {

  // var moviePlayer: MPMoviePlayerController?
    @IBOutlet weak var titlebarLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var VideoUIView: UIView!
    var player = AVPlayer(url: URL( string: "http://3.143.193.45/fpw.mp4")!)
    
    
    
    let transition = SlideInTransition()
    @IBAction func openSideMenuClicked(_ sender: Any) {
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "SideMenuStoryboardId") else {
              return
            }
            menuViewController.modalPresentationStyle = .overCurrentContext
            menuViewController.transitioningDelegate = self
            present(menuViewController, animated: true)
//        let label = UILabel(frame: CGRect(x: 500, y: 250, width: 200, height: 21))
//        label.center = CGPoint(x: 160, y: 285)
//        label.textAlignment = .center
//        label.text = "I'm a test label"
    }
   //let avPlayerViewController = CustomAVPlayerC()
  //  var playerView: AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        transition.delegate = self
    //    playVideo()
        
        // Do any additional setup after loading the view.
    }
    
 
    //func playVideo() {
 //     let path = Bundle.mainBundle.path(forResource: "paint-me_intro", ofType:"mp4")
   //         let url = NSURL           //.fileURLWithPath("http://3.143.193.45/fpw.mp4")
      //  let fileUrl = URL(string: "http://3.143.193.45/fpw.mp4")
        //    moviePlayer = MPMoviePlayerController(contentURL: fileUrl)
          //  if let player = moviePlayer {
                
            //    player.view.frame = self.VideoUIView.bounds
               // player.controlStyle = none
              //  player.prepareToPlay()
                //player.scalingMode = .aspectFit
                //self.VideoUIView.addSubview(player.view)
            //}
        //}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //let player = AVPlayer(url: URL( string: "http://3.143.193.45/fpw.mp4")!)
        let layer = AVPlayerLayer(player: player)
        layer.frame = VideoUIView.bounds
        VideoUIView.layer.addSublayer(layer)
        layer.videoGravity = .resizeAspect
        player.play()
        
    }
    override func viewWillDisappear(_ animated: Bool) {
         NotificationCenter.default.removeObserver(self)
        if player != nil{
            player.replaceCurrentItem(with: nil)
            //player = nil
        }
    }

}
//extension AVPlayer {
  // func stop(){
    //self.seek(to: CMTime.zero)
    //self.pause()
  // }
//}

//guard let movieURL = URL(string: "http://3.143.193.45/fpw.mp4")
       // else { return }

  //  playerView = AVPlayer(url: movieURL)
    //AVPlayerViewController.player = playerView
    //AVPlayerViewController.view.frame = newView.bounds
    //self.addChildViewController(AVPlayerViewController)
    //newView.addSubview(AVPlayerViewController.view)
    //AVPlayerViewController.didMove(toParentViewController: self)
//}

extension AboutUsViewController : UIViewControllerTransitioningDelegate{
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = true
    return transition
  }
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = false
    return transition
  }
}
extension AboutUsViewController : CloseSideMenu{
    func closeSideMenu(){
        dismiss(animated: true, completion: nil)
    }

}

