//
//  NewFeedViewController.swift
//  Family Pic Will
//
//  Created by Apple on 06/05/2021.
//

import UIKit

class NewFeedViewController: UIViewController {
    var toEditProfileScreenCheck : Bool?
    let transition = SlideInTransition()
    @IBAction func openSideMenuSideClick(_ sender: Any) {
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "SideMenuStoryboardId") else {
              return
            }
            menuViewController.modalPresentationStyle = .overCurrentContext
            menuViewController.transitioningDelegate = self
            present(menuViewController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        transition.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NewFeedViewController : UIViewControllerTransitioningDelegate{
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = true
    return transition
  }
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = false
    return transition
  }
}



extension NewFeedViewController : CloseSideMenu{
    func closeSideMenu(){
        dismiss(animated: true, completion: nil)
        //transition.isPresenting = true
    }
}
