//
//  ContactUsViewController.swift
//  Family Pic Will
//
//  Created by Apple on 19/05/2021.
//

import Foundation
import UIKit
import Alamofire
import SDWebImage

class ContactUsViewController: UIViewController {
    let transition = SlideInTransition()
    @IBOutlet weak var titleBar: UILabel!
    @IBOutlet weak var messageTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        //titleBar.font = UIFont(name: "Roboto-Regular", size: 25)
        transition.delegate = self
        // Do any additional setup after loading the view.
    }
    @IBAction func backButtonClicked(_ sender: Any) {
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "SettingStoryboardId") else {
              return
            }
        let presentingVC = self.presentingViewController
        menuViewController.modalPresentationStyle = .fullScreen
        dismiss(animated: false) {
            presentingVC!.present(menuViewController, animated: false,completion: nil)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        .lightContent
    }

    @IBAction func submitClicked(_ sender: Any) {
        if(messageTextField.text != nil && messageTextField.text != "")
        {
            callApi()
            showPromptAlert("Message Submited", message: "We will contact you shortly")
           self.messageTextField?.text = nil
//            guard let menuViewController = storyboard?.instantiateViewController(identifier: "SettingStoryboardId") as? SettingViewController else {
//                  return
//                }
//            let presentingVC = self.presentingViewController
//            menuViewController.modalPresentationStyle = .fullScreen
//            dismiss(animated: true) {
//                presentingVC!.present(menuViewController, animated: true,completion: nil)
//            }
            
        }
        
        else{
            
            showPromptAlert("Error", message: "Please Enter Text")

            
        }
        
    }
    
    func callApi()
    {
        let  parameters = ApiCallContactus.Request(message: messageTextField.text ?? "HI")
        let header = ["authorization":"Bearer \(UserDefaults.standard.string(forKey: "token")!)"]
        let requestService = SCService<ApiCallContactus.Request,ApiCallContactus.Response>()
        requestService.request("http://3.143.193.45:1337/api/contactus", method: .post, parameters: parameters, encoder: JSONParameterEncoder(), header: header){(result) in
            switch result{
            case .success(let response):
                
                if response.status == true{
                debugPrint(response)
                    
    }
            case .failure(let error):
                debugPrint(error)
            }
}
    }
    
}
extension ContactUsViewController : UIViewControllerTransitioningDelegate{
  func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = true
    return transition
  }
  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    transition.isPresenting = false
    return transition
  }
}



extension ContactUsViewController : CloseSideMenu{
    func closeSideMenu(){
        dismiss(animated: true, completion: nil)
        //transition.isPresenting = true
    }
}
