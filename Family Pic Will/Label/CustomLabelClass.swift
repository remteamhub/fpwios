//
//  CustomLabelClass.swift
//  Family Pic Will
//
//  Created by Apple on 21/05/2021.
//
import UIKit
import Foundation
class CustomLabelClass: UILabel {
    override func layoutSubviews() {
        self.font = UIFont.init(name: "Roboto-Regular", size: self.font.pointSize)
    }
}
