//
//  MessagesViewController.swift
//  Family Pic Will
//
//  Created by mac on 8/12/21.
//

import UIKit
import Firebase
//import JGProgressHUD

class MessagesViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    private var tableView: UITableView = {
        let table = UITableView()
        table.isHidden = false
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return table
    
    
    }()
      
    private let noConversationsLabel: UILabel = {
        let label = UILabel()
        label.text = "No Conversation"
        label.textAlignment = .center
        label.textColor = .gray
        label.font = .systemFont(ofSize: 21, weight: .medium)
        label.isHidden = true
        return label
      }()
    @IBOutlet weak var titleBarLabel: UILabel!
    let transition = SlideInTransition()
    @IBAction func NewMessageClicked(_sender: Any){
        print("clicked")
        guard let menuViewController = self.storyboard?.instantiateViewController(identifier: "NewMessageStoryboardId") as? NewMessageViewController else {
              return
            }
        
        menuViewController.modalPresentationStyle = .fullScreen
            self.present(menuViewController, animated: true,completion: nil)
    
    }
    @IBAction func openSideMenuClicked(_ sender: Any) {
        print("Clicked")
        guard let menuViewController = storyboard?.instantiateViewController(identifier: "SideMenuStoryboardId") else {
              return
            }
            menuViewController.modalPresentationStyle = .overCurrentContext
            menuViewController.transitioningDelegate = self
            present(menuViewController, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        transition.delegate = self
        view.addSubview(tableView)
        view.addSubview(noConversationsLabel)
        setupTableView()
        fetchConversations()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
    
}
extension MessagesViewController : UIViewControllerTransitioningDelegate{
      func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.isPresenting = true
        return transition
      }
      func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.isPresenting = false
        return transition
      }
    }
    extension MessagesViewController : CloseSideMenu{
        func closeSideMenu(){
            dismiss(animated: true, completion: nil)
        }
    }
         private func setupTableView() {
         //tableView.delegate = self
        // tableView.dataSource = self
  }

private func fetchConversations(){
    
}

extension MessagesViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = "Hello World"
        //cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = ChatsViewController()
        vc.title = "Mian Ahmad"
        vc.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(vc, animated: true)
        print("tapped")
        
    }
}





