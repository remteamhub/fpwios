//
//  galleryVC.swift
//  Bdaynotif
//
//  Created by Coder Crew on 5/20/21.
//

import UIKit

class galleryVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MyCustomCell

//        cell.backgroundColor = .lightGray
        cell.data = data[indexPath.row]
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var Width: CGFloat = collectionView.frame.width/4 - 1
        if UIDevice.current.orientation.isLandscape {
            Width = collectionView.frame.width/6 - 1
        }
        return CGSize(width: Width, height: Width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = ImageVC()
        vc.selectedIndex = indexPath.row
        vc.imageArr = data
        pushView(viewController: vc)
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
   
    
    fileprivate let data = [
        MyCustomData(Image: Image.everest),
        MyCustomData(image: Image.everest),
        MyCustomData(image: Image.MtFishTail),
        MyCustomData(image: Image.tilicho),
        MyCustomData(image: Image.mustang),
        MyCustomData(image: Image.rara),
        MyCustomData(image: Image.jomsom),
        MyCustomData(image: Image.pasupatinath),
        MyCustomData(image: Image.patan),
        MyCustomData(image: Image.bhaktapur_darbar),
        MyCustomData(image: Image.bhaktapur),
        MyCustomData(image: Image.MtEverest),
        MyCustomData(image: Image.everest),
        MyCustomData(image: Image.MtFishTail),
        MyCustomData(image: Image.tilicho),
        MyCustomData(image: Image.mustang),
        MyCustomData(image: Image.rara),
        MyCustomData(image: Image.jomsom),
        MyCustomData(image: Image.pasupatinath),
        MyCustomData(image: Image.patan),
        MyCustomData(image: Image.bhaktapur_darbar),
        MyCustomData(image: Image.bhaktapur),
    ]
    
    
    fileprivate let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(MyCustomCell.self, forCellWithReuseIdentifier: "cell")
        cv.backgroundColor = .white
        return cv
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Gallery"
        
        setupView()
        setupconstraint()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setupView(){
        view.addSubview(collectionView)
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    func setupconstraint(){
        collectionView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}

//extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return data.count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MyCustomCell
//
////        cell.backgroundColor = .lightGray
//        cell.data = data[indexPath.row]
//
//        return cell
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        var Width: CGFloat = collectionView.frame.width/4 - 1
//        if UIDevice.current.orientation.isLandscape {
//            Width = collectionView.frame.width/6 - 1
//        }
//        return CGSize(width: Width, height: Width)
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let vc = ImageVC()
//        vc.selectedIndex = indexPath.row
//        vc.imageArr = data
//        pushView(viewController: vc)
////        self.navigationController?.pushViewController(vc, animated: true)
//    }
//}
