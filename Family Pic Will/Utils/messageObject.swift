//
//  messageObject.swift
//  familywithwall
//
//  Created by Apple on 28/04/2021.
//

import Foundation
struct MessageData {
    let text: String
    let isFirstUser: Bool
}
