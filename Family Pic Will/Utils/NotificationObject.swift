//
//  NotificationObject.swift
//  familywithwall
//
//  Created by Apple on 29/04/2021.
//

import Foundation
struct NotificationData{
    let userimage: String
    let username: String
    let checked: Bool
    let notification: String
}
