//
//  ApiCallContactus.swift
//  Family Pic Will
//
//  Created by mac on 9/13/21.
//

import Foundation

enum ApiCallContactus {
    struct Request: Encodable {
        var message: String
    }
    struct Response:Codable {
        var status : Bool
        var message : String
        var data: User?
        
    }
}
