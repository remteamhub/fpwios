//
//  ApiCallChangeFamily.swift
//  FPW
//
//  Created by Apple on 04/05/2021.
//

import Foundation
enum ApiCallChangeFmaily {
    struct Request : Encodable {
        var familyCode:String
        
    }
    struct Response : Codable {
        var status:Bool
        var message:String
        var data : User
        enum CodingKeys: String, CodingKey {
                case status = "status"
                case message = "message"
                case data = "data"
            }
    }
}
