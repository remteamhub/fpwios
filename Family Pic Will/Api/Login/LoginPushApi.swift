//
//  LoginPushApi.swift
//  Family Pic Will
//
//  Created by Coder Crew on 6/2/21.
//

import Foundation
enum LoginPushApi {
    struct Request : Encodable{
       var token: String
        
        
    }
    struct Response : Codable{
        var status: Bool
        var message:String
    }

}
