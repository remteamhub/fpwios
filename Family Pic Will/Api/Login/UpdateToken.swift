//
//  UpdateToken.swift
//  Family Pic Will
//
//  Created by mac on 10/14/21.
//

import Foundation
enum UpdateToken {
    struct Request : Encodable{
       var email: String
    }
    struct Response : Codable{
        var status: Bool
        var message:String
        var data : DataClass
    }
    
    struct DataClass: Codable {
        let user: User
        let token: String
    }
}
