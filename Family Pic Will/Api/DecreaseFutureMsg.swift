//
//  DecreaseFutureMsg.swift
//  Family Pic Will
//
//  Created by Apple on 23/09/2021.
//

import Foundation
enum DecreaseFutureMsg {
  struct Request: Encodable {
  }
  struct Response:Codable {
    var status : Bool
    var message : String
    var data: Profile?
  }
}
