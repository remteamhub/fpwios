//
//  ApiCallProfilePic.swift
//  FPW
//
//  Created by Apple on 06/05/2021.
//

import Foundation
enum ApiCallProfilePic {
    struct Request : Encodable {
        
    }
    struct Response : Codable {
        var status : Bool
        var message : String
        var data : ProfilePic?
    }
}
