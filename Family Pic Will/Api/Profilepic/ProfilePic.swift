//
//  ProfilePic.swift
//  FPW
//
//  Created by Apple on 06/05/2021.
//

import Foundation
struct ProfilePic : Codable {
    var profilepic : String
    var updatedAt : Int
}
