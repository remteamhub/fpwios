//
//  ApiCallGetDeceasedMembers.swift
//  Family Pic Will
//
//  Created by mac on 9/13/21.
//

import Foundation
enum ApiCallGetDeceasedMembers {
    struct request: Encodable {
    }
    struct Response: Codable {
        var status: Bool
        var message: String
        var data: [User?]
    }
}
