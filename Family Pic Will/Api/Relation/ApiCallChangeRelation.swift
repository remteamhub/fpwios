//
//  ApiCallChangeRelation.swift
//  Family Pic Will
//
//  Created by Apple on 27/05/2021.
//

enum ApiCallChangeRelation {
    struct Request : Encodable{
        var email : String?
        var relation : String?
        var id: String?
    }
    struct Response : Codable{
        var status: Bool
        var message:String
        
    }
}
