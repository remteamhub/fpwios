//
//  ApiCallUnbloackMember.swift
//  Family Pic Will
//
//  Created by mac on 9/13/21.
//

import Foundation
enum ApiCallUnblockMember {
    struct Request : Encodable {
        var id : String?
    }
    struct Response : Codable {
        var status: Bool
        var message: String
    }
}
