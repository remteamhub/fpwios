//
//  CanMessage.swift
//  Family Pic Will
//
//  Created by Apple on 22/09/2021.
//

import Foundation
enum CanMessage {
    struct Request: Encodable {
        var user_id: String
    }
    struct Response:Codable {
        var status : Bool
        var message : String
        var data: data
        
    }
    struct data: Codable{
        
        
        var canmessage: Bool
    }
}
