//
//  ApiCallUpdatePassword.swift
//  FPW
//
//  Created by Apple on 06/05/2021.
//

import Foundation
enum ApiCallUpdatePassword {
    struct Request : Encodable{
        var oldpassword : String
        var newpassword : String
    }
    struct Response : Codable{
        var status : Bool
        var message : String
    }
}
