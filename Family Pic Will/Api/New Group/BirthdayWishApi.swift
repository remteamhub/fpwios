//
//  BirthdayWishApi.swift
//  Family Pic Will
//
//  Created by Coder Crew on 6/2/21.
//

import Foundation
enum BirthdayWishApi {
    struct Request : Encodable{
        var text : String
        var familyCode : String
        var userid : String
    }
    struct Response : Codable{
        var status: Bool
        var message:String
        
    }
}
