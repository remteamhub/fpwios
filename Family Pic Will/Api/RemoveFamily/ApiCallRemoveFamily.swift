//
//  ApiCallRemoveFamily.swift
//  FPW
//
//  Created by Apple on 05/05/2021.
//

import Foundation
enum ApiCallRemoveFamily {
    struct Request : Encodable {
        var familyCode : String
    }
    struct Response : Codable {
        var status : Bool
        var message : String
    }
}
