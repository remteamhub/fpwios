//
//  MediaImages&Videos.swift
//  Family Pic Will
//
//  Created by Apple on 23/05/2021.
//

import Foundation
struct MediaImages_Videos: Codable{
    
    let image, video: [Image]
}
