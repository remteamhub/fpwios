//
//  ApiCallGetMedia.swift
//  Family Pic Will
//
//  Created by Apple on 23/05/2021.
//

import Foundation
import UIKit
enum ApiCallGetMedia {
    struct Request : Encodable{
        var uid:String?
    }
    struct Response : Codable{
        var status: Bool
        var message:String
        var data: MediaImages_Videos?
    }
}

