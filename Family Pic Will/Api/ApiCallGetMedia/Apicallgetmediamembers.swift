//
//  File.swift
//  Family Pic Will
//
//  Created by mac on 8/17/21.
//

import Foundation
import UIKit
enum Apicallgetmediamembers {
    struct Request : Encodable{
        let uid: String
    }
    struct Response : Codable{
        var status: Bool
        var message:String
        var data: MediaImages_Videos?
    }
}
