//
//  posts.swift
//  familywithwall
//
//  Created by Apple on 17/05/2021.
//

import Foundation
struct Image: Codable {
    let createdAt, updatedAt: Int
    let id, albumid: String
    let image: String?
    let userid: String
    let video: String?
    
}
