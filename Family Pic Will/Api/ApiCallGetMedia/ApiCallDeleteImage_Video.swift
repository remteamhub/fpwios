//
//  ApiCallDeleteImage_Video.swift
//  Family Pic Will
//
//  Created by Apple on 28/05/2021.
//

import Foundation
enum ApiCallDeleteImage_Video {
    struct Request : Encodable{
        var vpid : String
        var type : String
    }
    struct Response : Codable{
        var status: Bool
        var message:String
        
    }
}
