//
//  Likers.swift
//  familywithwall
//
//  Created by Apple on 19/05/2021.
//

import Foundation
struct Likers : Codable {
//    let userimage: String
//    let username: String
//    let posttime: String
//    let postimage: String
//    let numberoflikes: Int
//    let numberofcomments: Int
    
    let createdAt: Int
    let updatedAt: Int
    let id: String
    let postid: String
    let likerid: String
    
    let likername: String
    let likerprofilepic: String
    
}
