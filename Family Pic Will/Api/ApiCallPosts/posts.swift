//
//  posts.swift
//  familywithwall
//
//  Created by Apple on 17/05/2021.
//

import Foundation
struct Posts : Codable {
//    let userimage: String
//    let username: String
//    let posttime: String
//    let postimage: String
//    let numberoflikes: Int
//    let numberofcomments: Int
    
    let postername: String
    let createdAt: Int
    let updatedAt: Int
    let id: String
    let posterid: String
    let text: String
    let video: String
    let image: String
    let likecount: Int
    let type: Int
    let familyCode: String
    let wall: String
    let commentcount: Int
    let thumb: String
    let posterimage: String?
    let isliked: Int?
    let caption: String?
    
}
