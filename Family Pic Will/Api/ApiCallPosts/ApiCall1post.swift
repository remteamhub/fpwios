//
//  ApiCall1post.swift
//  familywithwall
//
//  Created by Apple on 20/05/2021.
//

enum ApiCall1post {
    struct Request : Encodable{
        var postid : String
    }
    struct Response : Codable{
        var status: Bool
        var message:String
        var data: [Posts]?
    }
}
