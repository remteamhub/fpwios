//
//  ApiCallUpdateProfile.swift
//  FPW
//
//  Created by Apple on 05/05/2021.
//

import Foundation
import UIKit
enum ApiCallPost {
    struct Request : Encodable {
        var familyCode : String
        var text : String
        var type : String
        var image = Data()
        
    }
    struct Response : Codable{
        var status : Bool
        var message : String
    }
}


extension UIImage {
    var data: Data? {
        if let data = self.jpegData(compressionQuality: 1.0) {
            return data
        } else {
            return nil
        }
    }
}
extension Data {
    var image: UIImage? {
        if let image = UIImage(data: self) {
            return image
        } else {
            return nil
        }
    }
}
