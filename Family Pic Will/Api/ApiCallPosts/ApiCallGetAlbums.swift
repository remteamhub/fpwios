// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - Welcome
enum ApiCallGetAlbums {
    
    struct Request : Encodable{
      var type : String
        var uid : String?
    }
    struct Response : Codable {
      var status: Bool
      var message:String
      var data: [Album]?
    }
   
}

// MARK: - Datum
struct Album: Codable {
    let createdAt, updatedAt: Int
    let id, albumname: String
    let type: Int
    let userid: String
    let count: Int
    let familyCode: String
    let image: String
    let thumb : String?
    let caption : String?
}
