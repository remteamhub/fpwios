//
//  ApiCallPosts.swift
//  familywithwall
//
//  Created by Apple on 17/05/2021.
//

import Foundation
import UIKit
enum ApiCallPosts {
    struct Request : Encodable{
        var page : Int
    }
    struct Response : Codable{
        var status: Bool
        var message:String
        var data: data?
    }
}
struct data: Codable {
    let posts: [Posts]
    let pages: DataClass
}

struct DataClass: Codable {
    
    let pages: String
    init(_ pages: String) {
            self.pages = pages
        }

        init(from decoder: Decoder) throws {
            let container = try decoder.singleValueContainer()
            // attempt to decode from all JSON primitives
            if let str = try? container.decode(String.self) {
                pages = str
            } else if let int = try? container.decode(Int.self) {
                pages = int.description
            } else if let double = try? container.decode(Double.self) {
                pages = double.description
            } else if let bool = try? container.decode(Bool.self) {
                pages = bool.description
            } else {
                throw DecodingError.typeMismatch(String.self, .init(codingPath: decoder.codingPath, debugDescription: ""))
            }
        }

        func encode(to encoder: Encoder) throws {
            var container = encoder.singleValueContainer()
            try container.encode(pages)
        }
   // let pages: Int?
    
}
