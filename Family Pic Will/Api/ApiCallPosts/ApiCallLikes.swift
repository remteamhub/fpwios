//
//  ApiCallLikes.swift
//  familywithwall
//
//  Created by Apple on 17/05/2021.
//

enum ApiCallLikes {
    struct Request : Encodable{
        var postid : String
    }
    struct Response : Codable{
        var status: Bool
        var message:String
        
    }
}
