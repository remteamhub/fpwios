import Foundation
import UIKit
enum ApiCallGetComments {
    struct Request : Encodable{
        var pid : String
    }
    struct Response : Codable {
        var status: Bool
        var message:String
        var data: [Comments]?
    }
}
