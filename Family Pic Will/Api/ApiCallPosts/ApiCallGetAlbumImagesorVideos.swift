
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - Welcome
enum ApiCallGetAlbumsImagesorVideos {
    
    struct Request : Encodable{
      var id : String
        var type : String
    }
    struct Response : Codable {
      var status: Bool
      var message:String
      var data: [AlbumData]?
    }
   
}

// MARK: - Datum
struct AlbumData: Codable {
    let createdAt, updatedAt: Int
       let id, albumid: String
       let image: String?
    let video : String?
       let userid: String
    let caption : String?
    let thumb : String?
}
