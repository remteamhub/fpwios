//
//  posts.swift
//  familywithwall
//
//  Created by Apple on 17/05/2021.
//

import Foundation
struct Comments : Codable {
//    let userimage: String
//    let username: String
//    let posttime: String
//    let postimage: String
//    let numberoflikes: Int
//    let numberofcomments: Int
    
    let createdAt: Int
    let updatedAt: Int
    let id: String
    let postid: String
    let commenterid: String
    let comment: String
    let commentername: String
    let commenterprofilepic: String
    
}
