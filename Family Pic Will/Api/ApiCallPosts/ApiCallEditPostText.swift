//
//  ApiCallEditPostText.swift
//  familywithwall
//
//  Created by Apple on 21/05/2021.
//

import Foundation
import UIKit
enum ApiCallEditPostText {
    struct Request : Encodable{
        var type : String
        var postid : String
        var text : String
        
        
    }
    struct Response : Codable{
        var status: Bool
        var message:String
        
    }
}
