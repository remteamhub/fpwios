//
//  ApiCallDeletePost.swift
//  familywithwall
//
//  Created by Apple on 19/05/2021.
//

enum ApiCallDeletePost {
    struct Request : Encodable{
        var postid : String
    }
    struct Response : Codable{
        var status: Bool
        var message:String
        
    }
}
