


enum ApiCallDeleteComment {
    struct Request : Encodable{
        var cid : String
    }
    struct Response : Codable{
        var status: Bool
        var message:String
        
    }
}
