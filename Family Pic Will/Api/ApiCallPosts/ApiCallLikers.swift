//
//  ApiCallLikers.swift
//  familywithwall
//
//  Created by Apple on 19/05/2021.
//

import Foundation
import UIKit
enum ApiCallLikers {
    struct Request : Encodable{
        var pid : String
    }
    struct Response : Codable{
        var status: Bool
        var message:String
        var data: [Likers]?
    }
}
