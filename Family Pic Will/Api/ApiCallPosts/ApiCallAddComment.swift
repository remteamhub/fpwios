//
//  ApiCallAddComment.swift
//  familywithwall
//
//  Created by Apple on 20/05/2021.
//

import Foundation
import UIKit
enum ApiCallAddComment {
    struct Request : Encodable{
        var comment : String
        var postid : String
        
    }
    struct Response : Codable{
        var status: Bool
        var message:String
        
    }
}
