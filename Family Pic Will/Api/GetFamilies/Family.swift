//
//  Data.swift
//  FPW
//
//  Created by Apple on 03/05/2021.
//

import Foundation
struct Family: Codable {
    var createdAt : Int
    var updatedAt : Int
    var id: String
    var familyCode : String
    public var family : String
    var memberId :String
    var status : Int
    
}
