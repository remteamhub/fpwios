//
//  ApiCallGetFamilies.swift
//  FPW
//
//  Created by Apple on 03/05/2021.
//

import Foundation
enum ApiCallGetFamilies {
    struct Request : Encodable{
        
    }
    struct Response: Codable {
        var status : Bool
        var message: String
        var data : [Family]?
    }
}
