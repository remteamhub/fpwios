//
//  ApiCallUpdateProfile.swift
//  FPW
//
//  Created by Apple on 05/05/2021.
//

import Foundation
enum ApiCallUpdateProfile {
    struct Request : Encodable{
        var name : String
        var password : String
        var zip : String
        var state: String
        var country : String
        var dob : String
        var city : String
        var email : String
        var gender : Int
    }
    struct Response : Codable{
        var status : Bool
        var message : String
    }
}
