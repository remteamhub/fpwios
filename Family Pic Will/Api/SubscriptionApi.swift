//
//  SubscriptionApi.swift
//  Family Pic Will
//
//  Created by Apple on 20/10/2021.
//

import Foundation
enum SubscriptionApi {
    struct Request : Encodable{
       var plan: Int
       var payment: String
       var amount: String
        
    }
    struct Response : Codable{
        var status: Bool
        var message:String
//        var data : User
    }
}
