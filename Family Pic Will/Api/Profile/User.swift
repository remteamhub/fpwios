//
//  User.swift
//  FPW
//
//  Created by Apple on 04/05/2021.
//

import Foundation
struct User : Codable{
    
    var createdAt : Int
    var updatedAt : Int
    var id : String
    var name : String
    var email : String
    var city : String
    var state : String
    var zip : String
    var country : String
    var dob : String
    var gender : Int
    var family : String
    var familyCode : String
    var profilepic : String
    var fcmToken : String
    var san : String
    var verify : Int
    let blocked: Int?
    let relation: String?
    let plan: Int?
    let reportCount: Int?
    let deceased: Int?
    let fcount: Int?
    let isselect : Bool?
    
}
