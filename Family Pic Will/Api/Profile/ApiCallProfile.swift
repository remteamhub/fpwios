//
//  ApiCallProfile.swift
//  FPW
//
//  Created by Apple on 04/05/2021.
//

import Foundation
enum ApiCallProfile {
    struct Request : Encodable{
       var nuser: String?
    
    }
    struct Response : Codable{
        var status: Bool
        var message:String
        var data: Profile?
    }
}
