//
//  ApiCallAddFamilyByCode.swift
//  FPW
//
//  Created by Apple on 04/05/2021.
//

import Foundation
enum ApiCallAddFamilyByCode {
    struct Request : Encodable {
        var familyCode : String
    }
    struct Response : Codable {
        var status : Bool
        var message: String
        var data: AddFamilyByCode?
        enum CodingKeys: String, CodingKey {
                case status = "status"
                case message = "message"
                case data = "data"
            }
    }
}
