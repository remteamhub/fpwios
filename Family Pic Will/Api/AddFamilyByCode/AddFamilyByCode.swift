//
//  AddFamilyByCode.swift
//  FPW
//
//  Created by Apple on 04/05/2021.
//

import Foundation
struct AddFamilyByCode :Codable{
    var createdAt : Int
    var updatedAt : Int
    var id : String
    var familyCode : String
    var family : String
    var memberId : String
    var status : Int
}
