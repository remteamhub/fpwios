//
//  File.swift
//  Family Pic Will
//
//  Created by Apple on 16/09/2021.
//

import Foundation
struct notification : Codable {
    
    
    let id: String
    let post_id: String
    let user_id: String
    let read: Int
    let familyCode: String
    let text: String
    let name: String
    let verb: String
    let pic: String
    
}
