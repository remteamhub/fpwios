//
//  Country.swift
//  Family Pic Will
//
//  Created by Apple on 11/05/2021.
//

import Foundation

struct MyCountry : Codable{
    var name : String
    //var iso3 : String
    var states : [State]
}
