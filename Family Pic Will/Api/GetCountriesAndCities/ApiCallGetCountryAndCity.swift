//
//  ApiCallGetCountryAndCity.swift
//  
//
//  Created by Apple on 11/05/2021.
//

import Foundation
enum ApiCallGetCountryAndCity {
    
    struct Request : Encodable{
        
    }
    struct Response : Codable{
        var error : Bool
        var msg : String
        var data : [MyCountry]?
    }
}
