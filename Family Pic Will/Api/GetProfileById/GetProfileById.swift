//
//  GetProfileById.swift
//  Family Pic Will
//
//  Created by Apple on 20/05/2021.
//

import Foundation
struct GetProfileById : Codable{
    var pasword : String
    var createdAt : Int
    var updatedAt : Int
    var id : String
    var name : String
    var email : String
    var city : String
    var state : String
    var zip : String
    var country : String
    var dob : String
    var gender : Int
    var family : String
    var familyCode : String
    var profilepic : String
    var fcmToken : String
    var san : String
    var verify : Int
    var status : Int
    var relation : String
    var wall : data1
}

struct data1: Codable {
    let posts: [Posts]
    let pages: DataClass1
}

struct DataClass1: Codable {
    
    let pages: String
    init(_ pages: String) {
            self.pages = pages
        }

        init(from decoder: Decoder) throws {
            let container = try decoder.singleValueContainer()
            // attempt to decode from all JSON primitives
            if let str = try? container.decode(String.self) {
                pages = str
            } else if let int = try? container.decode(Int.self) {
                pages = int.description
            } else if let double = try? container.decode(Double.self) {
                pages = double.description
            } else if let bool = try? container.decode(Bool.self) {
                pages = bool.description
            } else {
                throw DecodingError.typeMismatch(String.self, .init(codingPath: decoder.codingPath, debugDescription: ""))
            }
        }

        func encode(to encoder: Encoder) throws {
            var container = encoder.singleValueContainer()
            try container.encode(pages)
        }
   // let pages: Int?
    
}
