//
//  ApiCallGetProfileById.swift
//  Family Pic Will
//
//  Created by Apple on 20/05/2021.
//

import Foundation
enum ApiCallGetProfileById {
    struct Request : Encodable {
        var id : String
        var page : Int
    }
    struct Response : Codable {
        var status : Bool
        var message : String
        var data: GetProfileById?
    }
}
