//
//  MessageModel.swift
//  Family Pic Will
//
//  Created by mac on 8/27/21.
//

import Foundation

struct chatModel : Codable {
    let idReceiver : String
    let idSender : String
    let isFutrue : Bool
    let status : String
    let text : String
    let timestamp : CLong
    let type : String
    
}
//struct ChatDataList : Codable {
  //var chat_id : Int?
  //var username : String?
  //var userpic : String?
  //var message : String?
 // var time : String?
 // var user_type : String?
 // var conversation_id : Int?
//  var pagination : paginationData?
//  init(dictionary: [String:Any]) {
//    self.chat_id = dictionary[“chat_id”] as? Int
//    self.username = dictionary[“username”] as? String
//    self.userpic = dictionary[“userpic”] as? String
//    self.message = dictionary[“message”] as? String
//    self.time = dictionary[“time”] as? String
//    self.user_type = dictionary[“user_type”] as? String
//    self.conversation_id = dictionary[“conversation_id”] as? Int
//  }
//  init(username: String, message: String, userpic: String, conversation_id: Int) {
//    self.username = username
//    self.message = message
//    self.userpic = userpic
//    self.conversation_id = conversation_id
//    self.user_type = “me”
//  }
//}
