//
//  VerifyModel.swift
//  Family Pic Will
//
//  Created by mac on 4/30/21.
//

import Foundation
import Alamofire

enum VerifyModel {
    struct Request: Encodable {
        let code: String
    }
   
    struct Response: Codable {
        let status: Bool
        let message: String
        let data: DataClass
    }

    struct DataClass: Codable {
        let user: User
    }

    
    struct User: Codable {
        let createdAt, updatedAt: Int
        let id, name, email, city: String
        let state, zip, country, dob: String
        let gender: Int
        let family, familyCode, profilepic, fcmToken: String
        let san: String
        let verify: Int
    }
}
