//
//  ForgetPassModel.swift
//  Family Pic Will
//
//  Created by mac on 4/30/21.
//

import Foundation
import Alamofire

enum ForgetPassModel {
    struct Request: Encodable {
        let email: String
        
    }
   struct Response: Codable {
        let status: Bool
        let message, data: String
    }
}

