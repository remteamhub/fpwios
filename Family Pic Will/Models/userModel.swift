//
//  userModel.swift
//  Family Pic Will
//
//  Created by Apple on 06/05/2021.
//

import Foundation


struct user: Codable {
    let status: Bool
    let message: String
    let token: String
    let createdAt, updatedAt: Int
    let id, name, email, city: String
    let state, zip, country, dob: String
    let gender: Int
    let family, familyCode, profilepic, fcmToken: String
    let san: String
    let verify: Int
    let reportCount: Int
 
}
