//
//  SignupModel.swift
//  Family Pic Will
//
//  Created by mac on 4/29/21.
//
import Foundation
import Alamofire

enum SignupModel {
    struct Request: Encodable {
       
        let email: String
        let name: String
        let password: String
        let zip: String
        let state: String
        let country: String
        let gender: Int
        let family: String
        let familyCode: String
        let dob: String
        let city: String
        
    }
   
    struct Response: Codable {
        let status: Bool
        let message: String
        let data: DataClass
    }

    struct DataClass: Codable {
        let user: User
        let token: String
    }

    struct User: Codable {
        let createdAt, updatedAt: Int
        let id, name, email, city: String
        let state, zip, country, dob: String
        let gender: Int
        let family, familyCode, profilepic, fcmToken: String
        let san: String
        let verify: Int
        let reportCount: Int
    }
    struct Response2: Codable {
        let status: Bool
        let message: String
    }
}
