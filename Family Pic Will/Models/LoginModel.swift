//
//  LoginModel.swift
//  Family Pic Will
//
//  Created by mac on 4/29/21.
//

import Foundation
import Alamofire

enum LoginModel {
    struct Request: Encodable {
        let email: String
        let password: String
        let fcmtoken: String
    }
   
    struct Response: Codable {
        let status: Bool
        let message: String
        let data: DataClass
    }


    struct DataClass: Codable {
        let user: User
        let token: String
    }

    struct User: Codable {
        let createdAt: Int
        let updatedAt: Int
        let gender: Int
        let reportCount: Int
        let verify: Int
        let id: String
        let name: String
        let email: String
        let city: String
        let state: String
        let zip: String
        let country: String
        let dob: String
        let family: String
        let familyCode: String
        let profilepic: String
        let fcmToken: String
        let san: String
        
    }
    
//    struct Response2: Codable {
//        let status: Bool
//        let message: String
//    }

}
