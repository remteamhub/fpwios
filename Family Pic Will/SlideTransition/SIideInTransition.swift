//
//  SIideInTransition.swift
//  FPW
//
//  Created by Apple on 03/05/2021.
//
import UIKit
import Foundation
protocol CloseSideMenu {
    func closeSideMenu()
}
class SlideInTransition: NSObject,UIViewControllerAnimatedTransitioning {
    var delegate : CloseSideMenu?
    var isPresenting = false
    let dimmingView = UIView()
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let toViewController = transitionContext.viewController(forKey: .to),
              let fromViewController = transitionContext.viewController(forKey: .from) else {
            return
        }
        let containerView = transitionContext.containerView
        let finalWidth = toViewController.view.bounds.width * 0.8
        let finalHeight = toViewController.view.bounds.height
        if isPresenting {
            // Add dimming view
            dimmingView.backgroundColor = UIColor(red: 38/255, green: 56/255, blue: 75/255,alpha: 1)
            //dimmingView.backgroundColor = .black
            //dimmingView.alpha = 0.0
            containerView.addSubview(dimmingView)
            let gestureTap = UITapGestureRecognizer(target: self, action: #selector(self.openSideMenu))
            gestureTap.numberOfTapsRequired = 1
            dimmingView.addGestureRecognizer(gestureTap)
            dimmingView.frame = containerView.bounds
            
            //animate on screen
            containerView.addSubview(toViewController.view)
            toViewController.view.frame = CGRect (x: -finalWidth, y: 0, width: finalWidth, height: finalHeight)
        }
        // Animate on Screen
        let transform = {
            self.dimmingView.alpha = 1
            toViewController.view.transform = CGAffineTransform(translationX: finalWidth, y: 0)
        }
        
        // Animation of the Screen
        let identity = {
            self.dimmingView.alpha = 0.0
            fromViewController.view.transform = .identity
        }
        // Animation of the transition
        let duration = transitionDuration(using: transitionContext)
        let isCancelled = transitionContext.transitionWasCancelled
        UIView.animate(withDuration: duration) {
            if self.isPresenting{
                transform()
            }
            else{
                identity()
            }
        } completion: { (_) in
            transitionContext.completeTransition(!isCancelled)
        }

    }
    @objc func openSideMenu(){
        print("Clicked")
        delegate?.closeSideMenu()
    }
    
}
